import { createStore, applyMiddleware } from "redux";
import RootReducer from  "./Reducers/rootReducer";
import ReduxThunk from 'redux-thunk';
import { persistStore } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';

/**
 * This is redux store 
 * Manage all action and reducers in redux
*/
export const store = createStore(RootReducer, composeWithDevTools(
  applyMiddleware(ReduxThunk)
));
export const persistor = persistStore(store);
export default { store, persistor }