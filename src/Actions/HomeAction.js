import * as ActionType from "./ActionType";

export const SaveEditData  = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "GET_DATA", data });
    }
}

export const SaveAdminData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_ADMIN_DATA", data });
    }
}

export const saveSidebarName = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SIDEBAR_NAME", data });
    }
}

export const saveFatherBioTests = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "CLICKED_FATHER_BIO_TEST_DATA", data });
    }
}

export const saveFatherBioTestNumber = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_FATHER_BIO_NUMBER", data });
    }
}

export const changeFilterStatus = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "CHANGE_FILTER_STATUS", data });
    }
}

export const saveExperimentData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_FATHER_DATA", data });
    }
}
export const clearExperimentData = () => {
    return async function(dispatch) {
        return dispatch({ type: "CLEAR_FATHER_DATA" });
    }
}
export const saveSupplierSortingData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_SUPPLIER_SORTING_DATA", data });
    }
}
export const SaveUserSortingData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_USER_SORTING_DATA", data });
    }
}
export const SaveExtractSortingData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_EXTRACT_SORTING_DATA", data });
    }
}
export const SaveExperimentSortingData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_EXPERIMENT_SORTING_DATA", data });
    }
}
export const SaveSubBioSortingData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_SUBBIOTEST_SORTING_DATA", data });
    }
}
export const SaveMedicalIndicationSortingData = (data) => {
    return async function(dispatch) {
        return dispatch({ type: "SAVE_MEDICAL_INDICATION_SORTING_DATA", data });
    }
}

