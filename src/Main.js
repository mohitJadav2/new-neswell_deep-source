import React from "react";
import { FaBars } from "react-icons/fa";
import Header from "./components/HeaderMain";
import { connect } from "react-redux";

const Main = (props) => {
  return (
    <main>
      <div className="btn-toggle">
        <FaBars />
      </div>
      {/* 
        *this conditions is for the if unauthorize user is try to access this page then function give the error massage  
      */}
      {props?.adminData &&
        ((props?.sideBarName === "Users" &&
          props?.adminData?.permissions != "Admin") ||
          (props?.sideBarName === "bioTestes" &&
            props?.adminData?.permissions != "Admin")) && (
          <p className="dont-have-access-content">
            You dont have access to this page
          </p>
        )}
      <Header />
      <div className="main-inner">{props.children}</div>
    </main>
  );
};

/**
 * This function manage adminData and sideBarName in redux
 */
const mapStateToProps = (state) => ({
  adminData: state.homeReducer.adminData,
  sideBarName: state.homeReducer.sideBarName,
});
export default connect(mapStateToProps, null)(Main);
