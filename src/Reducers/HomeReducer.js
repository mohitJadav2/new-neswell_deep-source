
import * as ActionType from "../Actions/ActionType";

const initialState = {
  buttonData: undefined,
  sideBarName: "Users",
  extractFilterStatus: false,
  fatherData: undefined
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionType.GET_DATA:
      return {
        ...state,
        data: action.data,
      };
    case ActionType.SAVE_ADMIN_DATA:
        return {
          ...state,
          adminData: action.data,
        };
    case ActionType.SIDEBAR_NAME:
          return {
            ...state,
            sideBarName: action.data,
          };
    case ActionType.CLICKED_FATHER_BIO_TEST_DATA:
          return {
            ...state,
            parentBioTestData: action.data,
          };
    case ActionType.SAVE_FATHER_BIO_NUMBER:
      return {
        ...state,
        saveFatherNumber: action.data,
      };
    case ActionType.CHANGE_FILTER_STATUS:
      return {
        ...state,
        extractFilterStatus: action.data,
      };
    case ActionType.SAVE_FATHER_DATA:
        return {
          ...state,
          fatherData: action.data,
        };
    case ActionType.CLEAR_FATHER_DATA:
          return {
            ...state,
            fatherData: undefined,
          };
    case ActionType.SAVE_SUPPLIER_SORTING_DATA:
      return {
        ...state,
        supplierSortingData: action.data,
      };
    case ActionType.SAVE_USER_SORTING_DATA:
    return {
      ...state,
      userSortingData: action.data,
    };
    case ActionType.SAVE_EXTRACT_SORTING_DATA:
    return {
      ...state,
      extractSortingData: action.data,
    };
    case ActionType.SAVE_EXPERIMENT_SORTING_DATA:
      return {
        ...state,
        experimentSortingData: action.data,
      };
    case ActionType.SAVE_SUBBIOTEST_SORTING_DATA:
      return {
        ...state,
        subBioSortingData: action.data,
      };
    case ActionType.SAVE_MEDICAL_INDICATION_SORTING_DATA:
    return {
      ...state,
      medicalIndicationSortingData: action.data,
    };
    default:
      return state;
  }
};

