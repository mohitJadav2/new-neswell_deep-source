import React, { useEffect, useState } from 'react'
import Highcharts from 'highcharts/highstock';
import Chart from './Chart'
import BioTestApi from '../../services/BioTestApi';

function ScoringGraph(props) {
    const [basicData, setBasicData] = useState([])
    const [basicData2, setBasicData2] = useState([])
    const [basicData3, setBasicData3] = useState([])
    const [error, setError] = useState(undefined)
    const [errorMsg, setErrorMsg] = useState(undefined)

    /**
     * This function manage extract, predicated score and actual score data from using API
     */
    useEffect(() => {
         BioTestApi.SubBioTestView(
            localStorage.getItem("neswell_access_token"),
            props?.id
        ).then(async (res) => {
          if(!res?.data?.error){
            setBasicData(res?.data?.extract_nms)
            setBasicData2(res?.data?.labeled_score_data) 
            setBasicData3(res?.data?.predicted_score_data)
          }else{
            setErrorMsg(res?.data?.error?.messages)
            setError(true)
          }
        });
    }, []);
    const chartOptions = {
        title: {
          text: '',
        },
        xAxis: {
            max: 12,
            categories: basicData,
            visible: true,
            crosshair: true,
            title:{
              text: 'Extracts',
            },
            scrollbar: {
              enabled: true
          },
        },
        yAxis: [
           {
            title: {
              text: 'Score',
              style: {
                color: ''
              }
            },
            labels: {
              format: '{value}',
              style: {
                color: ''
              }
            },
            
      
          }]
          ,
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0, 
            },
          },
        
          series: [{
            name: 'Predicted Score',
            type: 'line',
            data: basicData3,
            lineWidth: 0.75,
            color: 'orange',
            tooltip: {
            },
            marker:{
              radius: 5,
            },
          },
          {
            name: 'Labeled Score',
            type: 'scatter',
            color: 'blue',
            data: basicData2,
            tooltip: {
              headerFormat: '{point.x}',
              pointFormat: '</br>Labeled Score: <b>{point.y}</b>',
            },
            marker:{
                symbol: 'circle',
                radius: 4
            }
          }
          ],
      };
  return (
    <div>
      {error && <p>{errorMsg}</p>}
     {!error && <Chart options={chartOptions} highcharts={Highcharts}/>}
    </div>
  )
}

export default ScoringGraph
