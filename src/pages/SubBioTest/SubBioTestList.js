import React, { useState, useEffect, useRef } from "react";
import Layout from "../../Layout";
import { FiSearch } from "react-icons/fi";
import Pagination from "react-responsive-pagination";
import BioTestApi from "../../services/BioTestApi";
import { saveFatherBioTests, SaveSubBioSortingData } from "../../Actions/HomeAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import Loader from "../../components/loader";
import { BsChevronDown, BsChevronUp, BsPlusCircle } from "react-icons/bs";
import { AiFillCaretDown, AiFillCaretUp, AiOutlineSetting } from "react-icons/ai";
import Modal from "../../components/ModalComponent";

function SubBioTestList(props) {
    const [totalPages, setTotalPages] = useState(undefined);
    const [currentPage, setCurrentPage] = useState(1);
    const [subBioTestes, setSubBioTestes] = useState(undefined);
    const [totalCount, setTotalCount] = useState(undefined);
    const [loader, setLoader] = useState(false);
    let history = useHistory();
    const [perPage, setPerPage] = useState(props?.subBioSortingData?.perPage ? props?.subBioSortingData?.perPage : 10);
    const [toggleDropDown, setToggleDropDown] = useState(false);
    const [searchText, setSearchText] = useState("");
    const [error, setError] = useState(false)
    const [openDropDown, setOpenDropDown] = useState(undefined);    
    const perPageValue = [10, 25, 50, 100];
    const [modalOpen, setModalOpen] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [deleteUserData, setDeleteUserData] = useState(undefined);
    const [orderType, setOrderType] = useState("desc");
    const [orderField, setOrderField] = useState("id");

    /** 
     * This useEffect() manage bioTest records
     */
    useEffect(() => {
        props?.subBioSortingData && setOrderType(props?.subBioSortingData?.orderType)
        props?.subBioSortingData && setOrderField(props?.subBioSortingData?.orderField)
        props?.subBioSortingData && setCurrentPage(props?.subBioSortingData?.page)
        props?.subBioSortingData && setPerPage(props?.subBioSortingData?.perPage ? props?.subBioSortingData?.perPage : 10)
        setLoader(true);
        BioTestApi.getAllSubBioTestList(
            localStorage.getItem("neswell_access_token"),
            props?.subBioSortingData ? props?.subBioSortingData?.orderType : orderType,
            props?.subBioSortingData ? props?.subBioSortingData?.orderField :orderField,
            props?.subBioSortingData ? props?.subBioSortingData?.page : currentPage,
            props?.subBioSortingData ? props?.subBioSortingData?.perPage ? props?.subBioSortingData?.perPage :10 : perPage
        ).then((json) => {
            if (!json?.data?.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                setLoader(false);
            } else {
                setLoader(false);
            }
        });
    }, []);

    /**
     * This function manage popof setting for every user 
    */
  
     const handleSettingClick = async (item) => {
        openDropDown == item?.id
          ? setOpenDropDown(undefined)
          : setOpenDropDown(item?.id);
      };

    /**
     * This function manage click outside of the popup box 
    */
     function useOutsideAlerter(ref) {
        useEffect(() => {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
            setOpenDropDown(undefined);
            }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
        }, [ref]);
    }

    /**this ref is for the when user clicked outside the fucntion */
     const wrapperRef = useRef(null);
     useOutsideAlerter(wrapperRef);
    /**
      * This function manages pagination with passing page number in API
     */
    function handlePageChange(page) {
        setLoader(true)
        let sortingData = {orderType: orderType, orderField:orderField, page: page,perPage:perPage}
        props.SaveSubBioSortingData(sortingData)
         setCurrentPage(page);
        if(searchText){
            BioTestApi.searchSubBioTestes(
            localStorage.getItem("neswell_access_token"),
            searchText,
            orderType,
            orderField,
            perPage,
            page,
            ).then((json) => {
                if (!json?.data.error) {
                    let totalPages = json?.data?.count / perPage;
                    setTotalPages(Math.ceil(totalPages));
                    setTotalCount(json?.data?.count);
                    setSubBioTestes(json?.data?.results); 
                    setLoader(false)  
                    if(json?.data?.results == undefined){
                        setError(true)
                    }else{
                        setError(false)
                    }
                } else {
                    setLoader(false)
                    setError(true)
                }
            });                                                                                     
        }
        else{
            BioTestApi.getAllSubBioTestList(
            localStorage.getItem("neswell_access_token"),
            orderType,
            orderField,
            page,
            perPage,
        ).then((json) => {
            if (!json?.data?.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                setLoader(false);
            } else {
                setLoader(false);
            }
        });
        }
    }

    /**
       * This function manage search feature 
       * This function passes user's enter value to API and get positive and error response
    */
    const handleSearch = (value) => {
        setCurrentPage(1);
        setSearchText(value)
        BioTestApi.searchSubBioTestes(
        localStorage.getItem("neswell_access_token"),
        value,
        orderType,
        orderField,
        perPage,
        1
        ).then((json) => {
            if (!json?.data.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                if(json?.data?.results == undefined){
                    setError(true)
                }else{
                    setError(false)
                }
            } else {
                setError(true)
            }
        });
    };

     /**
     * This function manages record display drop down 
     * When user change value of drop down at that time  this function call API with change value
    */
  const handlePerPageChanges = async (perPageNuumber) => {
    let sortingData = {orderType: orderType, orderField:orderField, page: 1, perPage:perPageNuumber}
    props.SaveSubBioSortingData(sortingData)
    setToggleDropDown(false)
    setPerPage(perPageNuumber)
    setCurrentPage(1)
    if (searchText) {
        BioTestApi.searchSubBioTestes(
        localStorage.getItem("neswell_access_token"),
        searchText,
        orderType,
        orderField,
        perPageNuumber,
        1
        ).then((json) => {
            if (!json?.data.error) {
                let totalPages = json?.data?.count / perPageNuumber;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                if(json?.data?.results == undefined){
                    setError(true)
                }else{
                    setError(false)
                }
            } else {
                setError(true)
            }
        })
    } else {
      await BioTestApi.getAllSubBioTestListPerPage(
        localStorage.getItem("neswell_access_token"),
        orderType,
        orderField,
        1,
        perPageNuumber
      ).then(
        async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setSubBioTestes(json?.data?.results)
          }
        }
      )
    }
  }
  /** 
     * This fucntion manage to redirect to create bio page for creat a new sub bio test 
    */
   const handleCreatClick = () => {
    history.push({
        pathname: `/create-bio`,
        state: { userDetail: props?.match?.params?.slug }
    })
    }
    const handleViewChildBio = (id) => {
        history.push({
            pathname: `/view-bio-details/${id}`,
            state: { userDetail: `/sub-bio-test-list` }
        })
    }
   
    const handlePredictSubBio = (id) => {
    history.push(`/sub-bio-test-predict/${id}`)
}
    const handleScoreChildBio = (id) => {
        history.push(`/sub-bio-test-score/${id}`)
        }
     /**
      * This function manage Duplicate sub bio test button and user redirect to create sub bio test page
    */
      const handleDuplicateChild = (id) => {
        history.push({
        pathname: `/create-bio/${id}`,
        state: { userDetail:props?.match?.params?.slug },
        });
    };

    /**This function is use for delete sub bio test record */
    const handleDeleteChild = () => {
        setLoader(true)
        setModalOpen(false);
        if (searchText) {
            BioTestApi.getAllSubBioTestDelete(
                localStorage.getItem("neswell_access_token"),
                deleteUserData,
                "DELETE"
                ).then((json) =>{
                    BioTestApi.searchSubBioTestes(
                    localStorage.getItem("neswell_access_token"),
                    searchText,
                    orderType,
                    orderField,
                    perPage,
                    1
                    ).then((json) => {
                        if (!json?.data.error) {
                            let totalPages = json?.data?.count / perPage;
                            setTotalPages(Math.ceil(totalPages));
                            setTotalCount(json?.data?.count);
                            setSubBioTestes(json?.data?.results);
                            setLoader(false)
                            if(json?.data?.results == undefined){
                                setError(true)
                            }else{
                                setError(false)
                            }
                        } else {
                            setLoader(false)
                            setError(true)
                        }
                    })
                })
        } else {
            BioTestApi.getAllSubBioTestDelete(
                localStorage.getItem("neswell_access_token"),
                deleteUserData,
                "DELETE"
                ).then((json) => {
                    setLoader(true);
                    BioTestApi.getAllSubBioTestList(
                        localStorage.getItem("neswell_access_token"),
                        orderType,
                        orderField,
                        currentPage,
                        perPage
                    ).then((json) => {
                        if(json?.data?.length<=0){
                            let sortingData = {orderType: orderType, orderField:orderField, page: currentPage-1, perPage:perPage}
                             props.SaveSubBioSortingData(sortingData)
                            BioTestApi.getAllSubBioTestList(
                                localStorage.getItem("neswell_access_token"),
                                orderType,
                                orderField,
                                currentPage-1,
                                perPage
                            ).then((json) =>{
                                let totalPages = json?.data?.count / perPage;
                                setTotalPages(Math.ceil(totalPages));
                                setTotalCount(json?.data?.count);
                                setSubBioTestes(json?.data?.results);
                                setLoader(false);
                            })
                        }
                        if (!json?.data?.error) {
                            let totalPages = json?.data?.count / perPage;
                            setTotalPages(Math.ceil(totalPages));
                            setTotalCount(json?.data?.count);
                            setSubBioTestes(json?.data?.results);
                            setLoader(false);
                        } else {
                            setLoader(false);
                        }
                    });
                });
             }
        };
        
         /**
         * This function manage user record column's ascending and descending order
         */
        const handleAscending = (fieldName) => {
            setOrderType("asc");
            setOrderField(fieldName);
            setLoader(true);
            setCurrentPage(1);
            if (searchText) {
                BioTestApi.searchSubBioTestes(
                localStorage.getItem("neswell_access_token"),
                searchText,
                "asc",
                fieldName,
                perPage,
                1,
            ).then((json) => {
                if (!json.data.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                let sortingData = {orderType: "asc", orderField:fieldName, page:1, perPage:perPage}
                props.SaveSubBioSortingData(sortingData)
                setSubBioTestes(json?.data?.results);
                setTotalCount(json?.data?.count);
                setLoader(false);
                } else {
                setLoader(false);
                }
            });
            } else {
                BioTestApi.subBioAscendingOrder(
                "asc",
                fieldName,
                1,
                perPage,
                localStorage.getItem("neswell_access_token")
            ).then((json) => {
                if (!json?.data?.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                let sortingData = {orderType: "asc", orderField:fieldName,page:1, perPage:perPage}
                props.SaveSubBioSortingData(sortingData)
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                setLoader(false);
                } else {
                setLoader(false);
                }
            });
            }
        }
        
        const handledescending = (fieldName) => {
            setOrderType("desc");
            setOrderField(fieldName);
            setLoader(true);
            setCurrentPage(1);
            if (searchText) {
                BioTestApi.searchSubBioTestes(
                localStorage.getItem("neswell_access_token"),
                searchText,
                "desc",
                fieldName,
                perPage,
                1,
            ).then((json) => {
                if (!json.data.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                let sortingData = {orderType: "desc", orderField:fieldName,page:1, perPage:perPage}
                props.SaveSubBioSortingData(sortingData)
                setSubBioTestes(json?.data?.results);
                setTotalCount(json?.data?.count);
                setLoader(false);
                } else {
                setLoader(false);
                }
            });
            } else {
                BioTestApi.subBioAscendingOrder(
                "desc",
                fieldName,
                1,
                perPage,
                localStorage.getItem("neswell_access_token")
            ).then((json) => {
                if (!json?.data?.error) {
                let totalPages = json?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                let sortingData = {orderType: "desc", orderField:fieldName,page:1, perPage:perPage}
                props.SaveSubBioSortingData(sortingData)
                setTotalCount(json?.data?.count);
                setSubBioTestes(json?.data?.results);
                setLoader(false);
                } else {
                setLoader(false);
                }
            })
            }
        }

    return (
        <Layout>
            {loader && <Loader />}
            <Modal
            open={modalOpen}
            closeModal={() => setModalOpen(false)}
            DeleteUsersRecord={() => handleDeleteChild()}
            title={"Are you sure?"}
            subTitle={"Do you want to delete this record?"}
            delete={deleteModal}
            />
            <div className="bio-test-list-wrapper">
                <div className="bio-test-title-list-icon">
                    <div className="bio-test-search">
                    <div className="search">
                        <button className="search-icon">
                            {" "}
                            <FiSearch />{" "}
                        </button>
                        <input
                            onChange={(e) => handleSearch(e.target.value)}
                            type="search"
                            placeholder="Search"
                            className="search-input"
                        />
                    </div>
                </div>
                    <button onClick={() => handleCreatClick()} className="add-filter primary-btn"> <BsPlusCircle /> Create Sub Bio Test </button>
                </div>
                
                <div className='list-view-table details-table'>
                    <table className="sub-bio-test-table">
                        <thead>
                            <tr>
                            <th style={{ width: "40px" }}>
                                <div onClick={() => {orderType == "asc" ? handledescending("id") : handleAscending("id") }} style={{display: 'flex', alignItems: 'center', cursor: "pointer"}}>
                                    Sr.
                                    <div style={{marginLeft: 10, height: 22,}}>
                                        <div style={{cursor: "pointer", marginBottom: -8,}}><AiFillCaretUp style={{opacity: orderField == "id"  && orderType == "asc" ? 0.5 : 1}}/></div>
                                        <div style={{cursor: "pointer", marginTop: -8,}}><AiFillCaretDown style={{opacity: orderField == "id"  && orderType == "desc" ? 0.5 : 1}}/></div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div onClick={() => {orderType == "asc" ? handledescending("name") : handleAscending("name") }} style={{display: 'flex', alignItems: 'center', cursor: "pointer"}}>
                                Sub Bio-Tests
                                    <div style={{marginLeft: 10, height: 22,}}>
                                        <div style={{cursor: "pointer", marginBottom: -8,}}><AiFillCaretUp style={{opacity: orderField == "name"  && orderType == "asc" ? 0.5 : 1}}/></div>
                                        <div style={{cursor: "pointer", marginTop: -8,}}><AiFillCaretDown style={{opacity: orderField == "name"  && orderType == "desc" ? 0.5 : 1}}/></div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div onClick={() => {orderType == "asc" ? handledescending("general_bio_test_id") : handleAscending("general_bio_test_id") }} style={{display: 'flex', alignItems: 'center', cursor: "pointer"}}>
                                    General Bio-test
                                    <div style={{marginLeft: 10, height: 22,}}>
                                        <div style={{cursor: "pointer", marginBottom: -8,}}><AiFillCaretUp style={{opacity: orderField == "general_bio_test_id"  && orderType == "asc" ? 0.5 : 1}}/></div>
                                        <div style={{cursor: "pointer", marginTop: -8,}}><AiFillCaretDown style={{opacity: orderField == "general_bio_test_id"  && orderType == "desc" ? 0.5 : 1}}/></div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div onClick={() => {orderType == "asc" ? handledescending("created") : handleAscending("created") }} style={{display: 'flex', alignItems: 'center', cursor: "pointer"}}>
                                    Created at
                                    <div style={{marginLeft: 10, height: 22,}}>
                                        <div style={{cursor: "pointer", marginBottom: -8,}}><AiFillCaretUp style={{opacity: orderField == "created"  && orderType == "asc" ? 0.5 : 1}}/></div>
                                        <div style={{cursor: "pointer", marginTop: -8,}}><AiFillCaretDown style={{opacity: orderField == "created"  && orderType == "desc" ? 0.5 : 1}}/></div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div onClick={() => {orderType == "asc" ? handledescending("user_id") : handleAscending("user_id") }} style={{display: 'flex', alignItems: 'center', cursor: "pointer"}}>
                                    Created by
                                    <div style={{marginLeft: 10, height: 22,}}>
                                        <div style={{cursor: "pointer", marginBottom: -8,}}><AiFillCaretUp style={{opacity: orderField == "user_id"  && orderType == "asc" ? 0.5 : 1}}/></div>
                                        <div style={{cursor: "pointer", marginTop: -8,}}><AiFillCaretDown style={{opacity: orderField == "user_id"  && orderType == "desc" ? 0.5 : 1}}/></div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div onClick={() => {orderType == "asc" ? handledescending("user_id") : handleAscending("user_id") }} style={{display: 'flex', alignItems: 'center', cursor: "pointer"}}>
                                    Scored
                                    <div style={{marginLeft: 10, height: 22,}}>
                                        <div style={{cursor: "pointer", marginBottom: -8,}}><AiFillCaretUp style={{opacity: orderField == "user_id"  && orderType == "asc" ? 0.5 : 1}}/></div>
                                        <div style={{cursor: "pointer", marginTop: -8,}}><AiFillCaretDown style={{opacity: orderField == "user_id"  && orderType == "desc" ? 0.5 : 1}}/></div>
                                    </div>
                                </div>
                            </th>
                            <th style={{ width: "140px", textAlign: 'left', cursor: "context-menu" }}>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {subBioTestes?.map((item) => (
                                
                                <tr >
                                    <td onClick={() => {handleViewChildBio(item?.id)}} style={{ cursor: "pointer" }}>{item?.id}</td>
                                    <td onClick={() => {handleViewChildBio(item?.id)}} style={{ cursor: "pointer" }}>{item?.name}</td>
                                    <td onClick={() => {handleViewChildBio(item?.id)}} style={{ cursor: "pointer" }}>{item?.general_bio_test_id}</td>
                                    <td onClick={() => {handleViewChildBio(item?.id)}} style={{ cursor: "pointer" }}>{item?.created}</td>
                                    <td onClick={() => {handleViewChildBio(item?.id)}} style={{ cursor: "pointer" }}>{item?.user_id}</td>
                                    <td onClick={() => {handleViewChildBio(item?.id)}} style={{ cursor: "pointer" }}>{item?.scored == 1 ? "True" : "False"}</td>
                                    <td >
                                    <span  onClick={() => handleSettingClick(item)} className="blue-color-eyes">
                                        <AiOutlineSetting style={{ color: "#404040" }} />
                                        {openDropDown == item?.id && 
                                        <div className="user-setting-popup"
                                        ref={wrapperRef}  
                                        >
                                        <p onClick={() => {handleViewChildBio(item?.id)}}>View</p>
                                        <p onClick={() => {handleScoreChildBio(item?.id)}}>Score</p>
                                        <p onClick={() => {handlePredictSubBio(item?.id)}}>Predict</p>
                                        <p onClick={() => { handleDuplicateChild(item?.id) }}>Duplicate</p>
                                        <p onClick={() => {
                                                        setDeleteModal(true)
                                                        setModalOpen(true)
                                                        setDeleteUserData(item?.id)
                                                    }}>
                                                        Remove
                                                    </p>
                                        </div>}
                                    </span>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                        {error && <p style={{textAlign:'center'}}>No data available</p>}
                    <div className="pagination-wrapper">
                <div className="entries-content">
                  <span>{`Showing 1 to ${subBioTestes?.length} of ${totalCount} entries`}</span>
                </div>
                <div className="pagination">
                  <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={(page) => handlePageChange(page)}
                  />
                </div>
                <div className="per-pagination-wrapper">
                  <div
                    onClick={() => setToggleDropDown(!toggleDropDown)}
                    className={
                      toggleDropDown ? "selected-page open" : "selected-page "
                    }
                  >
                    <p>{perPage}</p>
                    <div className="up-down-arrow">
                      <span className="down-arrow">
                        <BsChevronDown />
                      </span>
                      <span className="up-arrow">
                        <BsChevronUp />
                      </span>
                    </div>
                  </div>
                  {toggleDropDown && (
                    <ul className="per-page-dropdown">
                      {perPageValue?.map((item) => (
                        <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
                </div>
            </div>
        </Layout>
    );
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ saveFatherBioTests,SaveSubBioSortingData }, dispatch),
    };
}
const mapStateToProps = (state) => ({
    subBioSortingData: state.homeReducer.subBioSortingData
  });
export default connect(mapStateToProps, mapDispatchToProps)(SubBioTestList);







