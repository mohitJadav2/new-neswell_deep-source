import React, { useState, useEffect, useRef } from "react";
import "./extractprofile.style.scss";
import { MdDateRange, MdInvertColors } from "react-icons/md";
import { FiSave } from "react-icons/fi";
import { GrFormClose } from "react-icons/gr";
import Layout from "../../Layout";
import DatePicker from "react-date-picker";
import { ColorPicker, useColor } from "react-color-palette";
import "react-color-palette/lib/css/styles.css";
import DashboardApi from "../../services/DashboardApi";
import * as moment from "moment";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import Loader from "../../components/loader";
import MyGoogleMap from "../../components/MyGoogleMap";
import Select from "react-select";
import { AiOutlineClose } from "react-icons/ai";

/**
 *This function manage create new extract record
 */
function CreateNewExtract(props) {
  let history = useHistory();
  const [name, setName] = useState(undefined);
  const [reservationPeriod, setReservationPeriod] = useState(undefined);
  const [priceUnit, setPriceunit] = useState(10);
  const [cbd, setCBD] = useState(undefined);
  const [thc, setThc] = useState(undefined);
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  const [image, setImage] = useState(undefined);
  const [error, setError] = useState(undefined);
  const [fileError, setFileError] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [selectCurrencyName, setSelectCurrencyName] = useState("usd");
  const [sampleInventoryLat, setSampleInventoryLat] = useState(
    props?.match?.params?.slug ? undefined : "31.771959"
  );
  const [sampleInventorylng, setSampleInventoryLng] = useState(
    props?.match?.params?.slug ? undefined : "35.217018"
  );
  const [extraInventoryLng, setExtraInventoryLng] = useState(
    props?.match?.params?.slug ? undefined : "35.217018"
  );
  const [extraInventoryLat, setExtraInventoryLat] = useState(
    props?.match?.params?.slug ? undefined : "31.771959"
  );
  const [smpleInventory, setSampleInventory] = useState(undefined);
  const [sampleUsed, setSampleUsed] = useState(undefined);
  const [totalInventory, setTotalInventory] = useState(undefined);
  const [extractInventory, setExtractInventory] = useState(undefined);
  const [extractInventoryUsed, setExtractInventoryUsed] = useState(undefined);
  const [totalExtractInventory, setTotalExtractInventory] = useState(undefined);
  const [editGetPhoto, setEditGetPhoto] = useState(undefined);
  const [user, setUser] = useState(null);
  const [supplierLat, setSupplierLat] = useState(
    props?.match?.params?.slug ? undefined : "31.771959"
  );
  const [supplierLng, setSupplierLng] = useState(
    props?.match?.params?.slug ? undefined : "35.217018"
  );
  const [extantionError, setExtantionError] = useState(false);
  const [sampleGraterThenError, setSampleGraterThenError] = useState(false);
  const [extractGraterThenError, setExtractGraterThenError] = useState(false);
  const [supplierName, setSupplierName] = useState([]);
  const [supplierValue, setSupplierValue] = useState([]);
  const [extractLocation, setExtractLocation] = useState("");
  const [supplierLocation, setSupplierLocation] = useState("");
  const [sampleLocation, setSampleLocation] = useState("");

  const [addressSampleName, setAddressSampleName] = useState(undefined);
  const [addressExtractName, setAddressExtractName] = useState(undefined);
  const [addressSupplierName, setAddressSupplerName] = useState(undefined);
  const [address, setAddress] = useState("");
  const [coaError, setCoAError] = useState(undefined);
  const [Data, setData] = useState({
    certificate: "",
    CBD: 0,
    THC: 0,
    inv_ava_data: new Date(),
    reservation_period: 0,
    firmname: "",
    Inventory: "",
    color: "",
  });
  const [color, setColor] = useColor("hex", "Choose Color");
  /**
   * this style is use for the selection fild
   */
  const customSelectStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? "#fff" : "#404040",
      padding: 10,
      borderRadius: 10,
      width: "98%",
      margin: "10px auto",
      cursor: "pointer",
    }),
    control: (styles) => ({
      ...styles,
      border: "solid 1px #E5E5E5",
      backgroundColor: "white",
      maxHeight: 46,
      minHeight: 46,
      fontSize: 14,
      borderRadius: 10,
      color: "#929292",
      fontWeight: "600",
    }),
  };
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  /**
   * When user clicked outside of the alert popup box this function executed
   * The function manages mouse event
   */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplayColorPicker(false);
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  /**
   * This function manage Extract edit feature and API set particular extract data in edit from
   * And user change the details and that data send in json formant to API
   */
  useEffect(() => {
    if (props?.match?.params?.slug) {
      setLoader(true);
      DashboardApi.extractDetail(
        props?.match?.params?.slug,
        localStorage.getItem("neswell_access_token")
      ).then((json) => {
        if (!json?.data?.error) {
          setTimeout(
            () =>
              setUser({
                name: json?.data?.name,
                reservation_period: json?.data?.reservation_period,
                CBD: json?.data?.CBD,
                THC: json?.data?.THC,
                pricePerkg: json?.data?.price_per_kg,
                SamplesInventory: json?.data?.samples_inventory,
                SampleUsed: json?.data?.sample_used,
                extractInventory: json?.data?.extract_inventory,
                extractInventoryused: json?.data?.extract_inventory_used,
                supplierCompanyName: json?.data?.supplier_Company_nm,
                supplierInventory: json?.data?.supplier_inventory,
                color: json?.data?.color_code,
              }),
            1000
          );
          setName(json?.data?.name);
          setReservationPeriod(json?.data?.reservation_period);
          setPriceunit(json?.data?.price_per_kg);
          setSelectCurrencyName(json?.data?.price_per_kg_lable);
          setCBD(json?.data?.CBD);
          setImage(json?.data?.CoA?.split("ext_extracts/")[1]);
          setThc(json?.data?.extract_THC_precentage);
          setSampleInventory(json?.data?.samples_inventory);
          setSampleUsed(json?.data?.sample_used);
          setTotalInventory(json?.data?.total_inventory);
          setExtractInventory(json?.data?.extract_inventory);
          setExtractInventoryUsed(json?.data?.extract_inventory_used);
          setTotalExtractInventory(json?.data?.total_Inventory);
          setSupplierLocation(json?.data?.supplier_inventory_location);
          setExtractLocation(json?.data?.extract_inventory_location);
          setSampleLocation(json?.data?.sample_inventory_location);
          setSupplierValue(json?.data?.supplier_id);
          if (json?.data?.color_code == null || " ") {
            setColor({
              hex: "#000000",
              hsv: { h: 0, s: 0, v: 0, a: undefined },
              rgb: { r: 0, g: 0, b: 0, a: undefined },
            });
          } else {
            setColor(JSON.parse(json?.data?.color_code));
          }
          if (json?.data?.sample_inventory_location) {
            let [lat, long] = json?.data?.sample_inventory_location.split(",");
            setSampleInventoryLat(lat);
            setSampleInventoryLng(long);
          }
          if (json?.data?.extract_inventory_location) {
            let [lat, long] = json?.data?.extract_inventory_location.split(",");
            setExtraInventoryLat(lat);
            setExtraInventoryLng(long);
          }
          if (json?.data?.supplier_inventory_location) {
            let [lat, long] =
              json?.data?.supplier_inventory_location.split(",");
            setSupplierLat(long);
            setSupplierLng(lat);
          }
          {
            json?.data?.supplier_inventory_availability &&
              setData((prevState) => ({
                ...prevState,
                inv_ava_data: new Date(
                  `Fri ${moment(
                    json?.data?.supplier_inventory_availability
                  ).format(
                    "MMM DD YYYY"
                  )} 13:27:30 GMT+0530 (India Standard Time)`
                ),
              }));
          }
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
    /**
     * This function fetch supplier's data from API
     */
    DashboardApi.fetchSupplierData(
      localStorage.getItem("neswell_access_token")
    ).then(async (json) => {
      if (!json?.data?.error) {
        await json?.data?.map((item) => {
          setSupplierName((prevItems) => [
            ...prevItems,
            {
              value: item?.id,
              label: item?.supplier_Firm_name,
            },
          ]);
        });
        setLoader(false);
      }
    });
  }, []);

  useEffect(() => {
    reset(user);
  }, [user]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  /**
   * This function manage color picker
   */
  const handleClick = (e) => {
    e.preventDefault();
    setDisplayColorPicker(!displayColorPicker);
  };

  /**
   * This function manage required filed to validate that filed
   */
  const handlePriceChange = (value, name, e) => {
    if (name == "pricePerkg") {
      setPriceunit(value);
    }
    if (name == "getPhoto") {
      setImage(value.split("\\")[2]);
      if (
        value.split(".").pop() != "png" &&
        value.split(".").pop() != "pdf" &&
        value.split(".").pop() != "jpeg" &&
        value.split(".").pop() != "jpg"
      ) {
        setExtantionError(true);
      } else {
        setExtantionError(false);
      }
    }

    if (name == "SamplesInventory") {
      setSampleInventory(value);
      setTotalInventory(value - sampleUsed);
      if (parseInt(value) <= parseInt(sampleUsed)) {
        setSampleGraterThenError(true);
      } else {
        setSampleGraterThenError(false);
      }
    }
    if (name == "SampleUsed") {
      setSampleUsed(value);
      setTotalInventory(smpleInventory - value);
      if (parseInt(smpleInventory) <= parseInt(value)) {
        setSampleGraterThenError(true);
      } else {
        setSampleGraterThenError(false);
      }
    }
    if (name == "extractInventory") {
      setExtractInventory(value);
      setTotalExtractInventory(value - extractInventoryUsed);
      if (parseInt(value) <= parseInt(extractInventoryUsed)) {
        setExtractGraterThenError(true);
      } else {
        setExtractGraterThenError(false);
      }
    }
    if (name == "extractInventoryused") {
      setExtractInventoryUsed(value);
      setTotalExtractInventory(extractInventory - value);
      if (parseInt(extractInventory) <= parseInt(value)) {
        setExtractGraterThenError(true);
      } else {
        setExtractGraterThenError(false);
      }
    }
    if (name == "name") {
      setName(value);
    }
    if (name == "reservation_period") {
      setReservationPeriod(value);
    }
    if (name == "pricePerkg") {
      setPriceunit(value);
    }
    if (name == "CBD") {
      setCBD(value);
    }
    if (name == "THC") {
      setThc(value);
    }
    if (name == "getPhoto") {
      setEditGetPhoto(value);
    }
  };
  /**
   * This function manage  sellect Doller dorp down API
   */
  const handleDropdownChangeForDoller = (e) => {
    let currencyName = e;
    setSelectCurrencyName(currencyName);
    fetch(
      `https://v6.exchangerate-api.com/v6/5aa29f0b27867dda0d762fae/latest/USD`
    )
      .then((res) => res.json())
      .then((json) => {
        let finalRate = priceUnit * json?.conversion_rates?.[e.toUpperCase()];
        setPriceunit(finalRate);
      });
  };
  /**
   * This function use for when user upload file
   */
  const getPhoto = (e) => {
    e.preventDefault();
    let file = e.target.files[0];
    setImage(file);
  };
  /**
   * This function collect extract details in form data in json formant
   * And send to API and get positive and error response from API
   */
  const onSubmit = async (data, event) => {
    event.preventDefault();
    let supplierInventoryLocationlocation = { supplierLat, supplierLng };
    let smpleInventoryLocationlocation = {
      sampleInventoryLat,
      sampleInventorylng,
    };
    let extractInventoryLocationlocation = {
      extraInventoryLat,
      extraInventoryLng,
    };
    let supplierLocation = [
      supplierInventoryLocationlocation?.supplierLng,
      supplierInventoryLocationlocation?.supplierLat,
    ];
    let smpleLocation = [
      smpleInventoryLocationlocation?.sampleInventoryLat,
      smpleInventoryLocationlocation?.sampleInventorylng,
    ];
    let extractLocation = [
      extractInventoryLocationlocation?.extraInventoryLat,
      extractInventoryLocationlocation?.extraInventoryLng,
    ];
    const fileunique = data?.getPhoto[0];
    setLoader(true);
    var formData1 = new FormData();
    formData1.append("name", data?.name);
    formData1.append("user_id", 5);
    formData1.append("CoA", fileunique);
    formData1.append("profile_name", "");
    formData1.append("inventery", 0);
    formData1.append("id", props?.match?.params?.slug);
    formData1.append("CBD", data?.CBD);
    formData1.append("THC", data?.THC);
    formData1.append("price_per_kg", priceUnit);
    formData1.append("is_active", 1);
    formData1.append("reservation_period", reservationPeriod);
    formData1.append("samples_inventory", parseInt(smpleInventory));
    formData1.append("sample_used", parseFloat(sampleUsed));
    formData1.append("sample_inventory_location", smpleLocation.toString());
    formData1.append("extract_inventory", data?.extractInventory);
    formData1.append(
      "extract_inventory_used",
      extractInventoryUsed && parseFloat(extractInventoryUsed)
    );
    formData1.append("extract_inventory_location", extractLocation.toString());
    formData1.append("supplier_id", supplierValue?.value);
    formData1.append("supplier_inventory", data?.supplierInventory);
    formData1.append(
      "supplier_inventory_location",
      supplierLocation.toString()
    );
    formData1.append("color_code", JSON.stringify(color));
    formData1.append("price_per_kg_lable", selectCurrencyName);
    formData1.append("total_sample", totalInventory);
    formData1.append("total_Inventory", totalExtractInventory);
    formData1.append(
      "supplier_inventory_availability",
      moment(Data?.inv_ava_data).format("YYYY-MM-DD")
    );
    if (props?.match?.params?.slug) {
      if (!extantionError) {
        DashboardApi.extractEditRecord(
          formData1,
          props?.match?.params?.slug,
          localStorage.getItem("neswell_access_token")
        ).then((json) => {
          if (!json?.data?.error) {
            history.push(`/view-extract/${props?.match?.params?.slug}`);
            setTimeout(() => {
              setLoader(false);
            }, 1000);
          } else {
            setTimeout(() => {
              setLoader(false);
            }, 1000);
          }
        });
      } else {
        setLoader(false);
        setCoAError("Please upload only png,jpeg,jpg and pdf file");
      }
    }
    if (!props?.match?.params?.slug) {
      if (!extantionError) {
        DashboardApi.CreateExtractRecord(
          formData1,
          localStorage.getItem("neswell_access_token")
        )
          .then(async (json) => {
            if (json?.message == "Record added sucessfully") {
              history.push("/users-extract");
              setLoader(false);
            } else {
              setError(json?.data?.error);
              setLoader(false);
            }
          })
          .catch((e) => {
            setError(e.response.data.error);
            setLoader(false);
          });
      } else {
        setLoader(false);
        setCoAError("Please upload only png,jpeg,jpg and pdf file");
      }
    }
  };
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  /**
   * This functions manage map location
   * When user pin on map location
   * */
  const _onClick = (value) => {
    setSampleInventoryLat(value?.lat);
    setSampleInventoryLng(value?.lng);
  };

  const _onClickExtraInventoryMap = (value) => {
    setExtraInventoryLng(value?.lng);
    setExtraInventoryLat(value?.lat);
  };

  const _onClickSupplierInventoryMap = (value) => {
    setSupplierLng(value?.lng);
    setSupplierLat(value?.lat);
  };

  return (
    <Layout>
      {loader && <Loader />}
      <form
        method="post"
        onSubmit={handleSubmit(onSubmit)}
        onChange={(e) => handlePriceChange(e.target.value, e.target.name, e)}
      >
        <div className="add-users-details-wrapper">
          <div className="users-title create-new-extract-header">
            <h2>Create New Extract</h2>
            <div className="create-extract-header">
              <Link className="icon-gred-box">
                {" "}
                <FiSave onClick={handleSubmit(onSubmit)} />{" "}
              </Link>
              <Link className="icon-gred-box">
                {" "}
                <AiOutlineClose
                  onClick={() =>
                    history.push(
                      props?.match?.params?.slug
                        ? `/view-extract/${props?.match?.params?.slug}`
                        : `/users-extract`
                    )
                  }
                />{" "}
              </Link>
            </div>
          </div>
          <div className="create-extract-bottom-tag">
            <h2>Information</h2>
          </div>
          <div className="add-user-form-wrapper">
            <div className="add-user-form">
              <div className="form-group">
                <span className="form-group-lable">Extract Number</span>
                <input
                  className={"add-user-input"}
                  type="text"
                  value={name}
                  name="name"
                  placeholder="Enter Extract Number"
                  {...register("name", { required: true, maxLength: 150 })}
                />
                {errors?.name && (
                  <p style={{ color: "red" }}>Please enter extract name</p>
                )}
              </div>
              <div className="form-group">
                <span className="form-group-lable">
                  Reservation Period (Days)
                </span>
                <input
                  className={"add-user-input"}
                  type="number"
                  placeholder="Enter Reservation Period"
                  name="reservation_period"
                  value={reservationPeriod}
                  {...register("reservation_period", {
                    required: false,
                    maxLength: 150,
                  })}
                />
              </div>
              <div className="form-group">
                <span className="form-group-lable">Price Per Kilogram</span>
                <div className="select-mg-price">
                  <input
                    className={"add-user-input"}
                    type="number"
                    placeholder="00.00"
                    name="pricePerkg"
                    value={priceUnit}
                    {...register("pricePerkg", {
                      required: props?.match?.params?.slug ? false : true,
                      maxLength: 150,
                    })}
                  />
                  <select
                    name="currency"
                    id="currency"
                    value={selectCurrencyName}
                    onChange={(e) =>
                      handleDropdownChangeForDoller(e.target.value)
                    }
                  >
                    <option value="usd">USD</option>
                    <option value="ils">SHEKEL</option>
                    <option value="eur">EURO</option>
                    <option value="gbp">POUND</option>
                  </select>
                </div>
                {errors?.pricePerkg && (
                  <p style={{ color: "red" }}>
                    Please enterr the prise of kilogram
                  </p>
                )}
              </div>
              <div className="form-group">
                <span className="form-group-lable">Color Code</span>
                <div style={{ display: "flex" }}>
                  <input
                    className="add-user-input"
                    type="text"
                    placeholder="Choose Color"
                    value={color.hex}
                    disabled={true}
                  />
                  <div ref={wrapperRef} className="color-picker">
                    <button onClick={handleClick}>
                      <MdInvertColors />
                    </button>
                    {displayColorPicker && (
                      <div
                        style={{
                          position: "absolute",
                          right: 0,
                          zIndex: "2",
                        }}
                      >
                        {" "}
                        <ColorPicker
                          width={456}
                          height={228}
                          color={color}
                          onChange={setColor}
                          hideHSV
                          dark
                        />
                        <a>okay</a>
                      </div>
                    )}
                  </div>
                </div>
                <div></div>
              </div>
              <div className="form-group">
                <span className="form-group-lable">CBD %</span>
                <input
                  className={"add-user-input"}
                  type="number"
                  placeholder="Enter Value"
                  onChange={(e) => handleChange(e)}
                  value={cbd}
                  name="CBD"
                  {...register("CBD", { required: false, maxLength: 150 })}
                />
              </div>
              <div className="form-group">
                <span className="form-group-lable">
                  CoA (Certificate of Analysis)
                </span>
                <div className="choose-file-wrapper">
                  <div className="choose-image">
                    <input
                      className={"choose-file-input"}
                      type="file"
                      placeholder="Enter Price"
                      onChange={() => getPhoto()}
                      {...register("getPhoto", {
                        required: false,
                      })}
                    />
                    <span className="choose-file-name">Choose Certificate</span>
                  </div>
                  <label className={"file-name-provider"}>
                    {image ? image : "Please choose the Certificate"}
                  </label>
                </div>
                {!props?.match?.params?.slug && errors?.getPhoto && (
                  <p style={{ color: "red" }}>Please upload the certificate</p>
                )}
                {extantionError && <p style={{ color: "red" }}>{coaError}</p>}
              </div>
              <div className="form-group">
                <span className="form-group-lable">THC %</span>
                <input
                  className={"add-user-input"}
                  type="number"
                  placeholder="Enter Value"
                  name="THC"
                  value={thc}
                  {...register("THC", { required: false, maxLength: 150 })}
                />
              </div>
            </div>
          </div>
          <div className="samples-inventory-wrapper">
            <div className="create-extract-bottom-tag">
              <h2>Inventory</h2>
            </div>
            <div className="samples-inventory-title">
              <h2>- Samples</h2>
            </div>
            <div className="samples-inventory-row">
              <div className="samples-inventory-column">
                <div className="form-group">
                  <span className="form-group-lable">Samples Inventory</span>
                  <input
                    className={"add-user-input"}
                    type="number"
                    value={smpleInventory}
                    name="SamplesInventory"
                    placeholder="Enter Samples Inventory"
                    {...register("SamplesInventory", {
                      required: false,
                      maxLength: 20,
                    })}
                  />
                </div>
                <div className="form-group">
                  <span className="form-group-lable">Sample used</span>
                  <input
                    className={"add-user-input"}
                    type="number"
                    value={sampleUsed}
                    name="SampleUsed"
                    placeholder="Enter Sample used"
                    {...register("SampleUsed", {
                      required: false,
                      maxLength: 20,
                    })}
                  />
                  {sampleGraterThenError && (
                    <p style={{ color: "red" }}>
                      Sample used should be less then the sample inventory
                    </p>
                  )}
                  {errors?.SampleUsed && (
                    <p style={{ color: "red" }}>Please fill the SampleUsed</p>
                  )}
                </div>
              </div>
              <div className="samples-inventory-column">
                <div className="form-group">
                  <span className="form-group-lable">
                    Sample inventory location
                  </span>
                  {sampleInventoryLat && sampleInventorylng && (
                    <div
                      style={{ height: "166px", width: "100%" }}
                      className="google-map"
                    >
                      <MyGoogleMap
                        onClickMap={(value) => _onClick(value)}
                        lat={sampleInventoryLat}
                        lng={sampleInventorylng}
                        setLat={(value) => setSampleInventoryLat(value)}
                        setLng={(value) => setSampleInventoryLng(value)}
                        value={(value) => setAddress(value)}
                        setAddresName={(value) => setAddressSampleName(value)}
                        addressName={addressSampleName}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="samples-inventory-wrapper">
            <div className="samples-inventory-title">
              <h2>- Extract</h2>
            </div>
            <div className="samples-inventory-row">
              <div className="samples-inventory-column">
                <div className="form-group">
                  <span className="form-group-lable">Extract Inventory</span>
                  <input
                    className={"add-user-input"}
                    type="number"
                    value={extractInventory}
                    name="extractInventory"
                    placeholder="Enter Extract Inventory"
                    {...register("extractInventory", {
                      required: false,
                      maxLength: 150,
                    })}
                  />
                </div>
                <div className="form-group">
                  <span className="form-group-lable">
                    Extract Inventory used
                  </span>
                  <input
                    className={"add-user-input"}
                    type="number"
                    value={extractInventoryUsed}
                    name="extractInventoryused"
                    placeholder="Enter Extract Inventory used"
                    {...register("extractInventoryused", {
                      required: false,
                      maxLength: 150,
                    })}
                  />
                  {extractGraterThenError && (
                    <p style={{ color: "red" }}>
                      Extract inventory used is should be lessthen extract
                      inventory
                    </p>
                  )}
                  {errors?.extractInventoryused && (
                    <p style={{ color: "red" }}>
                      Please fill the extractInventoryused
                    </p>
                  )}
                </div>
              </div>
              <div className="samples-inventory-column">
                <div className="form-group">
                  <span className="form-group-lable">
                    Extract Inventory location
                  </span>
                  {extraInventoryLat && extraInventoryLng && (
                    <div
                      style={{ height: "166px", width: "100%" }}
                      className="google-map"
                    >
                      <MyGoogleMap
                        onClickMap={(value) => _onClickExtraInventoryMap(value)}
                        lat={extraInventoryLat}
                        lng={extraInventoryLng}
                        setLat={(value) => setExtraInventoryLat(value)}
                        setLng={(value) => setExtraInventoryLng(value)}
                        value={(value) => setAddress(value)}
                        setAddresName={(value) => setAddressExtractName(value)}
                        addressName={addressExtractName}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="samples-inventory-wrapper">
            <div className="samples-inventory-title">
              <h2>Supplier</h2>
            </div>
            <div className="samples-inventory-row">
              <div className="samples-inventory-column">
                <div className="form-group">
                  <span className="form-group-lable">
                    {" "}
                    Supplier Company Name
                  </span>
                  <Select
                    placeholder="Select supplier company name"
                    options={supplierName}
                    isSearchable={false}
                    noOptionsMessage={() => null}
                    onMenuClose
                    styles={customSelectStyles}
                    value={supplierValue}
                    onChange={(value) => {
                      setSupplierValue(value);
                    }}
                  />
                </div>
                <div className="form-group">
                  <span className="form-group-lable">Supplier Inventory</span>
                  <input
                    className={"add-user-input"}
                    type="number"
                    name="supplierInventory"
                    placeholder="Enter Supplier Inventory"
                    {...register("supplierInventory", {
                      required: false,
                      maxLength: 150,
                    })}
                  />
                </div>
                <div className="form-group">
                  <span className="form-group-lable">
                    Supplier Inventory Availability
                  </span>
                  <DatePicker
                    calendarIcon={<MdDateRange />}
                    onChange={(e) => setData({ inv_ava_data: e })}
                    value={Data.inv_ava_data}
                    format={"dd-MM-yyyy"}
                    onKeyDown={(e) => {
                      e.preventDefault();
                    }}
                  />
                </div>
              </div>
              <div className="samples-inventory-column">
                <div className="form-group">
                  <span className="form-group-lable">
                    Supplier Inventory location
                  </span>
                  {supplierLat && supplierLng && (
                    <div
                      style={{ height: "166px", width: "100%" }}
                      className="google-map"
                    >
                      <MyGoogleMap
                        onClickMap={(value) =>
                          _onClickSupplierInventoryMap(value)
                        }
                        lat={supplierLat}
                        lng={supplierLng}
                        setLat={(value) => setSupplierLat(value)}
                        setLng={(value) => setSupplierLng(value)}
                        value={(value) => setAddress(value)}
                        setAddresName={(value) => setAddressExtractName(value)}
                        addressName={addressSupplierName}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          {props?.match?.params?.slug ? (
            <div className="extract-submit-btn">
              {error && <p style={{ color: "red" }}>{error}</p>}
              <button className=" primary-btn btn" type="submit">
                Save
              </button>
              <button
                className="primary-btn btn"
                type="submit"
                onClick={() =>
                  history.push(`/view-extract/${props?.match?.params?.slug}`)
                }
              >
                Cancel
              </button>
            </div>
          ) : (
            <div className="form-submit-btn">
              {error && <p style={{ color: "red" }}>{error}</p>}
              <button className=" primary-btn" type="submit">
                Save
              </button>
            </div>
          )}
        </div>
      </form>
    </Layout>
  );
}

export default CreateNewExtract;
