import React, { useState, useEffect, useRef } from "react";
import "./users.style.scss";
import { FiEdit } from "react-icons/fi";
import { RiDeleteBin5Fill } from "react-icons/ri";
import { GrPowerReset } from "react-icons/gr";
import Layout from "../../Layout";
import ProfileIcon from "../../assets/images/user-profile-icon.svg";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import DashboardApi from "../../services/DashboardApi";
import { useHistory } from "react-router-dom";
import Modal from "../../components/ModalComponent";
import { API_URL } from "../../config";
import Loader from "../../components/loader";
import MyGoogleMap from "../../components/MyGoogleMap";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

/**
 * This function manages user Profile Details
 */

function UserProfile(props) {
  const { data } = props;
  const [userData, setUserData] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [loader, setLoader] = useState(false);
  let history = useHistory();

  /**
   * When user clicked outside of the alert popup box this function executed
   * The function manages mouse event
   */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setModalOpen(false);
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  /**
   * This function manage specific user records from API and display in UI
   */
  useEffect(() => {
    setLoader(true);
    DashboardApi.UserDetails(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug
    ).then((json) => {
      if (!json?.data?.error) {
        setUserData(json?.data);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }, []);

  /**
   * When user clicked delete button ,this function manage user delete feature
   * The function called delete Api with user's id
   */
  const handeleDeleteClicked = () => {
    setLoader(true);
    setModalOpen(false);
    DashboardApi.UserDelete(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug
    ).then((json) => {
      if (!json.data.error) {
        history.push("/users");
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  };

  /**
   * The function manage reset password feature
   * The function send re-set password link to user's email address
   */
  const handleResetPassword = async () => {
    setLoader(true);
    DashboardApi.userResetPassword(
      props?.match?.params?.slug,
      localStorage.getItem("neswell_access_token")
    ).then((json) => {
      if (!json.data.error) {
        setLoader(false);
        setModalOpen(true);
      } else {
        setLoader(false);
      }
    });
  };

  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);
  return (
    <Layout>
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => {
          deleteModal ? handeleDeleteClicked() : setModalOpen(false);
        }}
        title={"Are you sure?"}
        subTitle={
          deleteModal
            ? "Do you want to delete this record?"
            : "We have emailed user password reset link!"
        }
        delete={deleteModal}
      />
      {loader && <Loader />}
      <div className="add-users-details-wrapper">
        <div className="users-title">
          <h2>User Profile</h2>
        </div>
        <div className="profile-wrapper">
          <div className="show-profile-details">
            <div className="profile-img">
              {!userData?.user_photo ? (
                <img src={ProfileIcon} alt="" />
              ) : (
                <img src={API_URL + userData?.user_photo} alt="" />
              )}
            </div>
            <div className="user-name-number">
              <div className="user-name">
                <h2>{userData?.username}</h2>
                <span
                  className={
                    userData?.permissions === "Uploader"
                      ? "yellow-status"
                      : userData?.permissions === "Admin"
                      ? "purple-status"
                      : userData?.permissions === "Editor" && "green-status"
                  }
                >
                  {userData?.permissions}
                </span>
              </div>
              <div className="number phone-react-view">
                <span>Mobile Number</span>
                {userData?.mobile_No == "" ? (
                  <p>--</p>
                ) : (
                  <PhoneInput
                    className="react-phone"
                    value={userData?.mobile_No}
                    disabled={true}
                  />
                )}
              </div>
            </div>
            <div className="mail-details">
              <div>
                <span>Email</span>
                <p>{userData?.email}</p>
              </div>
              <div>
                <span className="top-space">User Role</span>
                <p>{userData?.user_role}</p>
              </div>
            </div>
          </div>

          <div className="edit-profile-details">
            <div className="edit-icons">
              <Link
                onClick={() =>
                  history.push(`/add-users/${props?.match?.params?.slug}`)
                }
              >
                {" "}
                <FiEdit />{" "}
              </Link>
              <Link
                onClick={() => {
                  setDeleteModal(true);
                  setModalOpen(true);
                }}
              >
                {" "}
                <RiDeleteBin5Fill />{" "}
              </Link>
            </div>
            <div className="reset-btn">
              <button
                onClick={() => handleResetPassword()}
                className="reset-btn"
              >
                {" "}
                <GrPowerReset /> Reset Password{" "}
              </button>
            </div>
          </div>
        </div>
        {userData?.user_latitude && userData?.user_longitude && (
          <div className="mail-details">
            <span>User Location</span>
            <div className="add-user-map">
              <div className="form-group">
                <div
                  className="google-map"
                  style={{ height: 250, width: "100%" }}
                >
                  <MyGoogleMap
                    lat={userData?.user_latitude || "22.9863"}
                    lng={userData?.user_longitude || "72.6305"}
                    edit={true}
                    viewProfile={true}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
}

/**
 * This function use for get data from redux store
 */
const mapStateToProps = (state) => ({
  data: state.homeReducer.data,
});

export default connect(mapStateToProps, null)(UserProfile);
