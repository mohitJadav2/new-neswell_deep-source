import React, { useState, useEffect } from "react";
import Select from "react-select";
import "./medicalIndication.style.scss";
import { AiOutlinePlusCircle } from "react-icons/ai";
import Layout from "../../Layout";
import { Link } from "react-router-dom";
import MedicalIndicationApi from "../../services/MedicalIndicationApi";
import { useHistory } from "react-router-dom";
import Loader from "../../components/loader";

/** this style is use for the selection fild */
const customSelectStyles = {
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? "#fff" : "#404040",
    padding: 10,
    borderRadius: 10,
    width: "98%",
    margin: "10px auto",
  }),
  control: (styles) => ({
    ...styles,
    border: "solid 1px #E5E5E5",
    backgroundColor: "white",
    minHeight: 52,
    borderRadius: 10,
    color: "#929292",
    fontWeight: "600",
  }),
};

function CreateMedicalIndication(props) {
  const [medicalIndiacationData, setMedicalIndiacationData] =
    useState(undefined);
  const [userId, setUserId] = useState(undefined);
  const [name, setName] = useState("");
  const [loader, setLoader] = useState(false);
  const [apiError, setApiError] = useState(undefined);
  let history = useHistory();
  const [perPage, setPerPage] = useState(10);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(undefined);
  const [totalPages, setTotalPages] = useState(undefined);
  const [subBioTest, setSubBioTest] = useState([]);
  const [subBioTestName, setSubBioTestName] = useState([]);
  const [error, setError] = useState(false);
  const [nameError, setNameError] = useState(undefined);

  const handleSubmit = async () => {
    if (props?.match?.params?.slug) {
      let formData = {};
      formData["sub_bio_tests"] = subBioTestName;
      formData["user_id"] = userId;
      formData["name"] = name;
      await MedicalIndicationApi.AddDuplicateMedicalIndication(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug,
        formData
      ).then((json) => {
        if (json?.header?.status === 200 || json?.header?.status === 201) {
          history.push(
            `/medical-indication-view/${json?.data?.response?.messages}`
          );
          setLoader(false);
        } else {
          setApiError(json?.data?.error);
          setLoader(false);
        }
      });
    } else {
      if (name) {
        let formData = {};
        formData["sub_bio_tests"] = subBioTestName;
        formData["user_id"] = userId;
        formData["name"] = name;
        await MedicalIndicationApi.AddMedicalIndication(
          localStorage.getItem("neswell_access_token"),
          formData
        ).then((json) => {
          if (json?.header?.status === 200 || json?.header?.status === 201) {
            history.push(
              `/medical-indication-view/${json?.data?.response?.messages}`
            );
            setLoader(false);
          } else {
            setApiError(json?.data.error);
            setLoader(false);
          }
        });
      } else {
        setError(true);
        setNameError("Please enter medical indication name");
      }
    }
  };

  useEffect(() => {
    MedicalIndicationApi.MedicalIndicationData(
      localStorage.getItem("neswell_access_token"),
      page,
      perPage
    ).then((res) => {
      setMedicalIndiacationData(res?.data?.results);
      let totalPages = res?.data?.count / perPage;
      setTotalPages(Math.ceil(totalPages));
      setTotalCount(res?.data?.count);
    });

    MedicalIndicationApi.SelectSubBioTest(
      localStorage.getItem("neswell_access_token")
    ).then(async (json) => {
      if (!json?.data?.error) {
        await json?.data?.results?.map((item) =>
          setSubBioTest((prevItems) => [
            ...prevItems,
            {
              value: item?.value,
              label: item?.label,
            },
          ])
        );
      }
    });

    props?.match?.params?.slug &&
      MedicalIndicationApi.DuplicateMedicalIndication(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug
      ).then(async (json) => {
        if (!json?.data?.error) {
          await json?.data?.sub_bio_tests?.map((item) =>
            setSubBioTestName((prevItems) => [
              ...prevItems,
              {
                value: item?.value,
                label: item?.label,
              },
            ])
          );
        }
      });
  }, []);

  return (
    <Layout>
      {loader && <Loader />}
      <div className="add-users-details-wrapper">
        <div className="users-title">
          <h2>Create Medical Indication</h2>
        </div>
        <div className="add-user-form-wrapper">
          <div className="create-bio-form">
            <div className="form-group ">
              <span>Medical Indication Name</span>
              <input
                style={{ width: "100%", fontSize: "16px", color: "#404040" }}
                placeholder="Enter Medical Indication Name"
                className="css-1mizc5h-control medical-input-box"
                onChange={(e) => setName(e.target.value)}
              />
              {error && <p style={{ color: "red" }}>{nameError}</p>}
            </div>
            <div className="form-group">
              <span>Bio-Test</span>
              <Select
                value={subBioTestName}
                isMulti
                onChange={(value) => setSubBioTestName(value)}
                closeMenuOnSelect={false}
                styles={customSelectStyles}
                placeholder="Enter Child Bio Test Name"
                options={subBioTest}
              />
            </div>
          </div>
          <p className="btn-top-or-text">Or</p>
          <div className="create-medical-indication-btn">
            <Link to="/create-bio">
              {" "}
              <AiOutlinePlusCircle /> Create Sub Bio Test
            </Link>
          </div>
          <div className="form-submit-btn">
            <button onClick={() => handleSubmit()} className="primary-btn">
              Create
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default CreateMedicalIndication;
