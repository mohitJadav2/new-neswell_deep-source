import React, { useEffect, useState } from "react";
import MedicalIndicationApi from "../../services/MedicalIndicationApi";
import Table from "../../components/Table";

function Score(props) {
  const id = props?.match?.params?.slug;
  const [loader, setLoader] = useState(false);
  const [columns, setColumns] = useState([]);
  const [records, setRecords] = React.useState();
  const [extractNo, setExtractNo] = useState(undefined);
  const [errorModelOpen, setErrorModelOpen] = useState(false)
  const [errorMessage,setErrorMessage] = useState(undefined)

  /**
   * This function manage score table row and columns data
   * Get row and columns data from API and pass data using props to react-table component
   */
  useEffect(() => {
    setLoader(true);
    MedicalIndicationApi.MedicalIndicationScore(
      localStorage.getItem("neswell_access_token"),
      id
    ).then((res) => {
      if (!res?.data?.error) {
        setRecords(res?.data?.sbt_extract_data);
        setExtractNo(res?.data?.mi_no);
        setColumns(res?.data?.features_names);
        setLoader(false);
      } else {
        setLoader(false);
        setErrorMessage(res?.data?.error?.messages)
        setErrorModelOpen(true)
      }
    });
  }, []);
  return (
    <Table
      columns={columns}
      row={records}
      id={id}
      mi_no={extractNo}
      errorMrssage={errorMessage} 
      model={errorModelOpen}
      loader={loader}
    />
  );
}

export default Score;
