import React, {useState,useEffect, useRef} from 'react'
// import { useState,useEffect, useRef } from "react";
import { ColorPicker, useColor } from "react-color-palette";
import { MdInvertColors } from 'react-icons/md'
function ColorPiker() {

    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const [pricePerkg, setpricePerkg] = useState();
    const [color, setColor] = useColor("hex", "#121212");
    const handleClick = (e) => {
        e.preventDefault();
        setDisplayColorPicker(!displayColorPicker);
      };

      const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);
  function useOutsideAlerter(ref) {
    useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setDisplayColorPicker(false);
        }
      }
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }
  return (
    <div ref={wrapperRef} className="color-picker">
    <button onClick={handleClick}>
      <MdInvertColors />
    </button>
    {displayColorPicker && (
      <div
        style={{
          position: "absolute",
          right: 0,
          zIndex: "2",
        }}
      >
        {" "}
        <ColorPicker
          width={456}
          height={228}
          color={color}
          onChange={setColor}
          hideHSV
          dark
        />
        <a>okay</a>
      </div>
    )}
  </div>
  )
}

export default ColorPiker