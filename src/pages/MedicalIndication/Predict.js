import React, { useEffect, useState } from "react";
import MedicalIndicationApi from "../../services/MedicalIndicationApi";
import Table from "../../components/PredictTable";

function Predict(props) {
  const id = props?.match?.params?.slug;
  const [loader, setLoader] = useState(false);
  const [columns, setColumns] = useState([]);
  const [records, setRecords] = React.useState();
  const [errorModelOpen, setErrorModelOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(undefined);
  const [extractNo, setExtractNo] = useState(undefined);

  /**
   * This function manage predict table row and columns data
   * Get row and columns data from API and pass data using props to react-table component
   */
  useEffect(() => {
    setLoader(true);
    MedicalIndicationApi.MedicalIndicationPredicat(
      localStorage.getItem("neswell_access_token"),
      id
    ).then((res) => {
      if (!res?.data?.error) {
        setRecords(res?.data?.extract_data);
        setExtractNo(res?.data?.mi_no);
        setColumns(res?.data?.features_names);
        setLoader(false);
      } else {
        setLoader(false);
        setErrorMessage(res?.data?.error?.messages);
        setErrorModelOpen(true);
      }
    });
  }, []);
  return (
    <Table
      columns={columns}
      row={records}
      id={id}
      mi_no={extractNo}
      errorMrssage={errorMessage}
      model={errorModelOpen}
      loader={loader}
    />
  );
}

export default Predict;
