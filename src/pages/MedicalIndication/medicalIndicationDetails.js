import React, { useState, useEffect, useRef } from "react";
import "./medicalIndication.style.scss";
import {
  AiOutlinePlusCircle,
  AiOutlineSetting,
  AiFillCaretUp,
  AiFillCaretDown,
} from "react-icons/ai";
import Layout from "../../Layout";
import { Link, useHistory } from "react-router-dom";
import Pagination from "react-responsive-pagination";
import MedicalIndicationApi from "../../services/MedicalIndicationApi";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";
import { FiSearch } from "react-icons/fi";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { SaveMedicalIndicationSortingData } from "../../Actions/HomeAction";

function MedicalIndicationDetals(props) {
  const [medicalIndiacationData, setMedicalIndiacationData] =
    useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage, setPerPage] = useState(
    props?.medicalIndicationSortingData?.perPage
      ? props?.medicalIndicationSortingData?.perPage
      : 10
  );
  const [orderType, setOrderType] = useState("asc");
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(undefined);
  const [totalPages, setTotalPages] = useState(undefined);
  const [orderField, setOrderField] = useState("id");
  const [searchText, setSearchText] = useState("");
  const [loader, setLoader] = useState(false);
  const [openDropDown, setOpenDropDown] = useState(undefined);
  const [toggleDropDown, setToggleDropDown] = useState(false);
  const [searchError, setSearchError] = useState(false);
  let history = useHistory();
  const perPageValue = [10, 25, 50, 100];

  function handlePageChange(page) {
    let sortingData = {
      orderType: orderType,
      orderField: orderField,
      page: page,
      perPage: perPage,
    };
    props.SaveMedicalIndicationSortingData(sortingData);
    setLoader(true);
    setSearchText(searchText);
    setCurrentPage(page);
    if (searchText) {
      MedicalIndicationApi.SearchMedicalIndicationData(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        searchText,
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setMedicalIndiacationData(json?.data?.results);
          setSearchText(searchText);
          setLoader(false);
          if (json?.data?.length == 0) {
            setSearchError("No records found");
          } else {
            setSearchError(undefined);
          }
        }
      });
    } else {
      MedicalIndicationApi.MedicalIndicationData(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setMedicalIndiacationData(json?.data?.results);
          setCurrentPage(page);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  }
  useEffect(() => {
    setLoader(true);
    props?.medicalIndicationSortingData &&
      setOrderType(props?.medicalIndicationSortingData?.orderType);
    props?.medicalIndicationSortingData &&
      setOrderField(props?.medicalIndicationSortingData?.orderField);
    props?.medicalIndicationSortingData &&
      setCurrentPage(props?.medicalIndicationSortingData?.page);
    props?.medicalIndicationSortingData &&
      setPerPage(
        props?.medicalIndicationSortingData?.perPage
          ? props?.medicalIndicationSortingData?.perPage
          : 10
      );
    MedicalIndicationApi.MedicalIndicationData(
      props?.medicalIndicationSortingData
        ? props?.medicalIndicationSortingData?.orderType
        : orderType,
      props?.medicalIndicationSortingData
        ? props?.medicalIndicationSortingData?.orderField
        : orderField,
      localStorage.getItem("neswell_access_token"),
      props?.medicalIndicationSortingData
        ? props?.medicalIndicationSortingData?.page
        : currentPage,
      props?.medicalIndicationSortingData
        ? props?.medicalIndicationSortingData?.perPage
          ? props?.medicalIndicationSortingData?.perPage
          : 10
        : perPage
    ).then((res) => {
      setMedicalIndiacationData(res?.data?.results);
      let totalPages = res?.data?.count / perPage;
      setTotalPages(Math.ceil(totalPages));
      setTotalCount(res?.data?.count);
      setLoader(false);
    });
  }, []);
  const handeleDeleteClicked = () => {
    setModalOpen(false);
    if (searchText) {
      MedicalIndicationApi.MedicalIndicationDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData
      ).then((json) => {
        MedicalIndicationApi.SearchMedicalIndicationData(
          orderType,
          orderField,
          localStorage.getItem("neswell_access_token"),
          searchText,
          currentPage,
          perPage
        ).then((json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setMedicalIndiacationData(json?.data?.results);
            setSearchText(searchText);
            setLoader(false);
            if (json?.data?.length == 0) {
              setSearchError("No records found");
            } else {
              setSearchError(undefined);
            }
          }
        });
      });
    } else {
      MedicalIndicationApi.MedicalIndicationDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData
      ).then((json) => {
        setLoader(true);
        if (!json?.data?.error) {
          MedicalIndicationApi.MedicalIndicationData(
            orderType,
            orderField,
            localStorage.getItem("neswell_access_token"),
            currentPage,
            perPage
          ).then((res) => {
            if (res?.data?.results?.length <= 0) {
              setLoader(true);
              let sortingData = {
                orderType: orderType,
                orderField: orderField,
                page: currentPage - 1,
                perPage: perPage,
              };
              props.SaveMedicalIndicationSortingData(sortingData);
              MedicalIndicationApi.MedicalIndicationData(
                orderType,
                orderField,
                localStorage.getItem("neswell_access_token"),
                currentPage - 1,
                perPage
              ).then((res) => {
                let totalPages = res?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(res?.data?.count);
                setMedicalIndiacationData(res?.data?.results);
              });
            }
            let totalPages = res?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(res?.data?.count);
            setMedicalIndiacationData(res?.data?.results);
            setModalOpen(false);
            setLoader(false);
          });
        } else {
          setLoader(false);
        }
      });
    }
  };

  const handleDuplicateMedicalIndication = async (id) => {
    history.push({
      pathname: `/create-medical-indication/${id}`,
    });
  };
  /**
   * This function manage popof setting for every user
   */

  const handleSettingClick = async (item) => {
    openDropDown == item?.id
      ? setOpenDropDown(undefined)
      : setOpenDropDown(item?.id);
  };

  /**
   * This function manage click outside of the popup box
   */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenDropDown(undefined);
        }
      }
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }
  /**this ref is for the when user clicked outside the fucntion */
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  /**
   * This function manage search feature
   * This function passes user's enter value to API and get positive and error response
   */
  const handleSearch = (searchText) => {
    setSearchText(searchText);
    setCurrentPage(1);
    MedicalIndicationApi.SearchMedicalIndicationData(
      orderType,
      orderField,
      localStorage.getItem("neswell_access_token"),
      searchText,
      1,
      perPage
    ).then((json) => {
      if (!json?.data?.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setMedicalIndiacationData(json?.data?.results);
        setTotalCount(json?.data?.count);
        setSearchText(searchText);
        if (json?.data?.length == 0) {
          setSearchError("No records found");
        } else {
          setSearchError(undefined);
        }
      }
    });
  };

  /**
   * This function manages record display drop down
   * When user change value of drop down at that time  this function call API with change value
   */
  const handlePerPageChanges = async (perPageNuumber) => {
    let sortingData = {
      orderType: orderType,
      orderField: orderField,
      page: 1,
      perPage: perPageNuumber,
    };
    props.SaveMedicalIndicationSortingData(sortingData);
    setToggleDropDown(false);
    setPerPage(perPageNuumber);
    setCurrentPage(1);
    if (searchText) {
      MedicalIndicationApi.SearchMedicalIndicationData(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        searchText,
        1,
        perPageNuumber
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPageNuumber;
          setTotalPages(Math.ceil(totalPages));
          setMedicalIndiacationData(json?.data?.results);
          setSearchText(searchText);
          setLoader(false);
          if (json?.data?.length == 0) {
            setSearchError("No records found");
          } else {
            setSearchError(undefined);
          }
        }
      });
    } else {
      await MedicalIndicationApi.MedicalIndicationData(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        1,
        perPageNuumber
      ).then((res) => {
        setMedicalIndiacationData(res?.data?.results);
        let totalPages = res?.data?.count / perPageNuumber;
        setTotalPages(Math.ceil(totalPages));
        setTotalCount(res?.data?.count);
        setLoader(false);
      });
    }
  };
  const handleAscending = (fieldName) => {
    setOrderType("asc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      MedicalIndicationApi.SearchMedicalIndicationData(
        "asc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        searchText,
        1,
        perPage
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          let sortingData = {
            orderType: "asc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.SaveMedicalIndicationSortingData(sortingData);
          setMedicalIndiacationData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      MedicalIndicationApi.MedicalIndicationData(
        "asc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        1,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          let sortingData = {
            orderType: "asc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.SaveMedicalIndicationSortingData(sortingData);
          setTotalCount(json?.data?.count);
          setMedicalIndiacationData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };
  const handledescending = (fieldName) => {
    setOrderType("desc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      MedicalIndicationApi.SearchMedicalIndicationData(
        "desc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        searchText,
        1,
        perPage
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          let sortingData = {
            orderType: "desc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.SaveMedicalIndicationSortingData(sortingData);
          setMedicalIndiacationData(json?.data?.results);
          setTotalCount(json?.data?.count);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      MedicalIndicationApi.MedicalIndicationData(
        "desc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        1,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          let sortingData = {
            orderType: "desc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.SaveMedicalIndicationSortingData(sortingData);
          setTotalCount(json?.data?.count);
          setMedicalIndiacationData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };
  const handleViewMedicalIndication = (id) => {
    history.push({
      pathname: `/medical-indication-view/${id}`,
      state: { userDetail: `/medical-indication-view` },
    });
  };
  const handleMedicalIndicationPredict = (id) => {
    history.push(`/medical-indication-predict/${id}`);
  };
  const handleMedicalIndicationScore = (id) => {
    history.push(`/medical-indication-score/${id}`);
  };
  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => handeleDeleteClicked()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={deleteModal}
        error={false}
      />
      <div className="add-users-details-wrapper medical-indication-detail-page">
        <div className="medical-indication-details-wrapper">
          <div className="medical-indication-table users-details-inner-wrapper">
            <div className="user-search">
              <div className="search">
                <button className="search-icon">
                  {" "}
                  <FiSearch />{" "}
                </button>
                <input
                  onChange={(e) => handleSearch(e.target.value)}
                  type="search"
                  placeholder="Search"
                  className="search-input"
                />
              </div>
              <div className="medical-indication-table-title primary-btn">
                <Link to="/create-medical-indication">
                  {" "}
                  <AiOutlinePlusCircle /> Create Medical Indication
                </Link>
              </div>
            </div>
            <div className="user-details-table details-table">
              <table>
                <thead>
                  <tr>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("id")
                            : handleAscending("id");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Sr.
                        <div style={{ marginLeft: 10, height: 22 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "id" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "id" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("name")
                            : handleAscending("name");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Names
                        <div style={{ marginLeft: 10, height: 22 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "name" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "name" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("created")
                            : handleAscending("created");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Created at
                        <div style={{ marginLeft: 10, height: 22 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "created" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "created" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th>
                      <div
                        onClick={() => {
                          orderType == "asc"
                            ? handledescending("id")
                            : handleAscending("id");
                        }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                        }}
                      >
                        Score
                        <div style={{ marginLeft: 10, height: 22 }}>
                          <div style={{ cursor: "pointer", marginBottom: -8 }}>
                            <AiFillCaretUp
                              style={{
                                opacity:
                                  orderField == "id" && orderType == "asc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                          <div style={{ cursor: "pointer", marginTop: -8 }}>
                            <AiFillCaretDown
                              style={{
                                opacity:
                                  orderField == "id" && orderType == "desc"
                                    ? 0.5
                                    : 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </th>
                    <th style={{ cursor: "context-menu" }}>Action</th>
                  </tr>
                </thead>
                <tbody className="table-row">
                  {medicalIndiacationData?.map((item, idx) => {
                    return (
                      <tr>
                        <td
                          onClick={() => {
                            handleViewMedicalIndication(item?.id);
                          }}
                        >{`${item?.id}`}</td>
                        <td
                          onClick={() => {
                            handleViewMedicalIndication(item?.id);
                          }}
                        >{`${item?.name}`}</td>
                        <td
                          onClick={() => {
                            handleViewMedicalIndication(item?.id);
                          }}
                          style={{ textAlign: "left" }}
                        >{`${item?.created}`}</td>
                        <td
                          onClick={() => {
                            handleViewMedicalIndication(item?.id);
                          }}
                          style={{ textAlign: "left" }}
                        >
                          {item?.scored == 1 ? "True" : "False"}
                        </td>
                        <td>
                          <span
                            onClick={() => handleSettingClick(item)}
                            className="blue-color-eyes"
                          >
                            <AiOutlineSetting style={{ color: "#404040" }} />
                            {openDropDown == item?.id && (
                              <div
                                className="user-setting-popup"
                                ref={wrapperRef}
                              >
                                <p
                                  onClick={() => {
                                    handleViewMedicalIndication(item?.id);
                                  }}
                                >
                                  View
                                </p>
                                <p
                                  onClick={() => {
                                    handleMedicalIndicationScore(item?.id);
                                  }}
                                >
                                  Score
                                </p>
                                <p
                                  onClick={() => {
                                    handleMedicalIndicationPredict(item?.id);
                                  }}
                                >
                                  Predict
                                </p>
                                <p
                                  onClick={() => {
                                    handleDuplicateMedicalIndication(item?.id);
                                  }}
                                >
                                  Duplicate
                                </p>
                                <p
                                  onClick={() => {
                                    setDeleteModal(true);
                                    setModalOpen(true);
                                    setDeleteUserData(item?.id);
                                  }}
                                >
                                  Remove
                                </p>
                              </div>
                            )}
                          </span>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              <p style={{ textAlign: "center" }}>{searchError}</p>
              <div className="pagination-wrapper">
                <div className="entries-content">
                  <span>{`Showing 1 to ${medicalIndiacationData?.length} of ${totalCount} entries`}</span>
                </div>
                <div className="pagination">
                  <Pagination
                    total={totalPages}
                    current={currentPage}
                    onPageChange={(page) => handlePageChange(page)}
                  />
                </div>
                <div className="per-pagination-wrapper">
                  <div
                    onClick={() => setToggleDropDown(!toggleDropDown)}
                    className={
                      toggleDropDown ? "selected-page open" : "selected-page "
                    }
                  >
                    <p>{perPage}</p>
                    <div className="up-down-arrow">
                      <span className="down-arrow">
                        <BsChevronDown />
                      </span>
                      <span className="up-arrow">
                        <BsChevronUp />
                      </span>
                    </div>
                  </div>
                  {toggleDropDown && (
                    <ul className="per-page-dropdown">
                      {perPageValue?.map((item) => (
                        <li onClick={() => handlePerPageChanges(item)}>
                          {item}
                        </li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ SaveMedicalIndicationSortingData }, dispatch),
  };
}
const mapStateToProps = (state) => ({
  medicalIndicationSortingData: state.homeReducer.medicalIndicationSortingData,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MedicalIndicationDetals);
