import React, { useState, useEffect } from "react";
import { MdContentCopy } from "react-icons/md";
import { AiOutlineFileSearch, AiOutlineMinus } from "react-icons/ai";
import Layout from "../../Layout";
import { Link, useHistory } from "react-router-dom";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import ScoringGraph from "./ScoringGraph";
import { GrScorecard } from "react-icons/gr";
import "./medicalIndication.style.scss";
import MedicalIndicationApi from "../../services/MedicalIndicationApi";
import Trash from "../../assets/icons/trash.svg";

function MedicalIndicationDetals(props) {
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [supportedExtract, setSupportedExtract] = useState([]);
  const [fatherBioTest, setfatherBioTest] = useState([]);
  let history = useHistory();
  const [medicalIndicationName, setMedicalIndication] = useState(undefined);
  const [subBioTestName, setSubBioTestName] = useState([]);
  const [extractName, setExtractName] = useState([]);
  const [labelScore, setLabelScore] = useState([]);
  const [predictedScore, setPredictedScore] = useState([]);
  const [error, setError] = useState(undefined);
  const [errorMsg, setErrorMsg] = useState(undefined);
  /**
   * This function is use for get sub bio test details from api
   */
  useEffect(() => {
    setLoader(true);
    MedicalIndicationApi.MedicalIndicationView(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug
    ).then((json) => {
      if (!json?.data?.error) {
        setMedicalIndication(json?.data?.medical_indication_name);
        setSubBioTestName(json?.data?.sub_bio_tests);

        if (!json?.data?.score_graph) {
          setExtractName(json?.data?.extract_nms);
          setLabelScore(json?.data?.labeled_score_data);
          setPredictedScore(json?.data?.predicted_score_data);
          setError(false);
        } else {
          setErrorMsg(json?.data?.score_graph?.error?.messages);
          setError(true);
        }
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }, []);

  /**This function is use for delete sub bio test record */
  const handleDeleteChild = () => {
    setLoader(true);
    MedicalIndicationApi.MedicalIndicationDelete(
      localStorage.getItem("neswell_access_token"),
      deleteUserData,
      "DELETE"
    ).then((json) => {
      if (!json?.data?.error) {
        history.push(`/medical-indication-details`);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  };
  /**This function is use for when user clicked on Duplicate sub bio test button and user redirect to create sub bio test page*/
  const handleDuplicateMedicalIndication = (id) => {
    history.push({
      pathname: `/create-medical-indication/${id}`,
      state: { userDetail: "" },
    });
  };
  const handleMedicalIndicationScore = (id) => {
    history.push({
      pathname: `/medical-indication-score/${id}`,
    });
  };
  const handleMediaclIndicationPredict = (id) => {
    history.push({
      pathname: `/medical-indication-predict/${id}`,
    });
  };
  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => handleDeleteChild()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={deleteModal}
      />
      <div className="add-users-details-wrapper view-medical-wrapper med-indi-details-view-wrapper">
        <div className="users-title">
          <h2>Information Section</h2>
          <div className="create-sub-bio-header d-flex">
            <Link className="md-copy-icon icon-gred-box">
              {" "}
              <MdContentCopy
                onClick={() =>
                  handleDuplicateMedicalIndication(props?.match?.params?.slug)
                }
              />{" "}
            </Link>
            <Link
              className="icon-gred-box"
              onClick={() => {
                setDeleteModal(true);
                setModalOpen(true);
                setDeleteUserData(props?.match?.params?.slug);
              }}
            >
              {" "}
              <img src={Trash} alt="trash" className="trash-icon" />
            </Link>
          </div>
        </div>
        <div className="view-bio-details-wrapper">
          <div className="view-bio-details">
            <div className="view-bio-details-item ">
              <div className="view-sbt view-bio-details-item">
                <span>Medical Indication</span>
                <p style={{ borderBottom: "1px solid  #E5E5E5" }}>
                  {medicalIndicationName}
                </p>
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Sub Bio Test</span>
              <div className="clusters-option">
                <p>
                  {subBioTestName?.map((item) => (
                    <span>
                      <AiOutlineMinus />
                      {item?.label}
                    </span>
                  ))}
                </p>
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Scoring Graph</span>
              <div className="clusters-option">
                {!error && (
                  <ScoringGraph
                    className="graph-css"
                    id={props?.match?.params?.slug}
                    extractName={extractName}
                    labeledScoreData={labelScore}
                    predictedScoreData={predictedScore}
                  />
                )}
                {error && <p>{errorMsg}</p>}
              </div>
            </div>
            <div className="view-bio-details-btn">
              <button
                onClick={() =>
                  handleMedicalIndicationScore(props?.match?.params?.slug)
                }
                className="reset primary-btn view-sbt-btn"
              >
                <GrScorecard /> Score
              </button>
              <button
                onClick={() =>
                  handleMediaclIndicationPredict(props?.match?.params?.slug)
                }
                className="reset primary-btn view-sbt-btn"
              >
                <AiOutlineFileSearch /> Predict
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default MedicalIndicationDetals;
