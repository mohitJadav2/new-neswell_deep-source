import React, { useState } from "react";

import Highcharts from "highcharts/highstock";
import Chart from "../SubBioTest/Chart";

function ScoringGraph(props) {
  const [error, setError] = useState(undefined);
  const [errorMsg, setErrorMsg] = useState(undefined);

  const chartOptions = {
    title: {
      text: "",
    },
    xAxis: {
      max: 12,
      categories: props?.extractName,
      visible: true,
      crosshair: true,
      title: {
        text: "Extracts",
      },
      scrollbar: {
        enabled: true,
      },
    },
    yAxis: [
      {
        title: {
          text: "Score",
          style: {
            color: "",
          },
        },
        labels: {
          format: "{value}",
          style: {
            color: "",
          },
        },
      },
    ],
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },

    series: [
      {
        name: "Predicted Score",
        type: "line",
        data: props?.predictedScoreData,
        lineWidth: 0.75,
        color: "orange",
        tooltip: {},
        marker: {
          radius: 5,
        },
      },
      {
        name: "Labeled Score",
        type: "scatter",
        color: "blue",
        data: props?.labeledScoreData,
        tooltip: {
          headerFormat: "{point.x}",
          pointFormat: "</br>Labeled Score: <b>{point.y}</b>",
        },
        marker: {
          symbol: "circle",
          radius: 4,
        },
      },
    ],
  };
  return (
    <div>
      {error && <p>{errorMsg}</p>}
      {!error && <Chart options={chartOptions} highcharts={Highcharts} />}
    </div>
  );
}

export default ScoringGraph;
