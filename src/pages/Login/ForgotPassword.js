import React, { useState, useEffect } from "react";
import Logo from "../../assets/images/Login/login-logo.svg";
import { FiMail, FiChevronLeft } from "react-icons/fi";
import { useHistory } from "react-router-dom";
import LoginApi from "../../services/LoginApi";
import validator from "validator";
import Loader from "../../components/loader";

function ForgotPassword() {
  let history = useHistory();
  const [email, setEmail] = useState(undefined);
  const [error, setError] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const emailInputRef = React.useRef(null);

  /**
   * This function manages re-set password API
   * To call API need to pass email as parameter
   * When the API returns positive respones function redirect to otp page
   * If API returns error function show error message
   */
  const handleSubmit = () => {
    setLoader(true);
    if (!email) {
      setError("Please enter valid email address");
      setLoader(false);
    } else {
      if (validator.isEmail(email)) {
        let formData = {};
        formData["email"] = email;
        LoginApi.ForgotPassword(formData).then((json) => {
          if (!json?.data?.error) {
            localStorage.setItem("email", email);
            localStorage.setItem(
              "neswell_forgot_password_token",
              json?.data?.token
            );
            history.push("/otp");
            setLoader(false);
          } else {
            setError(json?.data?.error);
            setLoader(false);
          }
        });
      } else {
        setError("Please enter valid email address");
        setLoader(false);
      }
    }
  };
  /**
   * This function use for if token is not store in localStorage then, function redirect on forgot password page
   * And if tonke is availabe in local storage then funvtion redirect on user page
   */
  useEffect(() => {
    emailInputRef.current.focus();
    if (!localStorage.getItem("neswell_access_token")) {
      history.push("/forgot-password");
    } else if (localStorage.getItem("neswell_access_token") !== "null") {
      history.push("/users");
    }
  }, []);

  return (
    <div className="login-wrapper">
      {loader && <Loader />}
      <div className="login-inner-wrapper">
        <div className="login-right-fuild">
          <div>
            <div className="login-logo">
              <img src={Logo} alt="" />
            </div>
            <div className="logo-bottom-text">
              <h1>Reset Your Password</h1>
              <p className="forgot-password-text">
                We'll send you instructions to reset your <br />
                password and get you back on track.
              </p>
            </div>
            <div className="login-form">
              <div className="form-group">
                <label>
                  {" "}
                  <FiMail />{" "}
                </label>
                <input
                  ref={emailInputRef}
                  onChange={(e) => {
                    setEmail(e.target.value);
                    setError(undefined);
                  }}
                  onKeyDown={(event) => {
                    event.key === "Enter" && handleSubmit();
                  }}
                  value={email}
                  type="email"
                  placeholder="Email"
                />
              </div>
              <div className="login-btn">
                {error && <p className="error-massage">{error}</p>}
                <button onClick={() => handleSubmit()} className="primary-btn">
                  Reset
                </button>
              </div>
              <div className="forgot-text back-login-btn">
                <a onClick={() => history.push("/")}>
                  {" "}
                  <FiChevronLeft /> Back to Login
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
