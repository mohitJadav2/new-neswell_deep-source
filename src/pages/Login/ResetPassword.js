import React, { useState, useEffect } from "react";
import Logo from "../../assets/images/Login/login-logo.svg";
import { FiLock, FiEye } from "react-icons/fi";
import { useHistory } from "react-router-dom";
import LoginApi from "../../services/LoginApi";
import DashboardApi from "../../services/DashboardApi";
import validator from "validator";
import Loader from "../../components/loader";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

/**
 * This function manages new password and with conform password
 * The API manages new passwords and return succesuss or error message
 * When succesuss message get then function and redirect to login page with succesuss message
 * If error raises the function shows the error message
 */
function ResetPassword(props) {
  let history = useHistory();
  const [password, setPassword] = useState(undefined);
  const [reEnteredPassword, setReEnteredPassword] = useState(undefined);
  const [passwordError, setPasswordError] = useState(false);
  const [error, setError] = useState(undefined);
  const [strongError, setStrongError] = useState(false);
  const [loader, setLoader] = useState(false);
  const [uuid, setUuid] = useState(undefined);
  const [token, setToken] = useState(undefined);

  const [isRevealPwd, setIsRevealPwd] = useState(false);
  const [isRevealReCheck, setIsRevealRecheck] = useState(false);

  const handleSubmit = () => {
    setLoader(true);
    if (
      passwordError ||
      password != reEnteredPassword ||
      !reEnteredPassword ||
      !password
    ) {
      if (passwordError) {
        setLoader(false);
        setStrongError(true);
      }
      if (password != reEnteredPassword) {
        setLoader(false);
        setError("Password did not match");
      }
      if (!reEnteredPassword || !password) {
        setLoader(false);
        setError("Please fill up all fields");
      }
    } else {
      if (props?.match?.params?.uuid && props?.match?.params?.token) {
        setLoader(true);
        let formData = {};
        formData["new_password1"] = password;
        formData["confirm_password"] = reEnteredPassword;
        LoginApi.ResetPasswordFromLink(uuid, token, formData).then((json) => {
          if (!json?.data?.error) {
            notify();
            setLoader(false);
            setTimeout(() => history.push(`/`), 5000);
          } else {
            setLoader(false);
            setError(json?.data?.error);
          }
        });
      } else {
        setError(undefined);
        let formData = {};
        formData["email"] = localStorage.getItem("email");
        formData["new_password1"] = password;
        formData["new_password2"] = reEnteredPassword;
        LoginApi.ResetPassword(formData).then((json) => {
          if (!json?.data?.error) {
            DashboardApi.logOutApi(
              localStorage.getItem("neswell_forgot_password_token")
            ).then((json) => {
              if (!json?.data?.error) {
                notify();
                localStorage.setItem("neswell_forgot_password_token", "null");
                setLoader(false);
                setTimeout(() => history.push(`/`), 5000);
              } else {
                setLoader(false);
              }
            });
          } else {
            setLoader(false);
            setError(json.data.error);
          }
        });
      }
    }
  };

  /**
   * This function manage password validation
   */
  const handleEnterPassword = (item) => {
    setError(undefined);
    setPassword(item);
    if (
      validator.isStrongPassword(item, {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
      })
    ) {
      setStrongError(false);
      setPasswordError(false);
    } else {
      setPasswordError(true);
    }
  };

  /**
   * The useEffect() manage uuid & token to pass them in API call to store the new created password and redirect to login page
   */
  useEffect(() => {
    if (props?.match?.params?.uuid && props?.match?.params?.token) {
      setUuid(props?.match?.params?.uuid);
      setToken(props?.match?.params?.token);
    } else {
      if (!localStorage.getItem("neswell_forgot_password_token")) {
        history.push("/reset-password");
      } else if (
        localStorage.getItem("neswell_forgot_password_token") === "null"
      ) {
        history.push("/");
      }
      if (!localStorage.getItem("neswell_access_token")) {
        history.push("/");
      } else if (localStorage.getItem("neswell_access_token") !== "null") {
        history.push("/users");
      }
    }
  }, []);

  const notify = () => toast("Your password has been updated successfully!");

  return (
    <div className="login-wrapper">
      <div>
        <ToastContainer />
      </div>
      {loader && <Loader />}
      <div className="login-inner-wrapper">
        <div className="login-left-bg"></div>
        <div className="login-right-fuild">
          <div>
            <div className="login-logo">
              <img src={Logo} alt="" />
            </div>
            <div className="logo-bottom-text">
              <h1>Reset Password</h1>
              <p className="forgot-password-text">
                Please enter your new password below
              </p>
            </div>
            <div className="login-form">
              <div className="form-group">
                <label>
                  {" "}
                  <FiLock />{" "}
                </label>
                <input
                  onChange={(e) => handleEnterPassword(e.target.value)}
                  type={isRevealPwd ? "text" : "password"}
                  value={password}
                  placeholder="Enter New Password"
                  onKeyDown={(event) => {
                    event.key === "Enter" && handleSubmit();
                  }}
                />
                <label className="eyeBtn">
                  {" "}
                  <FiEye
                    onClick={() => setIsRevealPwd((prevState) => !prevState)}
                  />{" "}
                </label>
                {passwordError && strongError && (
                  <p style={{ color: "red" }}>
                    Enter passwords minimum 8 characters including uppercase
                    letters, lowercase letters, at least one number and special
                    character
                  </p>
                )}
              </div>
              <div className="form-group">
                <label>
                  {" "}
                  <FiLock />{" "}
                </label>
                <input
                  onChange={(e) => {
                    setReEnteredPassword(e.target.value);
                    setError(undefined);
                  }}
                  onKeyDown={(event) => {
                    event.key === "Enter" && handleSubmit();
                  }}
                  type={isRevealReCheck ? "text" : "password"}
                  value={reEnteredPassword}
                  placeholder="Re-Enter Password"
                />
                <label className="eyeBtn">
                  {" "}
                  <FiEye
                    onClick={() =>
                      setIsRevealRecheck((prevState) => !prevState)
                    }
                  />{" "}
                </label>
              </div>
              <div className="login-btn">
                {error && <p className="error-massage">{error}</p>}
                <button onClick={() => handleSubmit()} className="primary-btn">
                  Reset Password
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ResetPassword;
