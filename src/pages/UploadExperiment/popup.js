import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import "../../components/modal.style.scss";
import Select from "react-select";
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';

/** 
 * This style is use for cell data popup
 * */
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 30,
    borderRadius: 10,
    border: "none",
    width: 600,
    overflow: "inherit",
    boxShadow: "0 6px 16px rgba(#000, 0.16)",
  },
};
/** 
 * this style is use for the selection fild 
 */
const customSelectStyles1 = {
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? "#fff" : "#404040",
    padding: 10,
    borderRadius: 10,
    width: "98%",
    margin: "10px auto",
  }),
  control: (styles) => ({
    ...styles,
    border: "solid 1px #E5E5E5",
    backgroundColor: "white",
    maxHeight: 46,
    minHeight: 46,
    fontSize: 14,
    borderRadius: 10,
    color: "#929292",
    fontWeight: "600",
  }),
};

const Popup = (props) => {
  const groupData = props?.fatherData?.groupArray
    ? props?.fatherData?.groupArray
    : "";
  const extractdata = props?.fatherData?.extract
  const clusterData = props?.fatherData?.cluster
    ? props?.fatherData?.cluster
    : "";
  const [answerWithKey, setAnswerWithKey] = useState(undefined);
  const [fieldList, setFieldList] = useState([]);
  const [fieldData, setFieldData] = useState([]);
  const [fieldName, setFieldName] = useState(undefined);
  const [duplicateFileData, setDuplicateFileData] = useState([]);
  const [saveCancleData, setsaveCancleData] = useState(undefined);
  const [selectSupportedGroup, setSelectSupportedGroup] = useState([]);

  const [groupOptions, setGroupOptions] = useState([]);
  const [clusterOptions, setClusterOptions] = useState([]);
  const [extractOptions, setExtractOptions] = useState([]);

  const [extractValue, setExtractValue] = useState(undefined);
  const [clusterValue, setClusterValue] = useState(undefined);
  const [groupValue, setGroupValue] = useState(undefined);

  const [examinationTubeError, setExaminationTubeError] = useState([]);
  const [submitTimeErrorChecker, setSubmitTimeErrorChecker] = useState(false);


  let { slug } = useParams();
 
  const fieldMapping = {
    Cluster: "Experimental Condition",
    Group: "Experimental Group",
    Extract: "Extract",
    "Additional text": "Additional Text"
  }

  /**
   * This function manage group Data, cluster Data, extract data from props 
  */
  useEffect(() => {
    (async () => {
      setAnswerWithKey(props?.data?.answer);
      groupData &&
        (await groupData?.map((item) => {
          setGroupOptions((prevState) => [
            ...prevState,
            { value: item, label: item?.label },
          ]);
          if (props?.data?.answer?.group?.length > 0) {
            if (
              item?.label?.toString() ==
              props?.data?.answer?.group[0]?.label?.toString()
            ) {
              setGroupValue({ value: item, label: item?.label });
            }
          }
        }));
      clusterData &&
        (await clusterData?.map((item) => {
          setClusterOptions((prevState) => [
            ...prevState,
            { value: item, label: item?.label },
          ]);
          if (props?.data?.answer?.cluster?.length > 0) {
            if (
              item?.label?.toString() ==
              props?.data?.answer?.cluster[0]?.label?.toString()
            ) {
              setClusterValue({ value: item, label: item?.label });
            }
          }
        }));

      if(props?.params){
        if(props?.data?.answer?.extract){
            setExtractValue({ value: props?.data?.answer?.extract, label: props?.data?.answer?.extract?.label })
        }
        if(props?.data?.answer?.cluster?.length > 0){
          setClusterValue({ value: props?.data?.answer?.cluster[0], label: props?.data?.answer?.cluster[0]?.label })
        }
      
        if(props?.data?.answer?.group?.length > 0){
          let object
          if(slug){
            if(props?.data?.answer?.group[0]){
              if(typeof props?.data?.answer?.group[0] == "string"){
                object = props?.data?.answer?.group.map((item)=> ({value: 0, label:item ,color:"#DFCECE"}))
              } else {
                object = props?.data?.answer?.group.map((item)=> ({value: 0, label:item.label ,color:"#DFCECE"}))
              }
            }else{
              object = props?.data?.answer?.group.map((item)=> ({value: 0, label:item ,color:"#DFCECE"}))
            }
          }
          setGroupValue(object)
        }
      }
      extractdata &&
        (await extractdata?.map((item) => {
          setExtractOptions((prevState) => [
            ...prevState,
            { value: item, label: item?.label },
          ]);
          if (props?.data?.answer?.extract?.length > 0) {
            if (
              item?.label.toString() ==
              props?.data?.answer?.extract[0]?.label.toString()
            ) {
              setExtractValue({ value: item, label: item?.label });
            }
          }
        }));
      if (props?.data?.answer?.supported_groups?.length > 0) {
        let updatedGroup
        if(slug){
          props?.data?.answer?.supported_groups[0]?.value ? (
            updatedGroup = props?.data?.answer?.supported_groups?.map((item) =>  ({label:item?.label, value: {value: item?.value, label: item?.label, color: item?.color}}))
          ) : (
            updatedGroup = props?.data?.answer?.supported_groups?.map((item) => ({label:item, value: {value: item, label: item, color: item}}))
          )
        } else {
          updatedGroup = props?.data?.answer?.supported_groups?.map((item) =>  ({label:item?.label, value: {value: item?.value, label: item?.label, color: item?.color}}))
        }
        
        setSelectSupportedGroup(updatedGroup);
      }
      
      await props?.popUpData?.cell_types?.map((item) => {
        setFieldList((prevItems) => [
          ...prevItems,
          {
            value: item,
            label: item?.cell_key,
          },
        ]);
      });
      
      setFieldName({
        value: {
          cell_key: props.data?.cell_key,
          cell_type: props.data?.cell_key,
          fields: props.data?.fields,
        },
        label: props.data?.cell_key,
      });
      setFieldData({
        ...fieldData,
        cell_key: props.data?.cell_key,
        cell_type: props.data?.cell_key,
        fields: props.data?.fields,
      });

      setsaveCancleData(props?.data?.answer);
      setDuplicateFileData({
        ...duplicateFileData,
        cell_key: props.data?.cell_key,
        cell_type: props.data?.cell_key,
        fields: props.data?.fields,
      });
    })();
  }, []);

  /**
   *This function manage cell matrix pop's extract, cluster and group's drop down data
  */
  const onCallTypeRadioButtonClicked = async (changeEvent, item) => {
    if (item?.field_key == "extract") {
      setExtractValue(changeEvent);
      const updatedArray11 = examinationTubeError?.map((item1, idx) =>
        Object.keys(examinationTubeError[idx]).toString() == item?.name
          ? { [item?.name]: true }
          : item1
      );
      setExaminationTubeError(updatedArray11);
    }
    if (item?.field_key == "group") {
      setGroupValue(changeEvent?.value);
      await setAnswerWithKey({
        ...answerWithKey,
        [item?.field_key]: [changeEvent?.value],
        color: changeEvent?.value?.color,
      });
      const updatedArray11 = examinationTubeError?.map((item1, idx) =>
        Object.keys(examinationTubeError[idx]).toString() == item?.name
          ? { [item?.name]: true }
          : item1
      );
      setExaminationTubeError(updatedArray11);
    } else {
      setAnswerWithKey({
        ...answerWithKey,
        [item?.field_key]: [changeEvent?.value],
      });
      const updatedArray11 = examinationTubeError?.map((item1, idx) =>
        Object.keys(examinationTubeError[idx]).toString() == item?.name
          ? { [item?.name]: true }
          : item1
      );
      setExaminationTubeError(updatedArray11);
    }
  };

  /**
   * This function manage Field name in cell popup box
   */
  const handleSetFieldName = async (value) => {
    setExaminationTubeError([]);
    await value?.value?.fields?.map((item) => {
      if (item?.field_key != "additional_text") {
        setExaminationTubeError((prevState) => [
          ...prevState,
          { [item?.name]: false },
        ]);
      }
    });
    setsaveCancleData(answerWithKey);
    setDuplicateFileData({
      ...duplicateFileData,
      cell_key: props.data?.cell_key,
      cell_type: props.data?.cell_key,
      fields: props.data?.fields,
    });
    setAnswerWithKey(undefined);
    
    setFieldData({
      ...fieldData,
      cell_key: value?.value?.cell_key,
      cell_type: value?.value?.cell_key,
      fields: value?.value?.fields,
    });
    setFieldName(value);
  };

  /**
   * This function manage submit the popUp all data to API
   */
  const handleSubmit = async () => {
    setSubmitTimeErrorChecker(true);
    let data = await examinationTubeError.map(
      (v, idx) => Object.values(examinationTubeError[idx])[0]
    );
    let checker = data.every((v) => v === true);

    if (checker) {
      await props?.handleSubmiClicked({
        ...props.data,
        cell_key: fieldData?.cell_key,
        cell_type: fieldData?.cell_key,
        fields: fieldData?.fields,
        answer: answerWithKey, 
      });
    }
  };

  /**
   * This function manage close cell popup box
   */
  const handleCancleClicked = async () => {
    await props?.closeModal({
      ...props.data,
      cell_key: duplicateFileData?.cell_key,
      cell_type: duplicateFileData?.cell_key,
      fields: duplicateFileData?.fields,
      answer: saveCancleData,
    });
  };

  /**This function manage group data drop down in cell popup */
  const handleCheckboxClicked = (value, item) => {
    if (value.length > 0) {
      const updatedArray11 = examinationTubeError?.map((item1, idx) =>
        Object.keys(examinationTubeError[idx]).toString() == item?.name
          ? { [item?.name]: true }
          : item1
      );
      setExaminationTubeError(updatedArray11);
    } else {
      const updatedArray11 = examinationTubeError?.map((item1, idx) =>
        Object.keys(examinationTubeError[idx]).toString() == item?.name
          ? { [item?.name]: false }
          : item1
      );
      setExaminationTubeError(updatedArray11);
    }
    let data = value?.map((item) => item?.value);

    setAnswerWithKey({ ...answerWithKey, [item?.field_key]: data });
    setSelectSupportedGroup(value);
  };

  /**
   * This function manage additional text area in cell popup box
   */
  const handletextAreaChanges = (value, item) => {
    setAnswerWithKey({ ...answerWithKey, [item?.field_key]: value });
  };

  return (
    <div>
      <Modal
        isOpen={props.open}
        style={customStyles}
        overlayClassName="react-modal-body cell-popup-details"
        contentLabel="Example Modal"
        overlayClassName={
          fieldName?.label == "Treated infected"  ||  fieldName?.label == "Treated naive" ||  fieldName?.label == "Examined tube" 
            ? "react-modal-body cell-popup-details examined-tube"
            : "react-modal-body cell-popup-details"
        }
      >
        <div className="upload-experiment-popup-wrapper">
          <div className="upload-experiment ">
            <div className="form-group">
              <p style={{fontWeight: "700"}}> Cell Type</p>
              <Select
                          styles={customSelectStyles1}
                          placeholder="Select Cell Type"
                          options={fieldList}
                          isSearchable={false}
                          value={fieldName ? fieldName : "Select Cell Type"}
                          onChange={(value) => {
                            handleSetFieldName(value);
                          }}
                          isDisabled={props?.viewExperiment}
                        />
            </div>
            {fieldData?.fields?.map((item) => (
              <div>
                <div className="radio-group-title">
                  <p>{item ? fieldMapping[item?.name] : ""}</p>
                </div>
                <div className="radio-checkbox-group-wrapper">
                  {item?.type == "checkbox" ? (
                    <div className="checkbox-group">
                      <div className="form-group">
                        <Select
                          styles={customSelectStyles1}
                          placeholder="Select groups"
                          options={groupOptions}
                          closeMenuOnSelect={false}
                          isSearchable={false}
                          isMulti
                          value={selectSupportedGroup}
                          onChange={(value) => {
                            handleCheckboxClicked(value, item);
                          }}
                          isDisabled={props?.viewExperiment}
                        />
                        {submitTimeErrorChecker &&
                          examinationTubeError?.map((error, idx) => {
                            if (
                              item.name ==
                              Object.keys(examinationTubeError[idx])[0]
                            ) {
                              if (
                                !Object.values(examinationTubeError[idx])[0]
                              ) {
                                return (
                                  <p style={{ color: "red" }}>
                                    {" "}
                                    Please Select the Group First{" "}
                                  </p>
                                );
                              }
                            }
                          })}
                      </div>

                    </div>
                  ) : item?.type == "radio_button" ? (
                    <div className="radio-group">
                      {item?.field_key == "group" ? (
                        <div>
                          <Select
                            styles={customSelectStyles1}
                            placeholder="Select Experimental Group"
                            options={groupOptions}
                            isSearchable={false}
                            value={groupValue}
                            onChange={(value) => {
                              onCallTypeRadioButtonClicked(value, item);
                            }}
                            isDisabled={props?.viewExperiment}
                          />
                          {submitTimeErrorChecker &&
                            examinationTubeError?.map((error, idx) => {
                              if (
                                item.name ==
                                Object.keys(examinationTubeError[idx])[0]
                              ) {
                                if (
                                  !Object.values(examinationTubeError[idx])[0]
                                ) {
                                  return (
                                    <p style={{ color: "red" }}>
                                      {" "}
                                      Please Select the Group First{" "}
                                    </p>
                                  );
                                }
                              }
                            })}
                        </div>
                      ) : (
                        <div>
                          {item?.field_key == "cluster" ? (
                            <div>
                              <Select
                                styles={customSelectStyles1}
                                placeholder="Select Experimental Condition"
                                options={clusterOptions}
                                isSearchable={false}
                                value={clusterValue}
                                onChange={(value) => {
                                  onCallTypeRadioButtonClicked(value, item);
                                }}
                                isDisabled={props?.viewExperiment}
                              />
                              {submitTimeErrorChecker &&
                                examinationTubeError?.map((error, idx) => {
                                  if (
                                    item.name ==
                                    Object.keys(examinationTubeError[idx])[0]
                                  ) {
                                    if (
                                      !Object.values(
                                        examinationTubeError[idx]
                                      )[0]
                                    ) {
                                      return (
                                        <p style={{ color: "red" }}>
                                          {" "}
                                          Please Select the cluster First{" "}
                                        </p>
                                      );
                                    }
                                  }
                                })}
                            </div>
                          ) : (
                            <div>
                              <Select
                                styles={customSelectStyles1}
                                placeholder="Select Extracts"
                                options={extractOptions}
                                isSearchable={false}
                                value={extractValue}
                                onChange={(value) => {
                                  onCallTypeRadioButtonClicked(value, item);
                                }}
                                isDisabled={props?.viewExperiment}
                              />
                              {submitTimeErrorChecker &&
                                examinationTubeError?.map((error, idx) => {
                                  if (
                                    item.name ==
                                    Object.keys(examinationTubeError[idx])[0]
                                  ) {
                                    if (
                                      !Object.values(
                                        examinationTubeError[idx]
                                      )[0]
                                    ) {
                                      return (
                                        <p style={{ color: "red" }}>
                                          {" "}
                                          Please Select the extract First{" "}
                                        </p>
                                      );
                                    }
                                  }
                                })}
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  ) : null}
                </div>
                {item?.type == "text" && (
                  <div className="popup-textarea">
                    <textarea
                      onChange={(e) => {
                        handletextAreaChanges(e.target.value, item);
                      }}
                      disabled={props?.viewExperiment}
                      className="additional-text-details"
                      value={answerWithKey && answerWithKey[item?.field_key]}
                    />
                    {submitTimeErrorChecker &&
                      examinationTubeError?.map((error, idx) => {
                        if (
                          item.name == Object.keys(examinationTubeError[idx])[0]
                        ) {
                          if (!Object.values(examinationTubeError[idx])[0]) {
                            return (
                              <p style={{ color: "red" }}>
                                {" "}
                                Please fill the additional text{" "}
                              </p>
                            );
                          }
                        }
                      })}
                  </div>
                )}
              </div>
            ))
          }
          </div>
          <div className="popup-bottm-btn">
            <button
              className="add-filter primary-btn"
              onClick={() => handleCancleClicked()}
            >
              Cancel
            </button>
            {!props?.viewExperiment && (
              <button
                className="apply-filter primary-btn"
                onClick={() => handleSubmit()}
              >
                Ok
              </button>
            )}
          </div>
        </div>
      </Modal>
    </div>
  );
};

/**This function manage fatherData from redux store */
const mapStateToProps = (state) => ({
  fatherData: state.homeReducer.fatherData
});

export default connect(mapStateToProps, null)(Popup)
