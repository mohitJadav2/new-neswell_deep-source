import React, { useState, useEffect } from "react";
import Select from "react-select";
import "./experiment.style.scss";
import { BiPlus, BiMinus } from "react-icons/bi";
import Layout from "../../Layout";
import { AiOutlineCopy } from "react-icons/ai";
import { IoMdCopy } from "react-icons/io";
import Popup from "./popup";
import PDFImage from "../../assets/pdf-large.svg";
import CSVImage from "../../assets/csv.png";
import DOCImage from "../../assets/doc.png";
import XMLImage from "../../assets/xml-file.jpg";
import TEXTImage from "../../assets/txt-file.png";
import JPEGImage from "../../assets/jpeg.png";
import JPGImage from "../../assets/jpg.png";
import XLXSImage from "../../assets/xls.png";
import Switch from "react-switch";
import UploadExperimentApi from "../../services/UploadExperimentApi";
import Selecto from "react-selecto";
import { useHistory, useParams } from "react-router-dom";
import { BiEdit } from "react-icons/bi";
import { Link } from "react-router-dom";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import Base64Downloader from 'common-base64-downloader-react';
import { clearExperimentData, saveExperimentData } from "../../Actions/HomeAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import Trash from '../../assets/icons/trash.svg';
const selectTableOptions = [
  { value: { row: 8, col: 12 }, label: "8 X 12 (96 wells)" },
  { value: { row: 6, col: 4 }, label: "6 X 4 (24 wells)  " },
  { value: { row: 3, col: 2 }, label: "3 X 2 (6 wells)" },
  { value: { row: 8, col: 6 }, label: "8 X 6 (48 wells)" },
];

/** 
 * this style is use for the selection fild 
*/
const customSelectStyles = {
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? "#fff" : "#404040",
    padding: 10,
    borderRadius: 10,
    width: "98%",
    margin: "10px auto",
  }),
  control: (styles) => ({
    ...styles,
    border: "solid 1px #E5E5E5",
    backgroundColor: "white",
    maxHeight: 46,
    minHeight: 46,
    fontSize: 14,
    borderRadius: 10,
    color: "#929292",
    fontWeight: "600",
  }),
};

function UploadExperiment(props) {
  let { slug } = useParams();
  let history = useHistory();
  localStorage.getItem("fatherBioData");
  const filetypes = ["xlsx"];
  const [experimentName, setExperimentName] = useState(undefined)
  const [CountColumn, setCountColumn] = useState(12);
  const [CountRow, setCountRow] = useState(8);
  const [modalOpen, setModalOpen] = useState(false);
  const [checked, setChecked] = useState(false);
  const [selectedTableValue, setSelectedTableValue] = useState({ label: "8 X 12 (96 wells)", value: { row: 8, col: 12 } });
  const [radioButtonValue, setRadioButtonValue] = useState(
    !props?.match?.params?.slug ? "Predefined" : "Create"
  );
  const [saveChildBioTest, setSaveChildBioTest] = useState(
    JSON.parse(window.localStorage.getItem("faterBioData"))
  );
  const [saveFaterBiotestId, setSaveFaterBioTestId] = useState(
  );
  const [saveGeneralBioTest, setSaveGeneralBioTest] = useState();
  const [fatherBioTestDocuments, setFatherBioTestDocuments] =
    useState(undefined);
  const [copyedShell, setCopyedShell] = useState(undefined);
  const [rowArray, setRowArray] = useState([]);
  const [array, setArray] = useState([]);
  const [pastedArray, setPastedArray] = useState(undefined);
  const [copyedClicked, setCopyedClicked] = useState(false);
  const [saveCopyedText, setSaveCopydeText] = useState(undefined);
  const [shellData, setShellData] = useState(undefined);
  const [popUpData, setPopUpData] = useState(undefined);
  const [colError, setColError] = useState(false);
  const [bioTestError, setBioTestError] = useState(false);
  const [tableError, setTableError] = useState(false);
  const [fileError, setFileError] = useState(false);
  const [popUpModalOpen, setPopUpModalOpen] = useState(false);
  const handleChange = (nextChecked) => {
    setChecked(nextChecked);
  };
  const [submitClicked, setSubmitClicked] = useState(false);
  const [arraywitharray, setArrayWithArray] = useState(undefined);
  const [viewClusterData, setViewClusterData] = useState([]);
  const [viewExtractData, setViewExtractData] = useState([]);
  const [bioTestList, setBioTestList] = useState([]);
  const [bioTestListData, setBioTestListData] = useState([]);
  const [cluster, setCluster] = useState([]);
  const [extract, setExtract] = useState([]);
  const [fileData, setFileData] = useState("");
  const [dataMappingError, setDataMappingError] = useState(undefined);
  const [errorModelOpen, setErrorModelOpen] = useState(false)
  const [fileName, setFileName] = useState(undefined)
  const [loader, setLoader] = useState(false);
  let testArray = [];
  let finalArray = [];
  let x = 0;
  let y = CountColumn;
  const [saveFatherBioTestPath, setSaveFaterBioTestPath] = useState(undefined)
  let viewExperiment = window.location.href.includes("view-experiment");
  const [fatherBioData, setFatherBioData] = useState(undefined);

  /**
   * This function manage father Bio test data from  API
   */
  useEffect(() => {
    setLoader(true)
    UploadExperimentApi.getFaterBioTest(
      localStorage.getItem("neswell_access_token")
    ).then(async (json) => {
      if (!json?.data?.error) {
        await json?.data?.map((item) => {
          setBioTestList((prevItems) => [
            ...prevItems,
            {
              value: item?.id,
              label: item?.name,
            },
          ]);
        });
        setTimeout(() => {
          setLoader(false)
        }, 15000)
      } else {
        setLoader(false)
      }
    });
  }, []);

  /**
   * This function manage experiment records for experiment view page and edit page 
   */
  useEffect(() => {
    setLoader(true)
    if (props?.match?.params?.slug) {
      UploadExperimentApi.getExperimentOptionData(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug
      ).then(async (json) => {
        if (!json?.data?.error) {
          await json?.data?.extracts.map((item) => {
            setExtract((prevItems) => [
              ...prevItems,
              {
                value: item?.id,
                label: item?.name,
              },
            ]);
          });
          await json?.data?.clusters.map((item) => {
            setCluster((prevItems) => [
              ...prevItems,
              {
                value: item?.id,
                label: item?.name,
              },
            ]);
          });
          setExperimentName(json?.data?.name)
          setFatherBioTestDocuments(json?.data?.files?.result_file)
        } else {
          setLoader(false)
          setDataMappingError(json.data.error)
          setErrorModelOpen(true)
        }
      })

      UploadExperimentApi.getUploadExperimentById(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug
      ).then(async (json) => {
        if (!json?.data?.error) {
          setFatherBioData(json?.data?.general_bio_test_id)
          let reduxData = {
            faterBioData: json?.data?.general_bio_test_id,
            extract: json?.data?.extracts,
            experiment: [],
            cluster: json?.data?.clusters,
            groupArray: json?.data?.groups_data
          }
          props.saveExperimentData(reduxData)
          setViewClusterData(json?.data?.clusters);
          setViewExtractData(json?.data?.extracts);
          setBioTestListData(json?.data?.general_bio_test_id);
          setSaveFaterBioTestId(json?.data?.popup_data?.general_bio_test_id)
          setSelectedTableValue({
            value: {
              row: json?.data?.table_size_row,
              col: json?.data?.table_size_column,
            },
            label: `${json?.data?.table_size_row} X ${json?.data?.table_size_column
              } (${json?.data?.table_size_row * json?.data?.table_size_column
              } wells)`,
          });
          setSaveFaterBioTestPath(json?.data?.path_to_storage)
          setFatherBioTestDocuments(json?.data?.files?.result_file);
          setPopUpData(json?.data?.popup_data);
          setSaveGeneralBioTest(json?.data?.general_bio_test_id);
          setCountColumn(parseInt(json?.data?.table_size_column));
          setCountRow(parseInt(json?.data?.table_size_row));

          y = parseInt(json?.data?.table_size_column);
          setRowArray(json?.data?.uploaded_json);

          let fetchedArray = json?.data?.uploaded_json;
          _.uniqBy(fetchedArray, "answer.group")

          let groupData = json?.data?.uploaded_json?.map((item) => item?.answer?.group?.length > 0 && item?.answer?.group[0])

          for (let i = 0; i < parseInt(json?.data?.table_size_row); i++) {
            for (let j = x; j < y; j++) {
              let shellObject = fetchedArray[j];
              testArray.push(shellObject);
            }

            x = x + parseInt(json?.data?.table_size_column);
            y = y + parseInt(json?.data?.table_size_column);
            finalArray.push(testArray);
            testArray = [];
          }
          setArrayWithArray(finalArray);
          setLoader(false)
        } else {
          setLoader(false)
        }
      });
    }
  }, []);

  /**
   * This function use for create table in create Experiment  
   */
  const handleCreatTableClicked = () => {
    setLoader(true)
    var gbit = fatherBioData ? fatherBioData : props?.fatherData?.faterBioData[0];
    let dd = gbit?.length > 0 ? gbit[0]?.value : gbit?.value
    let Id = dd ? dd : saveChildBioTest?.value
    setColError(false);
    setRowArray([]);
    setArrayWithArray(undefined);
    UploadExperimentApi.generateMatrixData(
      CountRow,
      CountColumn,
      Id,
      localStorage.getItem("neswell_access_token")
    ).then(async (json) => {
      if (!json?.data?.error) {
        setPopUpData(json?.data?.popup_data);
        await json?.data?.popup_matrix_list?.cells_content?.map((item) =>
          setRowArray((prevItems) => [...prevItems, { ...item, answer: {} }])
        );
        for (let i = 0; i < CountRow; i++) {
          for (let j = x; j < y; j++) {
            let shellObject = json?.data?.popup_matrix_list?.cells_content[j];
            testArray.push({ ...shellObject, answer: {} });
          }
          x = x + CountColumn;
          y = y + CountColumn;
          finalArray.push(testArray);
          testArray = [];
        }
        setArrayWithArray(finalArray);
        setLoader(false)
      } else {
        setColError(true)
      }
    });
  };

  /**
   * This function use for select radio button in phase-2 in create  experiment to creat table
   */
  const onSiteChanged = (changeEvent) => {
    setRadioButtonValue(changeEvent?.target?.value);
  };

  /**
   * This function is use for select table column and row to create table in creat table drop down 
   */
  const handleSelectTableValue = async (value) => {
    y = value.value.col;

    await setSelectedTableValue(value);
    setCountRow(value.value.row);
    setCountColumn(value.value.col);
  };

  /**
   * This function manage copy cell in table and past in empty cell 
  */
  const handlePastClicked = async () => {
    let updatedarray = await rowArray.map((r) =>
      array.includes(r.position [0].toString() + (r.position[1]+1).toString())
        ? {
          cell_key: saveCopyedText.cell_key,
          cell_type: saveCopyedText.cell_type,
          fields: saveCopyedText.fields,
          position: r.position,
          answer: saveCopyedText.answer,
        }
        : r
    );
    for (let i = 0; i < CountRow; i++) {
      for (let j = x; j < y; j++) {
        testArray.push(updatedarray[j]);
      }
      x = x + CountColumn;
      y = y + CountColumn;
      finalArray.push(testArray);
      testArray = [];
    }
    setArray([]);
    setRowArray(updatedarray);
    setArrayWithArray(finalArray);
    setPastedArray(updatedarray);
  };

  const handleSavedCopyedText = () => {
    setSaveCopydeText(copyedShell);
  };

  /**
   * This function manage when user fill the cell data and clicked on submit button to store the cell data
  */
  const submitPopUp = async (value) => {
    let updatedarray = await rowArray.map((r) =>
      r.position == value.position ? value : r
    );
    for (let i = 0; i < CountRow; i++) {
      for (let j = x; j < y; j++) {
        testArray.push(updatedarray[j]);
      }
      x = x + CountColumn;
      y = y + CountColumn;
      finalArray.push(testArray);
      testArray = [];
    }
    setRowArray(updatedarray);
    setArrayWithArray(finalArray);
    setPastedArray(updatedarray);
    setModalOpen(false);
  };

  /**
   * This function manage close to cell popup  
  */
  const closePopUp = async (value) => {
    let updatedarray = await rowArray.map((r) =>
      r.position == value.position ? value : r
    );
    for (let i = 0; i < CountRow; i++) {
      for (let j = x; j < y; j++) {
        testArray.push(updatedarray[j]);
      }
      x = x + CountColumn;
      y = y + CountColumn;
      finalArray.push(testArray);
      testArray = [];
    }
    setRowArray(updatedarray);
    setArrayWithArray(finalArray);
    setPastedArray(updatedarray);
    setModalOpen(false);
  };
  let clusterArray = cluster.map((item) => item?.value);
  let extractArray = extract.map((item) => item?.value);

  /**
     * This function collect experiment details in form data in json formant
     * And send to API and get positive and error response from API
    */
  const handleSubmit = async () => {
    setLoader(true)
    setSubmitClicked(true);
    var gbit = JSON.parse(localStorage.getItem("faterBioData"));
    let formData4 = {};
    formData4["row_array"] = rowArray;
    formData4["general_bio_test_id"] = gbit?.length > 0 ? gbit[0]?.value : gbit?.value;
    formData4["user_id"] = 2;
    formData4["id"] = slug;
    formData4["files"] = fileData;
    formData4["clusters"] = JSON.stringify(clusterArray);
    formData4["extracts"] = JSON.stringify(extractArray);
    formData4["path_to_storage"] = fatherBioTestDocuments;
    formData4["name"] = experimentName
    UploadExperimentApi.submitUploadExperiment(
      localStorage.getItem("neswell_access_token"),
      formData4
    ).then(async (json) => {
      if (!json?.data?.error) {
        history.push(`/experiment-list`);
        await props?.clearExperimentData();
        setLoader(false)
      } else {
        if (!experimentName) {
          setBioTestError(true)
        } else {
          setBioTestError(false)
        }
        if (!arraywitharray) {
          setTableError(true)
        } else {
          setTableError(false)
        }
        if (!fileName) {
          setFileError(true)
        } else {
          setFileError(false)
        }
        setDataMappingError(json.data.error)
        setErrorModelOpen(true)
        setLoader(false)
      }
    })
      .catch(e => {
        setLoader(false)
        setDataMappingError(e)
        setErrorModelOpen(true)
      });

  };

  /**
     * This function manage experiment edit feature and API set particular experiment data in edit from
     * And user change the details and that data send in json formant to API
    */
  const handleSubmit2 = async () => {
    setSubmitClicked(true);
    var formData4 = {};
    formData4["row_array"] = rowArray;
    formData4["general_bio_test_id"] = bioTestListData?.value;
    formData4["user_id"] = 2;
    formData4["id"] = parseInt(slug);
    formData4["files"] = fileData ? fileData : fatherBioTestDocuments;;
    formData4["name"] = experimentName
    UploadExperimentApi.editExperiment(
      localStorage.getItem("neswell_access_token"),
      slug,
      formData4
    ).then(async (json) => {
      if (!json?.data?.error) {
        history.push(`/experiment-list`);
      } else {
        setDataMappingError(json.data.error)
        setErrorModelOpen(true)
      }
    })
  };

  /**
   * This function  use for when user upload to file in create experiment 
    */
  async function handleFileChange2(e) {
    const file = e?.target?.files[0];
    await setSaveFaterBioTestPath(file?.name)
    await setFileName(file?.name)
    const reader = new FileReader();
    await reader?.readAsDataURL(file);
    reader.onloadend = function () {
      setFileData(reader.result);
    };
  }
  /**
   * When user clicked delete button ,this function manage experiment delete feature 
   * The function called delete Api with experiment's id 
  */
  const handleDeleteClicked = () => {
    setModalOpen(false)
    setLoader(true)
    UploadExperimentApi.deleteExperiment(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug
    )
      .then((json) => {
        if (!json.data.error) {
          history.push('/experiment-list')
          setLoader(false)
          setModalOpen(false)
        } else {
          setLoader(false)
          setModalOpen(false)
        }
      })
  };
  /**
   * This constant variable upload file path for convert in Base64 
   */
  const base64Xlsx = fatherBioTestDocuments;

  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={errorModelOpen}
        closeModal={() => (setErrorModelOpen(false), dataMappingError == 'Record Not Found' ? history.push('/experiment-list') : dataMappingError == 'Data mapping error.') ? history.push('/experiment-list') : null}
        subTitle={dataMappingError}
        error={true}
        fromError={true}
      />
      {modalOpen && (
        <Popup
          data={shellData}
          open={modalOpen}
          closeModal={(value) => closePopUp(value)}
          handleSubmiClicked={(value) => submitPopUp(value)}
          viewExperiment={viewExperiment}
          popUpData={popUpData}
          params={props?.match?.params?.slug}
        />
      )}
      <Modal
        open={popUpModalOpen}
        closeModal={() => setPopUpModalOpen(false)}
        DeleteUsersRecord={() => handleDeleteClicked()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={true}
      />
      <div className="upload-experiment add-users-details-wrapper">
        {viewExperiment && (
          <div className="users-title create-new-extract-header">
            <h2>Experiment View</h2>
            <div className="create-extract-header">
              <Link className="icon-gred-box" to={`/upload-experiment/${props?.match?.params?.slug}`}>
                {" "}
                <BiEdit />{" "}
              </Link>
              <Link className="icon-gred-box" onClick={() => setPopUpModalOpen(true)}>
                {" "}
                <img src={Trash} alt="trash" className="trash-icon" />
              </Link>
            </div>
          </div>
        )}
        <div className="selector-wrapper">
          <div className="title-step">
            <span>1</span>
            <label>Experiment Name</label>
          </div>
          {viewExperiment ? (
            <div className="bio-test-view-one-value experiment-name-view">
              <span>{experimentName}</span>
            </div>
          ) : (
            <div className='selector-wrapper experiment-name-input'>
              <input
                className='add-group-input'
                placeholder="Select experiment name"
                type='text'
                value={experimentName}
                onChange={e => {
                  setExperimentName(e.target.value)
                }}
              />
              {bioTestError && (
                <p style={{ color: "red" }}>Please Select a Experiment name </p>
              )}
            </div>
          )
          }
          <div className="title-step">
            <span>2</span>
            <label>Bio-Test</label>
          </div>
          {viewExperiment ? (
            <div className="bio-test-view-one-value">
              <span>{bioTestListData?.label}</span>
            </div>
          ) : slug >= parseInt(1) ? (
            <div>
              <Select
                styles={customSelectStyles}
                placeholder="Select Bio-Test"
                options={bioTestList}
                isSearchable={false}
                value={bioTestListData}
                onChange={(value) => {
                  setBioTestListData(value);
                }}
              />
              {bioTestError && (
                <p style={{ color: "red" }}>Please Select a child biotest</p>
              )}
            </div>
          ) : (
            <div className="bio-test-view-one-value">
              <span>{saveChildBioTest.length > 0 ? saveChildBioTest[0]?.label : saveChildBioTest?.label}</span>
            </div>
          )}
        </div>
        <div className="column-table-wrapper">
          <div className="title-step">
            <span>3</span>
            <label>table</label>
          </div>
          <div className="predefined-custom-table-wrapper">
            <div className="predefined-table">
              {!viewExperiment && (
                <div className="radio">
                  <input
                    type="radio"
                    checked={radioButtonValue == "Predefined" ? true : false}
                    value="Predefined"
                    onChange={onSiteChanged}
                  />
                  <label htmlFor="PredefinedTableSizes" className="radio-label">
                    Predefined Table sizes
                  </label>
                </div>
              )}
              {!viewExperiment && (
                <div className="radio">
                  <input
                    type="radio"
                    checked={radioButtonValue == "Create" ? true : false}
                    value="Create"
                    onChange={onSiteChanged}
                  />
                  <label htmlFor="CreateCustomTable" className="radio-label">
                    Create Custom Table
                  </label>
                </div>
              )}
            </div>
            <div className="custom-table">
              {!viewExperiment && (
                <div className="selector-wrapper select-table-size">
                  {radioButtonValue == "Predefined" && (
                    <Select
                      styles={customSelectStyles}
                      placeholder="Select table size"
                      options={selectTableOptions}
                      isSearchable={false}
                      defaultValue={selectedTableValue}
                      value={selectedTableValue}
                      onChange={(value) => {
                        handleSelectTableValue(value);
                      }}
                    />
                  )}
                </div>
              )}
              {!viewExperiment && radioButtonValue == "Create" && (
                <div className="input-group-wrapper">
                  <div className="input-btn-group">
                    <label>Column</label>
                    <div className="btn-group">
                      <button
                        onClick={() =>
                          CountColumn > 1 && setCountColumn(CountColumn - 1)
                        }
                      >
                        <BiMinus />
                      </button>
                      <input value={CountColumn} placeholder="0" type="text" />
                      <button onClick={() => setCountColumn(CountColumn + 1)}>
                        <BiPlus />
                      </button>
                    </div>
                  </div>
                  <div className="input-btn-group">
                    <label>Row</label>
                    <div className="btn-group">
                      <button
                        onClick={() =>
                          CountRow > 1 && setCountRow(CountRow - 1)
                        }
                      >
                        <BiMinus />
                      </button>
                      <input value={CountRow} placeholder="0" type="text" />
                      <button onClick={() => setCountRow(CountRow + 1)}>
                        <BiPlus />
                      </button>
                    </div>
                  </div>
                </div>
              )}
            </div>
            {tableError && (
              arraywitharray?.length > 0 ? <></> : <p style={{ color: "red" }}>Please create table</p>
            )}
          </div>

          {!viewExperiment && (
            <div className="create-btn">
              <button
                onClick={() => handleCreatTableClicked()}
                className="primary-btn"
              >
                Create Table
              </button>
            </div>
          )}
          {viewExperiment && (
            <div>
              <p>{`Table size ${CountRow} rows X ${CountColumn} columns (${CountRow * CountColumn
                } cells) `}</p>
            </div>
          )}
          {arraywitharray && (
            <ul className="row-column-btn-group">
              <div className="row-column-btn-group-head">
                <div className="switch-group">
                  {arraywitharray && (
                    <div>
                      {!viewExperiment && (
                        <Switch
                          onChange={handleChange}
                          checked={checked}
                          offColor={"#656565"}
                          onColor={"#1C6FE9"}
                          offHandleColor={"#656565"}
                          onHandleColor={"#1C6FE9"}
                          className="react-switch"
                        />
                      )}
                    </div>
                  )}
                  {!viewExperiment && <p>Do you want to copy cell data?</p>}
                  {checked && <span>(Please select cell)</span>}
                  {saveCopyedText?.position && <span>(Press Ctrl for multiple selections)</span>}
                </div>
                {checked && (
                  <div className="copy-paste-btn">
                    {copyedClicked && (
                      <p>{saveCopyedText?.position!= undefined && `${saveCopyedText?.position[0]?.toString() + "," + (saveCopyedText?.position[1] + 1 )?.toString()} Cell Copied`}</p>
                    )}
                    <a
                      onClick={() => {
                        setCopyedClicked(true);
                        handleSavedCopyedText();
                      }}
                      className="add-filter primary-btn"
                    >
                      Copy Cell <AiOutlineCopy />{" "}
                    </a>
                    {saveCopyedText?.position!= undefined &&<a
                      onClick={() => copyedClicked && handlePastClicked()}
                      className={
                        !copyedClicked
                          ? "apply-filter primary-btn disable"
                          : "apply-filter primary-btn"
                      }
                    >
                      Paste Cell <IoMdCopy />
                    </a>}
                  </div>
                )}
              </div>
              <li>
                <div style={{ padding: 20 }} className="demo">

                  {arraywitharray &&
                    arraywitharray?.map((row, i) => (
                      <div key={i}>
                        {row.map((col, j) => {
                          return (
                            <span
                              style={{
                                cursor: "pointer",
                                backgroundColor: props?.match?.params?.slug ? (col?.answer?.group?.length > 0 ? col?.answer?.group[0]?.color : null) : col?.answer?.color,
                                borderColor: col?.fields == '' ? 'none' : col?.cell_key && "blue"
                              }}
                              onClick={() => {
                                checked && setCopyedShell(col);
                                !checked && setModalOpen(true);
                                setShellData(col);
                              }}
                              key={j}
                              className={
                                copyedShell?.position == col?.position
                                  ? "column-btn selected"
                                  : "column-btn"
                              }
                            >
                              {col?.position[0] +""+ Number(col?.position[1] +1)}
                            </span>
                          )
                        })}
                      </div>
                    ))}
                </div>
              </li>
            </ul>
          )}

        </div>
        <div className="upload-bio-wrapper">
          <div className="title-step">
            <span>4</span>
            <label>upload Bio-Test document</label>
          </div>
          <div className="upload-file-group-wrapper">
            <div className="upload-file-group">
              <div className="upload-file-item">
                {filetypes.map((item, i) => {
                  return item == "JPEG" ? (
                    <img src={JPEGImage} alt={""} />
                  ) : item == "CSV" ? (
                    <img src={CSVImage} alt={""} />
                  ) : item == "xlsx" ? (
                    <img src={XLXSImage} alt={""} />
                  ) : item == "TXT" ? (
                    <img src={TEXTImage} alt={""} />
                  ) : item == "XML" ? (
                    <img src={XMLImage} alt={""} />
                  ) : item == "JPG" ? (
                    <img src={JPGImage} alt={""} />
                  ) : item == "DOCX" ? (
                    <img src={DOCImage} alt={""} />
                  ) : (
                    <img src={PDFImage} alt={""} />
                  );
                })}
                <div
                  className="upload-file-btn"
                >
                  {!viewExperiment && <input
                    onChange={(e) => {
                      handleFileChange2(e);
                    }}
                    className="choose-file-input"
                    type={"file"}
                    placeholder="Enter Price"
                  />}
                  {!viewExperiment && <span className="choose-file-name">Upload</span>}
                  {viewExperiment ? (
                    <Base64Downloader
                      base64={base64Xlsx}
                      downloadName="1x1_red_pixel"
                      Tag="a"
                      extraAttributes={{ href: '#' }}
                     ><span className="choose-file-name">Download</span></Base64Downloader>) : (
                    slug >= parseInt(1) ? (<p>{saveFatherBioTestPath}</p>) : (<p>{fileName}</p>)
                  )}
                </div>
              </div>
            </div>
            {fileError && (
              <p style={{ color: "red" }}>Please upload file</p>
            )}
          </div>
        </div>
        {!viewExperiment &&
          (slug >= parseInt(1) ? (
            <div className="form-submit-btn">
              <button
                onClick={() => handleSubmit2()}
                className=" primary-btn"
              >
                Save
              </button>
            </div>
          ) : (
            <div className="form-submit-btn">
              <button
                onClick={() => handleSubmit()}
                className="primary-btn"
              >
                Submit
              </button>
            </div>
          ))}
        {checked && copyedClicked && (
          <Selecto
            container={document.querySelector(".demo")}
            dragContainer={document.querySelector(".demo")}
            selectableTargets={[".column-btn"]}
            selectByClick={true}
            selectFromInside={true}
            continueSelect={false}
            toggleContinueSelect={"shift"}
            keyContainer={document.querySelector(".demo")}
            hitRate={100}
            onSelect={(e) => {
              e.added.forEach(async (el) => {
                el.classList.add("selected");
              });
              e.removed.forEach(async (el) => {
                el.classList.remove("selected");
              });
            }}
            onSelectEnd={(e) => {
              setArray([]);
              e.afterAdded.forEach((el) => {
                setArray((prevItem) => [...prevItem, el.innerHTML]);
                el.classList.add("selected");
              });
            }}
          />
        )}
      </div>
    </Layout>
  );
}

/**
 * This function manage saveExperimentData in redux store
 */
const mapStateToProps = (state) => ({
  fatherData: state.homeReducer.fatherData
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ clearExperimentData, saveExperimentData }, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UploadExperiment)
