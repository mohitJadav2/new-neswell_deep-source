import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import './experiment.style.scss'
import { AiFillCloseCircle } from "react-icons/ai";
import Layout from '../../Layout';
import UploadExperimentApi from '../../services/UploadExperimentApi';
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import Loader from "../../components/loader";
import { saveExperimentData } from "../../Actions/HomeAction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

/** 
  * this style is use for the selection fild 
*/
const customSelectStyles = {
    option: (provided, state) => ({
        ...provided,
        color: state.isSelected ? '#fff' : '#404040',
        padding: 10,
        borderRadius: 10,
        width: "98%",
        margin: '10px auto',
    }),
    control: (styles) => ({ ...styles, border: 'solid 1px #E5E5E5', backgroundColor: 'white', maxHeight: 46, minHeight: 46, fontSize: 14, borderRadius: 10, color: '#929292', fontWeight: '600' }),
}

function CreateExperiment(props) {
    let history = useHistory();
    const [loader, setLoader] = useState(false);
    const [experimentList, setExperimentList] = useState([]);
    const [experiment, setExperiment] = useState(undefined);

    const [group, setGroup] = useState(undefined);
    const [groupArray, setGroupArray] = useState([]);

    const [faterBioTestData, setFaterBioTestData] = useState([]);
    const [faterBioData, setFaterBioData] = useState([]);

    const [extractList, setExtractList] = useState([]);
    const [extract, setExtract] = useState([]);

    const [clusterList, setClusterList] = useState([]);
    const [cluster, setCluster] = useState([]);

    const [experimentError, setExperimentError] = useState(undefined);
    const [groupError, setGroupError] = useState(undefined);
    const [extractError, setExtractError] = useState(undefined);
    const [clusterError, setClusterError] = useState(undefined);
    const [bioTest, setBioTest] = useState()

    /**
     * This useEffect() manage Bio-test, extract and experiment records from API  
    */
    useEffect(() => {
        setLoader(true)
        UploadExperimentApi.getFaterBioTest(localStorage.getItem("neswell_access_token")).then(
            async (json) => {
                if (!json?.data?.error) {
                    await json?.data?.map((item) => {
                        setFaterBioTestData(prevItems => [...prevItems, {
                            value: item?.id,
                            label: item?.name
                        }]);
                        !props?.fatherData && setGroupArray(['default'])
                    })
                } else {
                    setLoader(false)
                }
            })
        UploadExperimentApi.fetchExtractsData(localStorage.getItem("neswell_access_token")).then(
            async (json) => {
                if (!json?.data?.error) {
                    await json?.data?.map((item) => {
                        setExtractList(prevItems => [...prevItems, {
                            value: item?.id,
                            label: item?.name
                        }]);
                    })
                } else {
                    setLoader(false)

                }
            }
        )

        UploadExperimentApi.getExperimentAllData(localStorage.getItem("neswell_access_token")).then(
            async (json) => {
                if (!json?.data?.error) {
                    await json?.data?.map((item) => {
                        setExperimentList(prevItems => [...prevItems, {
                            value: item,
                            label: item?.name
                        }]);
                    })
                    setTimeout(() => {
                        setLoader(false)
                    }, 3000)
                } else {
                    setLoader(false)
                }
            }
        )
        setExperiment(props?.fatherData?.experiment)
        setCluster(props?.fatherData?.cluster)
        setExtract(props?.fatherData?.extract)
        let groupArrayData = props?.fatherData?.groupArray.map((item) => item.label)
        setGroupArray(groupArrayData)
        setFaterBioData(props?.fatherData?.faterBioData)
    }, [])

    /**
     * This function manage  Cluster records from API 
    */
    const filteredCluster = (id) => {
        setLoader(true)
        setClusterList([])
        UploadExperimentApi.getFatherFilterCulster(localStorage.getItem("neswell_access_token"), id).then(
            async (json) => {
                if (!json?.data?.error) {
                    await json?.data?.map((item) => {
                        setClusterList(prevItems => [...prevItems, {
                            value: item?.id,
                            label: item?.name
                        }]);
                    })
                    setLoader(false)
                } else {
                    setLoader(false)
                }
            }
        )
    }

    /**
     * This function manage data bio-test, extract and clusters data according to experiments from API
    */

    const test = async (value) => {
        setLoader(true)
        const v1 = await faterBioTestData.filter(item => item?.value === value?.value?.general_bio_test_id);
        setFaterBioData(v1)
        setClusterList([])
        setCluster([])
        setExtract([])
        UploadExperimentApi.getExperimentOptionData(localStorage.getItem("neswell_access_token"), value?.value?.id).then(
            async (json) => {
                if (!json?.data?.error) {
                    await json?.data?.extracts.map((item) => {
                        setExtract(prevItems => [...prevItems, {
                            value: item?.value,
                            label: item?.label
                        }])
                    })
                    await json?.data?.clusters.map((item) => {
                        setCluster(prevItems => [...prevItems, {
                            value: item?.value,
                            label: item?.label
                        }])
                    })
                    setLoader(false)
                } else {
                    setLoader(false)
                }

            }
        )
        UploadExperimentApi.getFatherFilterCulster(localStorage.getItem("neswell_access_token"), v1[0]?.value).then(
            async (json) => {
                if (!json?.data?.error) {
                    await json?.data?.map((item) => {

                        setClusterList(prevItems => [...prevItems, {
                            value: item,
                            label: item?.name
                        }]);
                    })
                } else {

                }
            }
        )
    }

    /**
     * This function use for when user press enter list data set in group input box
    */
    const handleKeyDown = (e) => {
        if (e.key === 'Enter' && group) {
            setGroup(undefined)
            setGroupArray(prevItems => [...prevItems,
                group
            ])
        }
    }
    /**
     * This function use for remove data from group input box 
    */
    const handleDeleteClick = (value) => {
        let updatedArray = groupArray?.filter((item) => item != value)
        setGroupArray(updatedArray)
    }

    /**
     * This function manage user click submit button set group, extract, experiment , cluster and bio-test data to redux store and local storage
     * And after submit the data user redirect to phase-2
    */
    const handleSubmit = async () => {
        let array_group = []
        var letters = 'BCDEF'.split('');
        var color = '#'
        for (let i = 0; i < groupArray?.length; i++) {
            array_group.push({
                value: i + 1,
                label: groupArray[i],
                color: color + letters[Math.floor(Math.random() * letters.length)] + letters[Math.floor(Math.random() * letters.length)] + letters[Math.floor(Math.random() * letters.length)] + letters[Math.floor(Math.random() * letters.length)] + letters[Math.floor(Math.random() * letters.length)] + letters[Math.floor(Math.random() * letters.length)]
            })
        }
        let reduxData = {
            faterBioData: faterBioData,
            extract: extract,
            experiment: experiment,
            cluster: cluster,
            groupArray: array_group

        }
        props.saveExperimentData(reduxData)
        localStorage.setItem("faterBioData", JSON.stringify(faterBioData))
        localStorage.setItem("extract", JSON.stringify(extract))
        localStorage.setItem("experiment", JSON.stringify(experiment))
        localStorage.setItem("cluster", JSON.stringify(cluster))
        localStorage.setItem("groupArray", JSON.stringify(array_group))
        if (faterBioData && groupArray?.length > 0 && extract?.length > 0 && cluster?.length > 0) {
            history.push('/upload-experiment')
        } else {
            if (!bioTest) {
                setExperimentError("Please select Bio Test")
            }
            if (groupArray?.length == 0 || groupArray == 'default') {

                setGroupError("This fild is reuired")
            }
            if (!extract) {
                setExtractError("Please select extracts")
            }
            if (!cluster) {
                setClusterError("Please select the Experimental Condition")
            }
        }

    }

    return (
        <Layout>
            {loader && <Loader />}
            <div className="add-users-details-wrapper">
                <div className="users-title">
                    <h2>Experiment Data</h2>
                </div>
                <div className="add-user-form-wrapper create-experiment-data">
                    <div className="create-bio-form">
                        <div className="form-group">
                            <span> <label>1</label>Based on Experiment</span>

                            <Select
                                styles={customSelectStyles}
                                placeholder="None"
                                options={experimentList}
                                isSearchable={false}
                                value={experiment}
                                onChange={(value) => {
                                    setExperiment(value)
                                    test(value)
                                }}
                            />
                        </div>

                        <div className="form-group">
                            <span> <label>2</label>Bio Test</span>
                            <Select
                                id="bio-test"
                                styles={customSelectStyles}
                                placeholder="Select Bio test"
                                options={faterBioTestData}
                                isSearchable={false}
                                value={faterBioData}
                                onChange={(value) => {
                                    setFaterBioData(value)
                                    setBioTest(value)
                                    filteredCluster(value?.value)
                                    setExperimentError(undefined)
                                }}
                            />
                        </div>
                        {experimentError && <p style={{ color: "red" }}>{experimentError}</p>}
                        <div className="form-group">
                            <span> <label>3</label>Experimental Group</span>
                            <div className='group-name-selected'>
                                {groupArray?.map((item) => (
                                    <p> {item} <AiFillCloseCircle onClick={() => handleDeleteClick(item)} /> </p>
                                ))}

                                <input
                                    type="text"
                                    className='add-group-input'
                                    placeholder="Enter experimental group"
                                    onKeyDown={handleKeyDown}
                                    value={group ? group : ''}
                                    onChange={e => {
                                        setGroup(e.target.value)
                                        setGroupError(undefined)
                                    }}
                                />

                            </div>
                            {groupError && <p style={{ color: "red" }}>{groupError}</p>}
                        </div>
                        <div className="form-group">
                            <span> <label>4</label> Extracts</span>
                            <Select
                                styles={customSelectStyles}
                                placeholder="Select Extracts"
                                options={extractList}
                                closeMenuOnSelect={false}
                                isSearchable={false}
                                isMulti
                                value={extract}
                                onChange={(value) => {
                                    setExtract(value)
                                    setExtractError(undefined)
                                }}
                            />
                            {extractError && <p style={{ color: "red" }}>{extractError}</p>}
                        </div>
                        <div className="form-group">
                            <span> <label>5</label>Experimental Condition</span>
                            <Select
                                styles={customSelectStyles}
                                placeholder="Select experimental condition"
                                options={clusterList}
                                closeMenuOnSelect={false}
                                isSearchable={false}
                                isMulti
                                value={cluster}
                                onChange={(value) => {
                                    setCluster(value)
                                    setClusterError(undefined)
                                }}
                            />
                            {clusterError && <p style={{ color: "red" }}>{clusterError}</p>}
                        </div>
                    </div>

                    <div className="form-submit-btn">
                        <Link className="link-submit">
                            <button onClick={() => handleSubmit()} className=" primary-btn">Create</button>
                        </Link>
                    </div>

                </div>
            </div>
        </Layout>
    );
}

/**
 * This function manage SaveExperimentData from redux store
 */
function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ saveExperimentData }, dispatch)
    }
}

const mapStateToProps = (state) => ({
    fatherData: state.homeReducer.fatherData
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateExperiment)