import React, { useState, useEffect, useRef } from "react";
import Layout from "../../Layout";
import "./bioTest.style.scss";
import { FiSearch } from "react-icons/fi";
import { AiOutlineSetting } from "react-icons/ai";
import { BsPlusCircle } from "react-icons/bs";
import PdfIcon from "../../assets/pdf-large.svg";
import { connect } from "react-redux";
import BioTestApi from "../../services/BioTestApi";
import * as moment from "moment";
import { useHistory } from "react-router-dom";
import Loader from "../../components/loader";
import Modal from "../../components/ModalComponent";
import { saveFatherBioTestNumber } from "../../Actions/HomeAction";
import { bindActionCreators } from "redux";

function BioTestList(props) {
  const [fatherData, setFatherData] = useState(undefined);
  const [childBioTestList, setChildBioTestList] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [error, setError] = useState(undefined);
  let history = useHistory();
  const [openDropDown, setOpenDropDown] = useState(undefined);
  let id = props?.match?.params?.slug;

  /**
   * This useEffect() manage the father bio test details
   * After successfully calling the father bio test detail,This function manage the child bio test data using the father bio test id
   */
  useEffect(() => {
    setLoader(true);
    BioTestApi.fetchFatherDetailData(
      localStorage.getItem("neswell_access_token"),
      id
    ).then((json) => {
      if (!json?.data?.error) {
        setFatherData(json?.data);
        BioTestApi.fetchChildBasedOnParent(
          localStorage.getItem("neswell_access_token"),
          json?.data?.id
        ).then((json) => {
          if (!json?.data?.error) {
            setChildBioTestList(json?.data);
            setLoader(false);
          } else {
            setLoader(false);
          }
        });
      } else {
        setLoader(false);
      }
    });
  }, []);

  /**
   * This function manage search feature
   * This function passes user's enter value to API and get positive and error response
   */
  const handleSearch = (value) => {
    BioTestApi.searchchildBioTestes(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug,
      value
    ).then((json) => {
      if (!json?.data?.error) {
        setChildBioTestList(json?.data);
        if (json?.data?.length == 0) {
          setError("No data available");
        } else {
          setError(undefined);
        }
      } else {
        setError(json?.data?.error);
      }
    });
  };

  /**
   * When user clicked delete button ,this function manage user delete feature
   * The function called delete Api with user's id
   */
  const handleDeletChild = (item) => {
    setModalOpen(false);
    setLoader(true);
    BioTestApi.DeleteAndDetailChildBioTest(
      localStorage.getItem("neswell_access_token"),
      deleteUserData.id,
      "DELETE"
    ).then((json) => {
      if (!json?.data?.error) {
        BioTestApi.fetchChildBasedOnParent(
          localStorage.getItem("neswell_access_token"),
          props?.match?.params?.slug
        ).then((json) => {
          if (!json?.data?.error) {
            setLoader(false);
            setChildBioTestList(json?.data);
          } else {
            setModalOpen(false);
            setLoader(false);
          }
        });
      } else {
        setModalOpen(false);
        setLoader(false);
      }
    });
  };

  /**
   * This function manage redirect to view bio details page
   */
  const handleViewChildBio = (id) => {
    props.saveFatherBioTestNumber(props?.match?.params?.slug);
    history.push(`/view-bio-details/${id}`);
  };

  /**
   * This function manage Duplicate sub bio test button and user redirect to create sub bio test page
   */
  const handleDuplicateChild = (id) => {
    history.push({
      pathname: `/create-bio/${id}`,
      state: { userDetail: props?.match?.params?.slug },
    });
  };

  const handlePredictSubBio = (id) => {
    history.push(`/sub-bio-test-predict/${id}`);
  };
  const handleScoreChildBio = (id) => {
    history.push(`/sub-bio-test-score/${id}`);
  };
  /**
   * This fucntion manage to redirect to create bio page for creat a new sub bio test
   */
  const handleCreatClick = () => {
    props.saveFatherBioTestNumber(props?.match?.params?.slug);
    history.push({
      pathname: `/create-bio`,
      state: { userDetail: props?.match?.params?.slug },
    });
  };

  /**
   * This function manage popof setting for every user
   */

  const handleSettingClick = async (item) => {
    openDropDown == item?.id
      ? setOpenDropDown(undefined)
      : setOpenDropDown(item?.id);
  };
  /**
   * This function manage click outside of the popup box
   */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenDropDown(undefined);
        }
      }
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }
  /**this ref is for the when user clicked outside the fucntion */
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);
  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => handleDeletChild()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={deleteModal}
      />
      <div className="bio-test-view-wrapper">
        <div className="bio-test-details">
          <div className="bio-name">
            <h2>{fatherData?.name}</h2>
            <p>Bio-Test</p>
          </div>
          <div className="bio-descrition">
            <span>Description</span>
            <p>{fatherData?.description}</p>
          </div>
          <div className="bio-file">
            <p>
              Files Required{" "}
              <span>{`(${fatherData?.files_required?.length})`}</span>
            </p>
            <div className="bio-test-file-view">
              {fatherData?.files_required?.map((item) => (
                <div className="bio-test-file-column">
                  <div className="file-img">
                    <img src={PdfIcon} alt="" />
                    <p>{`Max Size ${item?.max_size}MB`}</p>
                  </div>
                  <h3>{item?.descriptor}</h3>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="child-bio-test-table-list">
          <div className="child-bio-test-list-title">
            <h2>
              Sub Bio Tests List <span>{`(${childBioTestList?.length})`}</span>
            </h2>
            <button
              onClick={() => handleCreatClick()}
              className="add-filter primary-btn"
            >
              {" "}
              <BsPlusCircle /> Create Sub Bio Test{" "}
            </button>
          </div>
          <div className="bio-test-search">
            <div className="search">
              <button className="search-icon">
                {" "}
                <FiSearch />{" "}
              </button>
              <input
                onChange={(e) => handleSearch(e.target.value)}
                type="search"
                placeholder="Search"
                className="search-input"
              />
            </div>
          </div>
          <div className="list-view-table details-table">
            <table className="sub-bio-test-table">
              <thead>
                <tr>
                  <th style={{ width: "40px" }}>Sr.</th>
                  <th>Sub Bio-Tests</th>
                  <th>General Bio-test</th>
                  <th>Created at</th>
                  <th>Created by</th>
                  <th>Scored</th>
                  <th
                    style={{
                      width: "140px",
                      textAlign: "left",
                      cursor: "context-menu",
                    }}
                  >
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {childBioTestList?.map((item) => (
                  <tr>
                    <td
                      onClick={() => {
                        handleViewChildBio(item.id);
                      }}
                      style={{ cursor: "pointer" }}
                    >
                      {item?.id}
                    </td>
                    <td
                      onClick={() => {
                        handleViewChildBio(item.id);
                      }}
                      style={{ cursor: "pointer" }}
                    >
                      {item?.name.includes("_")
                        ? item?.name.split("__")[0] +
                          "  " +
                          moment(item?.name?.split("__")[1]).format(
                            "(DD-MM-YYYY  h:mm a)"
                          )
                        : item?.name}
                    </td>
                    <td
                      onClick={() => {
                        handleViewChildBio(item.id);
                      }}
                      style={{ cursor: "pointer" }}
                    >
                      {item?.general_bio_test_id}
                    </td>
                    <td
                      onClick={() => {
                        handleViewChildBio(item.id);
                      }}
                      style={{ cursor: "pointer" }}
                    >
                      {item?.created}
                    </td>
                    <td
                      onClick={() => {
                        handleViewChildBio(item.id);
                      }}
                      style={{ cursor: "pointer" }}
                    >
                      {item?.user_id}
                    </td>
                    <td
                      onClick={() => {
                        handleViewChildBio(item.id);
                      }}
                      style={{ cursor: "pointer" }}
                    >
                      {item?.scored === 1 ? "True" : "False"}
                    </td>
                    <td>
                      <span
                        className="blue-color-eyes"
                        onClick={() => handleSettingClick(item)}
                      >
                        <AiOutlineSetting style={{ color: "#404040" }} />
                        {openDropDown == item?.id && (
                          <div ref={wrapperRef} className="user-setting-popup">
                            <p
                              onClick={() => {
                                handleViewChildBio(item.id);
                              }}
                            >
                              View
                            </p>
                            <p
                              onClick={() => {
                                handleScoreChildBio(item?.id);
                              }}
                            >
                              Score
                            </p>
                            <p
                              onClick={() => {
                                handlePredictSubBio(item?.id);
                              }}
                            >
                              Predict
                            </p>
                            <p
                              onClick={() => {
                                handleDuplicateChild(item.id);
                              }}
                            >
                              Duplicate
                            </p>
                            <p
                              onClick={() => {
                                setDeleteModal(true);
                                setModalOpen(true);
                                setDeleteUserData(item);
                              }}
                            >
                              Remove
                            </p>
                          </div>
                        )}
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div>
            <p style={{ textAlign: "center" }}>{error}</p>
          </div>
        </div>
      </div>
    </Layout>
  );
}

const mapStateToProps = (state) => ({
  parentBioTestData: state.homeReducer.parentBioTestData,
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ saveFatherBioTestNumber }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BioTestList);
