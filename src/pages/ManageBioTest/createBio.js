import React, { useState, useEffect } from "react";
import Select from "react-select";
import "./bioTest.style.scss";
import Layout from "../../Layout";
import BioTestApi from "../../services/BioTestApi";
import { useHistory, useLocation } from "react-router-dom";
import Loader from "../../components/loader";

/** this style is use for the selection fild */
const customSelectStyles = {
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? "#fff" : "#404040",
    padding: 10,
    borderRadius: 10,
    width: "98%",
    margin: "10px auto",
  }),
  control: (styles) => ({
    ...styles,
    border: "solid 1px #E5E5E5",
    backgroundColor: "white",
    maxHeight: 46,
    minHeight: 46,
    fontSize: 14,
    borderRadius: 10,
    color: "#929292",
    fontWeight: "600",
  }),
};

function CreateBio(props) {
  const [fatherBioTestList, setFatherBioTestList] = useState([]);
  const [clusterList, setClusterList] = useState([]);
  const [fatherBioTest, setFatherBioTest] = useState([]);
  const [cluster, setCluster] = useState([]);
  const [childBioTest, setChildBioTest] = useState(undefined);
  const [fatherError, setFatherError] = useState(undefined);
  const [childError, setChildError] = useState(undefined);
  const [clusterError, setClusterEror] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState(undefined);
  let history = useHistory();
  const location = useLocation();
  const [supportedExtractName, setSupportedExtractName] = useState(undefined);
  const [supportedExtractError, setSuppoeredExtractError] = useState(false);
  let data = location?.state?.userDetail;
  let id = props?.match?.params?.slug;
  useEffect(() => {
    setLoader(true);
    /** from this api we get father list which we show into the select component of react */
    BioTestApi.AllFatherList(localStorage.getItem("neswell_access_token")).then(
      async (json) => {
        if (!json?.data?.error) {
          await json?.data?.results?.map((item) => {
            setFatherBioTestList((prevItems) => [
              ...prevItems,
              {
                value: item?.id,
                label: item?.name,
              },
            ]);
            if (item?.id == data) {
              setFatherBioTest({
                value: item?.id,
                label: item?.name,
              });
            }
          });
        } else {
          setLoader(false);
        }
      }
    );
    /** from this api we get Cluster list which we show into the select component of react */
    BioTestApi.getFatherFilterCulster(
      localStorage.getItem("neswell_access_token"),
      data
    ).then(async (json) => {
      if (!json?.data?.error) {
        await json?.data?.map((item) => {
          setClusterList((prevItems) => [
            ...prevItems,
            {
              value: item?.id,
              label: item?.name,
            },
          ]);
        });
        setLoader(false);
      } else {
        setLoader(false);
      }
    });

    /** if we come from the duplicate bio test then we call this api and get the all data of the child and show it into the all fild */
    props?.match?.params?.slug &&
      BioTestApi.DuplicateBioTestGet(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug
      ).then(async (json) => {
        if (!json?.data?.error) {
          setFatherBioTest(json?.data?.general_bio_test_id);
          setChildBioTest(json?.data?.child_bio_test_name);
          setSupportedExtractName(json?.data?.suppoerted_extract);
          await json?.data?.clusters?.map(
            async (item) =>
              await setCluster((prevItems) => [
                ...prevItems,
                {
                  value: item?.value,
                  label: item?.label,
                },
              ])
          );
          json?.data?.suppoerted_extract.length === 0
            ? setSuppoeredExtractError(true)
            : setSuppoeredExtractError(false);
        } else {
        }
      });
  }, []);

  /**
   * this function is called on the click of the submit button
   */
  const handleCreatClicked = async () => {
    /** this if conditioin is for make sure that all the data is filled properly other wise it go to the else part */
    setLoader(true);
    if (fatherBioTest && childBioTest && cluster) {
      setFatherError(undefined);
      setChildError(undefined);
      setClusterEror(undefined);
      let formData = {};
      formData["user_id"] = 5;
      formData["name"] = childBioTest;
      formData["clusters"] = cluster;
      formData["general_bio_test_id"] = fatherBioTest?.value;
      formData["is_active"] = true;
      formData["description"] = "New sub bio test";
      if (props?.match?.params?.slug) {
        /** this is for the duplicate the child bio test */
        BioTestApi.DuplicateBioTestPost(
          localStorage.getItem("neswell_access_token"),
          props?.match?.params?.slug,
          formData
        ).then(async (json) => {
          if (!json?.data?.error) {
            if (localStorage.getItem("subBioTestes") == "subBioTestes") {
              history.push(
                `/view-bio-details/${json?.data?.response?.messages}`
              );
            } else {
              history.push(`/bio-test-view/${fatherBioTest?.value}`);
            }
            setLoader(false);
          } else {
            setLoader(false);
            setError(json?.data?.error);
          }
        });
      } else {
        setLoader(true);
        /** this is for the creating a new bio test */
        BioTestApi.creatChildBioTest(
          localStorage.getItem("neswell_access_token"),
          formData
        ).then((json) => {
          if (!json.data.error) {
            history.push(`/view-bio-details/${json?.data?.response?.messages}`);
            setLoader(false);
          } else {
            setLoader(false);
            setError(json?.data?.error);
          }
        });
      }
      /** in this else we check which fild is remaining for the filled up and as per the requirment we will set error massage */
    } else {
      setLoader(false);
      if (!fatherBioTest) {
        setFatherError("Please select father bio test");
      } else {
        setFatherError(undefined);
      }
      if (!childBioTest) {
        setChildError("Please enter sub bio test name");
      } else {
        setChildError(undefined);
      }
      if (cluster.length == 0) {
        setClusterEror("Please select experimental condition");
      } else {
        setClusterEror(undefined);
      }
    }
  };

  /**This function is use for when user select bio-test and according to 
        that bio test cluter set in examination Condition drop daow list option*/
  const ClusterListData = (value) => {
    setClusterList([]);
    BioTestApi.getFatherFilterCulster(
      localStorage.getItem("neswell_access_token"),
      value
    ).then(async (json) => {
      if (!json?.data?.error) {
        await json?.data?.map((item) => {
          setClusterList((prevItems) => [
            ...prevItems,
            {
              value: item?.id,
              label: item?.name,
            },
          ]);
        });
      } else {
        setLoader(false);
      }
    });
  };
  /**
   * This function manage supported extract according to cluster list
   */
  const supportedExtract = (value) => {
    setSupportedExtractName(undefined);
    let cluterArray = value?.map((item) => item?.value);
    BioTestApi.getFilterExtract(
      localStorage.getItem("neswell_access_token"),
      fatherBioTest?.value,
      JSON.stringify(cluterArray)
    ).then(async (json) => {
      if (!json?.data?.error) {
        setSupportedExtractName(json?.data?.response);
        setSuppoeredExtractError(false);
      } else {
        setSuppoeredExtractError(true);
      }
    });
  };
  return (
    <Layout>
      {loader && <Loader />}
      <div className="add-users-details-wrapper">
        <div className="users-title">
          <h2>Create Sub Bio Test</h2>
        </div>
        <div className="add-user-form-wrapper">
          <div className="create-bio-form">
            <div className="form-group">
              <span>Bio-Test</span>
              <Select
                onChange={(value) => {
                  setFatherBioTest(value);
                  ClusterListData(value.value);
                }}
                value={fatherBioTest}
                onSelect
                styles={customSelectStyles}
                placeholder="Select Father Bio Test"
                options={fatherBioTestList}
              />
              {fatherError && <p style={{ color: "red" }}>{fatherError}</p>}
            </div>
            <div className="form-group">
              <span>Sub Bio Test</span>
              <input
                value={childBioTest}
                onChange={(e) => setChildBioTest(e.target.value)}
                className="create-bio-input"
                type="text"
                placeholder="Enter sub bio test name"
              />
              {childError && <p style={{ color: "red" }}>{childError}</p>}
            </div>
            <div className="form-group">
              <span>Experimental Condition</span>
              <Select
                value={cluster}
                onChange={(value) => {
                  setCluster(value);
                  supportedExtract(value);
                }}
                isMulti
                styles={customSelectStyles}
                placeholder="Select experimental condition"
                options={clusterList}
                closeMenuOnSelect={false}
                isSearchable={false}
              />
              {clusterError && <p style={{ color: "red" }}>{clusterError}</p>}
            </div>
            <div className="form-group view-bio-details-item">
              <span>Supported Extract</span>
              <div className="supported-extract ">
                <p style={{ display: "flex" }}>
                  {supportedExtractName?.map((item) => (
                    <span>
                      {/* <AiOutlineMinus /> */}
                      {item}
                    </span>
                  ))}
                </p>
                {supportedExtractError && (
                  <p style={{ color: "red" }}>Data is unavailable.</p>
                )}
              </div>
            </div>
          </div>
          <div className="form-submit-btn">
            {error && <p style={{ color: "red" }}>{error}</p>}
            <button
              onClick={() => handleCreatClicked()}
              className="primary-btn"
            >
              Create
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default CreateBio;
