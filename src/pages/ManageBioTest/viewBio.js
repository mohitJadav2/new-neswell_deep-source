import React, { useState, useEffect } from "react";
import "./bioTest.style.scss";
import { MdContentCopy } from "react-icons/md";
import { AiOutlineFileSearch, AiOutlineMinus } from "react-icons/ai";
import Layout from "../../Layout";
import BioTestApi from "../../services/BioTestApi";
import { Link, useHistory } from "react-router-dom";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import { GrScorecard } from "react-icons/gr";
import ScoringGraph from "../SubBioTest/ScoringGraph";
import PopUp from "./PopUp";
import Trash from "../../assets/icons/trash.svg";

function ViewBio(props) {
  const [childBioTestData, setChildBioTestData] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [clusterList, setClusterList] = useState([]);
  const [supportedExtract, setSupportedExtract] = useState([]);
  const [fatherBioTest, setfatherBioTest] = useState([]);
  const [extractError, setExtractError] = useState(false);
  let history = useHistory();
  const [extractModalOpen, setExtractModalOpen] = useState(false);
  const [AllSuportedExtract, setAllSupportedExtract] = useState([]);
  /**
   * This function is use for get sub bio test details from api
   */
  useEffect(() => {
    setLoader(true);
    BioTestApi.DeleteAndDetailChildBioTest(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug,
      "GET"
    ).then((json) => {
      if (!json?.data?.error) {
        setChildBioTestData(json?.data);
        setClusterList(json?.data?.clusters);
        setfatherBioTest(json?.data?.general_bio_test_id);
        setSupportedExtract(json?.data?.suppoerted_extract);
        setAllSupportedExtract(json?.data?.suppoerted_extract);
        if (json?.data?.suppoerted_extract?.length === 0) {
          setExtractError(true);
        } else {
          setExtractError(false);
        }
        setTimeout(() => {
          setLoader(false);
        }, 5000);
      } else {
        setLoader(false);
      }
    });
  }, []);

  /**This function is use for delete sub bio test record */
  const handleDeleteChild = () => {
    setLoader(true);
    BioTestApi.DeleteAndDetailChildBioTest(
      localStorage.getItem("neswell_access_token"),
      deleteUserData,
      "DELETE"
    ).then((json) => {
      if (!json?.data?.error) {
        if (localStorage.getItem("subBioTestes") == "subBioTestes") {
          history.push(`/sub-bio-test-list`);
        } else {
          history.push(`/bio-test-view/${fatherBioTest?.value}`);
        }
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  };
  /**This function is use for when user clicked on Duplicate sub bio test button and user redirect to create sub bio test page*/
  const handleDuplicateChild = (id) => {
    history.push({
      pathname: `/create-bio/${id}`,
      state: { userDetail: "" },
    });
  };
  const handleSubBioTestScore = (id) => {
    history.push({
      pathname: `/sub-bio-test-score/${id}`,
      state: { userDetail: fatherBioTest?.value },
    });
  };
  const handleSubBioTestPredict = (id) => {
    history.push({
      pathname: `/sub-bio-test-predict/${id}`,
      state: { userDetail: fatherBioTest?.value },
    });
  };
  /**This function is use for when user clicked on create sub bio test button and user redirect to create sub bio test page*/
  const handleViewAllExtract = () => {
    setExtractModalOpen(true);
    setSupportedExtract(supportedExtract);
  };

  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => handleDeleteChild()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={deleteModal}
      />
      <PopUp
        open={extractModalOpen}
        closeModal={() => setExtractModalOpen(false)}
        supportedExtract={supportedExtract}
        AllSuportedExtract={AllSuportedExtract}
        handelChangeExtract={(serachItem) => setSupportedExtract(serachItem)}
      />
      <div className="add-users-details-wrapper view-bio-wrapper  view-bio-details-graph p-0">
        <div className="users-title">
          <h2>Information Section</h2>
          <div className="create-sub-bio-header d-flex">
            <Link className="md-copy-icon icon-gred-box">
              {" "}
              <MdContentCopy
                onClick={() => handleDuplicateChild(childBioTestData?.id)}
              />{" "}
            </Link>
            <Link
              className="icon-gred-box"
              onClick={() => {
                setDeleteModal(true);
                setModalOpen(true);
                setDeleteUserData(childBioTestData?.id);
              }}
            >
              {" "}
              <img src={Trash} alt="trash" className="trash-icon" />
            </Link>
          </div>
        </div>
        <div className="view-bio-details-wrapper">
          <div className="view-bio-details">
            <div className="view-bio-details-item sbt-row">
              <div className="view-sbt view-bio-details-item">
                <span>Sub Bio Test</span>
                <p>{childBioTestData?.name}</p>
              </div>
              <div className="view-gbt view-bio-details-item">
                <span>Bio-Test</span>
                <p>{fatherBioTest?.label}</p>
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Experimental Condition</span>
              <div className="clusters-option">
                <p>
                  {clusterList.map((item) => (
                    <span>
                      <AiOutlineMinus />
                      {item?.label}
                    </span>
                  ))}
                </p>
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Supported Extract</span>
              <div className="clusters-option supported-ext-tags">
                <p className="tag-span-list">
                  {AllSuportedExtract.slice(0, 10)?.map((item) => (
                    <span className="se-span">{item}</span>
                  ))}
                  {AllSuportedExtract?.length > 10 && "..."}
                  {extractError && <span>Data is not available.</span>}
                </p>
                {AllSuportedExtract?.length > 10 && (
                  <button
                    onClick={() => handleViewAllExtract()}
                    className="btn-outline view-all-ext"
                  >
                    View All extract
                  </button>
                )}
              </div>
            </div>
            <div className="view-bio-details-item">
              <span>Scoring Graph</span>
              <div className="clusters-option">
                <ScoringGraph
                  className="graph-css"
                  id={props?.match?.params?.slug}
                />
              </div>
            </div>
            <div className="view-bio-details-btn">
              <button
                onClick={() => handleSubBioTestScore(childBioTestData?.id)}
                className="reset primary-btn view-sbt-btn"
              >
                <GrScorecard /> Score
              </button>
              <button
                onClick={() => handleSubBioTestPredict(childBioTestData?.id)}
                className="reset primary-btn view-sbt-btn"
              >
                <AiOutlineFileSearch /> Predict
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default ViewBio;
