import React from "react";
import { FiSearch } from "react-icons/fi";
import { IoMdClose } from "react-icons/io";
import Modal from "react-modal";
import "../../components/modal.style.scss";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 30,
    borderRadius: 10,
    border: "none",
    width: 600,
    overflow: "inherit",
    boxShadow: "0 6px 16px rgba(#000, 0.16)",
    height: 350,
  },
};
const PopUp = (props) => {
  /**
   * This function manage supported extract popup close button
   */
  const handelClose = async () => {
    props.closeModal();
  };
  /**
   * This function manage search extract when user search extract
   */
  const handleSearch = async (searchText) => {
    const searchItem = props?.AllSuportedExtract?.filter(
      (item) => item?.slice(0, searchText?.length) == searchText
    );
    await props?.handelChangeExtract(searchItem);
  };
  return (
    <div>
      <Modal
        isOpen={props.open}
        style={customStyles}
        overlayClassName="react-modal-body sub-bio-popup-details"
      >
        <div className="popup-header">
          <p>Supported Extract</p>
          <IoMdClose onClick={(item) => handelClose(item)} />
        </div>
        <div className="search">
          <button className="search-icon">
            {" "}
            <FiSearch />{" "}
          </button>
          <input
            onChange={(e) => handleSearch(e.target.value)}
            type="search"
            placeholder="Search"
            className="search-input"
          />
        </div>
        <div className="popup-extract">
          {props?.supportedExtract.length <= 0 ? (
            <p className="tag-span-list no-tag-list">
              <span className="not-center">No suppoted extract found</span>
            </p>
          ) : (
            <p className="tag-span-list">
              {props?.supportedExtract?.map((item) => (
                <span className="se-span">{item}</span>
              ))}
            </p>
          )}
          <span className="total-count">
            Total {props?.supportedExtract?.length} Extract
          </span>
        </div>
      </Modal>
    </div>
  );
};

export default PopUp;
