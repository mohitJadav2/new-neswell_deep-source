import React from "react";
import Logo from '../../assets/images/Login/login-logo.svg';
import ConnectionImage from '../../assets/images/connection-error.svg';
import { Link } from "react-router-dom";
import './404.style.scss'

function connectionError() {
    return (
        <section className="page-not-found-bg">
            <div className="page-not-found-wrapper">
                <div className="page-not-found-logo">
                    <img src={Logo} alt="" />
                </div>
                <div className="page-not-found-zip">
                    <img src={ConnectionImage} alt="" />
                </div>
                <div className="page-not-found-bottom-text connection-bottom-text">
                    <h2>No Internet Connection</h2>
                    <p>Please check your internet connection and try again</p>
                    <div className="back-home-btn">
                        <Link>Refresh</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default connectionError;