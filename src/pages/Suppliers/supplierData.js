import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import "./supplier.style.scss";
import { FiSearch } from "react-icons/fi";
import {
  AiOutlineSetting,
  AiFillCaretUp,
  AiFillCaretDown,
} from "react-icons/ai";
import { BsPlusCircle } from "react-icons/bs";
import Layout from "../../Layout";
import Pagination from "react-responsive-pagination";
import SuppliersApi from "../../services/SuppliersApi";
import Modal from "../../components/ModalComponent";
import Loader from "../../components/loader";
import { useHistory } from "react-router-dom";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";
import PhoneInput from "react-phone-input-2";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { saveSupplierSortingData } from "../../Actions/HomeAction";

function SuppliersData(props) {
  const [openDropDown, setOpenDropDown] = useState(undefined);
  const [SupplierData, setSupplierData] = useState(undefined);
  const [totalPages, setTotalPages] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteUserData, setDeleteUserData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [perPage, setPerPage] = useState(
    props?.supplierSortingData?.perPage
      ? props?.supplierSortingData?.perPage
      : 10
  );
  const [orderType, setOrderType] = useState("desc");
  const [orderField, setOrderField] = useState("supplier_Firm_name");
  const [searchText, setSearchText] = useState("");
  const [searchError, setSearchError] = useState(undefined);
  const [toggleDropDown, setToggleDropDown] = useState(false);
  let history = useHistory();
  const perPageValue = [10, 25, 50, 100];
  /**
   * This function for handling the action  pop for every user
   */
  const handleSettingClick = async (item) => {
    openDropDown == item?.id
      ? setOpenDropDown(undefined)
      : setOpenDropDown(item?.id);
  };
  /**
   * When user clicked outside of the alert popup box this function executed
   * The function manages mouse event
   */
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setOpenDropDown(undefined);
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  /**
   * This ref function manage the when user clicked outside the fucntion
   */
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  useEffect(() => {
    props?.supplierSortingData &&
      setOrderType(props?.supplierSortingData?.orderType);
    props?.supplierSortingData &&
      setOrderField(props?.supplierSortingData?.orderField);
    props?.supplierSortingData &&
      setCurrentPage(props?.supplierSortingData?.page);
    props?.supplierSortingData &&
      setPerPage(
        props?.supplierSortingData?.perPage
          ? props?.supplierSortingData?.perPage
          : 10
      );
    setLoader(true);
    SuppliersApi.SuppliersData(
      props?.supplierSortingData
        ? props?.supplierSortingData?.orderType
        : orderType,
      props?.supplierSortingData
        ? props?.supplierSortingData?.orderField
        : orderField,
      localStorage.getItem("neswell_access_token"),
      props?.supplierSortingData ? props?.supplierSortingData?.page : page,
      props?.supplierSortingData
        ? props?.supplierSortingData?.perPage
          ? props?.supplierSortingData?.perPage
          : 10
        : perPage
    ).then((res) => {
      setSupplierData(res?.data?.results);
      let totalPages = res?.data?.count / perPage;
      setTotalPages(Math.ceil(totalPages));
      setTotalCount(res?.data?.count);
      setLoader(false);
    });
  }, []);

  /**
   * This function manages pagination with passing page number in API
   */

  function handlePageChange(page) {
    let sortingData = {
      orderType: orderType,
      orderField: orderField,
      page: page,
      perPage: perPage,
    };
    props.saveSupplierSortingData(sortingData);
    setLoader(true);
    setSearchText(searchText);
    setCurrentPage(page);
    if (searchText) {
      SuppliersApi.SuppliersSearch(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        searchText,
        page,
        perPage
      ).then((json) => {
        if (!json.data.error) {
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersData(
        orderType,
        orderField,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let sortingData = {
            orderType: orderType,
            orderField: orderField,
            page: page,
            perPage: perPage,
          };
          props.saveSupplierSortingData(sortingData);
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  }

  /**
   * This function manages record display drop down
   * When user change value of drop down at that time  this function call API with change value
   */
  const handlePerPageChanges = async (perPageNuumber) => {
    let sortingData = {
      orderType: orderType,
      orderField: orderField,
      page: 1,
      perPage: perPageNuumber,
    };
    props.saveSupplierSortingData(sortingData);
    setToggleDropDown(false);
    setPerPage(perPageNuumber);
    setCurrentPage(1);
    searchText
      ? await SuppliersApi.SuppliersSearch(
          orderType,
          orderField,
          localStorage.getItem("neswell_access_token"),
          searchText,
          1,
          perPageNuumber
        ).then(async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setSupplierData(json?.data?.results);
          }
        })
      : await SuppliersApi.SuppliersData(
          orderType,
          orderField,
          localStorage.getItem("neswell_access_token"),
          1,
          perPageNuumber
        ).then(async (json) => {
          if (!json?.data?.error) {
            let totalPages = json?.data?.count / perPageNuumber;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(json?.data?.count);
            setSupplierData(json?.data?.results);
          }
        });
  };

  /**
   * This function manage search feature
   * This function passes user's enter value to API and get positive and error response
   */
  const handleSearch = (searchText) => {
    setSearchText(searchText);
    setCurrentPage(1);
    SuppliersApi.SuppliersSearch(
      orderType,
      orderField,
      localStorage.getItem("neswell_access_token"),
      searchText,
      1,
      perPage
    ).then((json) => {
      if (!json?.data?.error) {
        let totalPages = json?.data?.count / perPage;
        setTotalPages(Math.ceil(totalPages));
        setSupplierData(json?.data?.results);
        setSearchText(searchText);
        setSearchError(json?.data?.message);
      }
    });
  };

  /**
   * When user clicked delete button ,this function manage user delete feature
   * The function called delete Api with user's id
   */
  const handeleDeleteClicked = () => {
    setModalOpen(false);
    setLoader(true);
    if (searchText) {
      SuppliersApi.SuppliersDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData?.id
      ).then((json) => {
        if (!json.data.error) {
          SuppliersApi.SuppliersSearch(
            orderType,
            orderField,
            localStorage.getItem("neswell_access_token"),
            searchText,
            currentPage,
            perPage
          ).then((json) => {
            if (!json.data.error) {
              let totalPages = json?.data?.count / perPage;
              setTotalPages(Math.ceil(totalPages));
              setSupplierData(json?.data?.results);
              setTotalCount(json?.data?.count);
              setLoader(false);
            } else {
              setLoader(false);
            }
          });
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersDelete(
        localStorage.getItem("neswell_access_token"),
        deleteUserData?.id
      ).then((json) => {
        if (!json?.data?.error) {
          SuppliersApi.SuppliersData(
            orderType,
            orderField,
            localStorage.getItem("neswell_access_token"),
            currentPage,
            perPage
          ).then((res) => {
            if (res?.data?.results?.length<=0) {
              setLoader(true);
              let sortingData = {
                orderType: orderType,
                orderField: orderField,
                page: currentPage - 1,
                perPage: perPage,
              };
              props.saveSupplierSortingData(sortingData);
              SuppliersApi.SuppliersData(
                orderType,
                orderField,
                localStorage.getItem("neswell_access_token"),
                currentPage - 1,
                perPage
              ).then((res) => {
                setSupplierData(res?.data?.results);
                let totalPages = res?.data?.count / perPage;
                setTotalPages(Math.ceil(totalPages));
                setTotalCount(res?.data?.count);
              });
            }
            setSupplierData(res?.data?.results);
            let totalPages = res?.data?.count / perPage;
            setTotalPages(Math.ceil(totalPages));
            setTotalCount(res?.data?.count);
            setLoader(false);
          });
        } else {
          setLoader(false);
        }
      });
    }
  };
  /**
   * This function manage user record column's ascending and descending order
   */
  const handleAscending = (fieldName) => {
    setOrderType("asc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      SuppliersApi.SuppliersSearch(
        "asc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        searchText,
        1,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let sortingData = {
            orderType: "asc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.saveSupplierSortingData(sortingData);
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersData(
        "asc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let sortingData = {
            orderType: "asc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.saveSupplierSortingData(sortingData);
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };

  const handledescending = (fieldName) => {
    setOrderType("desc");
    setOrderField(fieldName);
    setLoader(true);
    setCurrentPage(1);
    if (searchText) {
      SuppliersApi.SuppliersSearch(
        "desc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        searchText,
        1,
        perPage
      ).then((json) => {
        if (!json.data.error) {
          let sortingData = {
            orderType: "desc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.saveSupplierSortingData(sortingData);
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      SuppliersApi.SuppliersData(
        "desc",
        fieldName,
        localStorage.getItem("neswell_access_token"),
        page,
        perPage
      ).then((json) => {
        if (!json?.data?.error) {
          let sortingData = {
            orderType: "desc",
            orderField: fieldName,
            page: 1,
            perPage: perPage,
          };
          props.saveSupplierSortingData(sortingData);
          let totalPages = json?.data?.count / perPage;
          setTotalPages(Math.ceil(totalPages));
          setTotalCount(json?.data?.count);
          setSupplierData(json?.data?.results);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    }
  };

  /**
   * This function use for to redirect view Supplier's details
   */
  const handleSupplierClicked = async (item) => {
    await SuppliersApi.SuppliersView;
    history.push(`/view-suppliers/${item?.id}`);
  };

  /**
   * This function use for to redirect edit Supplier's records details
   */
  const handleEditClicked = async (item) => {
    history.push(`create-suppliers/${item?.id}`);
  };
  return (
    <Layout>
      <Modal
        open={modalOpen}
        closeModal={() => setModalOpen(false)}
        DeleteUsersRecord={() => {
          deleteModal ? handeleDeleteClicked() : setModalOpen(false);
        }}
        subTitle={
          deleteModal
            ? "Do you want to delete this record?"
            : "We have emailed user password reset link!"
        }
        delete={deleteModal}
      />
      {loader && <Loader />}
      <div className="users-details-wrapper">
        <div className="users-details-inner-wrapper">
          <div className="user-search">
            <div className="search">
              <button className="search-icon">
                {" "}
                <FiSearch />{" "}
              </button>
              <input
                onChange={(e) => handleSearch(e.target.value)}
                type="search"
                placeholder="Search"
                className="search-input"
              />
            </div>
            <div className="add-user-btn">
              <Link to="/create-suppliers" className="primary-btn">
                {" "}
                <BsPlusCircle /> Create Suppliers
              </Link>
            </div>
          </div>
          <div className="user-details-table details-table">
            <table>
              <thead>
                <tr>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "asc"
                          ? handledescending("supplier_Firm_name")
                          : handleAscending("supplier_Firm_name");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                    >
                      Company Name
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField == "supplier_Firm_name" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField == "supplier_Firm_name" &&
                                orderType == "asc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "asc"
                          ? handledescending("supplier_Contact_name")
                          : handleAscending("supplier_Contact_name");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                    >
                      Contact Name
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField == "supplier_Contact_name" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField == "supplier_Contact_name" &&
                                orderType == "asc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "asc"
                          ? handledescending("supplier_Contact_Phone")
                          : handleAscending("supplier_Contact_Phone");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                    >
                      Phone
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField == "supplier_Contact_Phone" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField == "supplier_Contact_Phone" &&
                                orderType == "asc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div
                      onClick={() => {
                        orderType == "asc"
                          ? handledescending("supplier_Contact_Position_Role")
                          : handleAscending("supplier_Contact_Position_Role");
                      }}
                      style={{
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                    >
                      Position
                      <div style={{ marginLeft: 10 }}>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretUp
                            style={{
                              opacity:
                                orderField ==
                                  "supplier_Contact_Position_Role" &&
                                orderType == "desc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                        <div style={{ cursor: "pointer", marginBottom: -8 }}>
                          <AiFillCaretDown
                            style={{
                              opacity:
                                orderField ==
                                  "supplier_Contact_Position_Role" &&
                                orderType == "asc"
                                  ? 0.5
                                  : 1,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th style={{ cursor: "context-menu" }}>Action</th>
                </tr>
              </thead>
              <tbody style={{ cursor: "pointer" }}>
                {SupplierData?.map((item, idx) => {
                  return (
                    <tr>
                      <td
                        onClick={() => handleSupplierClicked(item)}
                      >{`${item?.supplier_Firm_name}`}</td>
                      <td onClick={() => handleSupplierClicked(item)}>
                        {`${item?.supplier_Contact_name}`
                          ? item?.supplier_Contact_name
                          : "-"}
                      </td>
                      <td
                        className="phone-react-view supplier-data-phone-react-view"
                        onClick={() => handleSupplierClicked(item)}
                      >
                        {!item?.supplier_Contact_Phone ? (
                          "-"
                        ) : (
                          <PhoneInput
                            className="react-phone"
                            value={item?.supplier_Contact_Phone}
                            disabled={true}
                          />
                        )}
                      </td>
                      <td onClick={() => handleSupplierClicked(item)}>
                        {`${item?.supplier_Contact_Position_Role}`
                          ? item?.supplier_Contact_Position_Role
                          : "-"}
                      </td>
                      <td>
                        <span
                          onClick={() => handleSettingClick(item)}
                          className="blue-color-eyes"
                        >
                          <AiOutlineSetting style={{ color: "#404040" }} />
                          {openDropDown == item?.id && (
                            <div
                              ref={wrapperRef}
                              className="user-setting-popup"
                            >
                              <p onClick={() => handleEditClicked(item)}>
                                Edit{" "}
                              </p>
                              <p
                                onClick={() => {
                                  setDeleteModal(true);
                                  setModalOpen(true);
                                  setDeleteUserData(item);
                                }}
                              >
                                Remove
                              </p>
                            </div>
                          )}
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <p style={{ textAlign: "center" }}>{searchError}</p>
          </div>
          <div className="pagination-wrapper">
            <div className="entries-content">
              <span>{`Showing 1 to ${SupplierData?.length} of ${totalCount} entries`}</span>
            </div>
            <div className="pagination">
              <Pagination
                total={totalPages}
                current={currentPage}
                onPageChange={(page) => handlePageChange(page)}
              />
            </div>
            <div className="per-pagination-wrapper">
              <div
                onClick={() => setToggleDropDown(!toggleDropDown)}
                className={
                  toggleDropDown ? "selected-page open" : "selected-page "
                }
              >
                <p>{perPage}</p>
                <div className="up-down-arrow">
                  <span className="down-arrow">
                    <BsChevronDown />
                  </span>
                  <span className="up-arrow">
                    <BsChevronUp />
                  </span>
                </div>
              </div>
              {toggleDropDown && (
                <ul className="per-page-dropdown">
                  {perPageValue?.map((item) => (
                    <li onClick={() => handlePerPageChanges(item)}>{item}</li>
                  ))}
                </ul>
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
/**
 * This function manage SaveExperimentData from redux store
 */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ saveSupplierSortingData }, dispatch),
  };
}

const mapStateToProps = (state) => ({
  supplierSortingData: state.homeReducer.supplierSortingData,
});
export default connect(mapStateToProps, mapDispatchToProps)(SuppliersData);
