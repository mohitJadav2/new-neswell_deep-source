import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./supplier.style.scss";
import Layout from "../../Layout";
import validator from "validator";
import SuppliersApi from "../../services/SuppliersApi";
import MyGoogleMap from "../../components/MyGoogleMap";
import DashboardApi from "../../services/DashboardApi";
import Loader from "../../components/loader";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Modal from "../../components/ModalComponent";

function CreateSupplier(props) {
  let history = useHistory();
  const [loader, setLoader] = useState(false);
  const [firmName, setFirmName] = useState("");
  const [firmNamError, setFirmNamError] = useState(undefined);
  const [contactName, setContactName] = useState("");
  const [phone, setPhone] = useState("");
  const [phoneError, setPhoneError] = useState(undefined);
  const [contactPosition, setContactPosition] = useState("");
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState(undefined);
  const [additionalEmailError, setAdditionalEmailError] = useState(undefined);
  const [website, setWebSite] = useState("");
  const [supplierInventory, setSupplierInventory] = useState("");
  const [shippingTime, setShippingTime] = useState("");
  const [notes, setNotes] = useState("");
  const [apiError, setApiError] = useState(undefined);
  const [errorModelOpen, setErrorModelOpen] = useState(false);
  const [error, setError] = useState(false);
  const [supplierInventoryLatitude, setSupplierInventoryLatitude] = useState(
    props?.match?.params?.slug ? undefined : "31.771959"
  );
  const [supplierInventoryLongitude, setSupplierInventoryLongitude] = useState(
    props?.match?.params?.slug ? undefined : "35.217018"
  );
  const [CoAsAvailabilityRadioBtn, setCoAsAvailabilityRadioBtn] =
    useState(undefined);
  const [terpenesProfileRadioBtn, setTerpenesProfileRadioBtn] =
    useState(undefined);
  const [QADocumentRadioBtn, setQADocumentRadioBtn] = useState(undefined);
  const [preferredSupplierRadioBtn, setPreferredSupplierRadioBtn] =
    useState(undefined);
  const [address, setAddress] = useState("");
  const id = props?.match?.params?.slug;
  const [extractData, setExtractData] = useState(undefined);
  const [S, setSupplierAddress] = useState(undefined);
  const [storageCondtion, setStorageCondition] = useState("");
  const [additionalEmail, setAdditionalEmail] = useState("");
  const [additionalPhone, setAdditionalPhone] = useState("");
  const [addressName, setAddressName] = useState(undefined);

  /**
   * This function collect suppler details in form data in json formant
   * And send to API and get positive and error response from API
   */
  const handleSubmit = async () => {
    setLoader(true);
    setFirmNamError(true);
    if (firmName) {
      if (props?.match?.params?.slug) {
        setLoader(true);
        let formData = {};
        formData["supplier_Firm_name"] = firmName;
        formData["supplier_Contact_name"] = contactName;
        formData["supplier_Contact_Phone"] = phone;
        formData["supplier_Contact_Phone2"] = additionalPhone;
        formData["supplier_Contact_Position_Role"] = contactPosition;
        formData["supplier_Contact_email"] = email;
        formData["supplier_Contact_email2"] = additionalEmail;
        formData["supplier_Website"] = website;
        formData["supplier_CoAs_availability"] =
          CoAsAvailabilityRadioBtn === "true" ? true : false;
        formData["supplier_Terpenes_Profile"] =
          terpenesProfileRadioBtn === "true" ? true : false;
        formData["supplier_QA_Document"] =
          QADocumentRadioBtn === "true" ? true : false;
        formData["supplier_storage_conditions"] = storageCondtion;
        formData["supplier_Shipping_time"] = shippingTime;
        formData["supplier_Notes"] = notes;
        formData["supplier_Is_Preferred_Supplier"] =
          preferredSupplierRadioBtn === "true" ? true : false;
        formData["supplier_Inventory_kg"] = supplierInventory;
        formData["supplier_Inventory_latitude"] = supplierInventoryLatitude;
        formData["supplier_Inventory_longitude"] = supplierInventoryLongitude;
        formData["supplier_Physical_Address"] = addressName;
        await SuppliersApi.SuppliersEdit(
          localStorage.getItem("neswell_access_token"),
          props?.match?.params?.slug,
          formData
        ).then((json) => {
          if (json?.header?.status === 200 || json?.header?.status === 201) {
            history.push("/suppliers-data");
            setLoader(false);
          } else {
            setErrorModelOpen(true);
            setApiError(json?.data.error);
            setLoader(false);
          }
        });
      } else {
        let formData = {};
        formData["supplier_Firm_name"] = firmName;
        formData["supplier_Contact_name"] = contactName;
        formData["supplier_Contact_Phone"] = phone;
        formData["supplier_Contact_Phone2"] = additionalPhone;
        formData["supplier_Contact_Position_Role"] = contactPosition;
        formData["supplier_Contact_email"] = email;
        formData["supplier_Contact_email2"] = additionalEmail;
        formData["supplier_Website"] = website;
        formData["supplier_CoAs_availability"] = CoAsAvailabilityRadioBtn;
        formData["supplier_Terpenes_Profile"] = terpenesProfileRadioBtn;
        formData["supplier_QA_Document"] = QADocumentRadioBtn;
        formData["supplier_storage_conditions"] = storageCondtion;
        formData["supplier_Shipping_time"] = shippingTime;
        formData["supplier_Notes"] = notes;
        formData["supplier_Is_Preferred_Supplier"] = preferredSupplierRadioBtn;
        formData["supplier_Inventory_kg"] = supplierInventory;
        formData["supplier_Inventory_latitude"] = supplierInventoryLatitude;
        formData["supplier_Inventory_longitude"] = supplierInventoryLongitude;
        formData["supplier_Physical_Address"] = addressName;
        await SuppliersApi.AddSuppliers(
          formData,
          localStorage.getItem("neswell_access_token")
        ).then((json) => {
          if (json?.header?.status === 200 || json?.header?.status === 201) {
            history.push("/suppliers-data");
            setLoader(false);
          } else {
            setErrorModelOpen(true);
            setApiError(json?.data?.error);
            setLoader(false);
          }
        });
      }
    } else {
      setError(true);
      setLoader(false);
    }
  };
  /**
   * This functions manage validation in supplier form
   */
  const handleChangeFirmName = async (item) => {
    setFirmName(item);
    validator.isEmpty(item)
      ? setFirmNamError(true)
      : !validator.matches(item, "^[a-zA-Z0-9.-@+_]*$")
      ? setFirmNamError(true)
      : setFirmNamError(false);
    if (item?.length > 60) {
      setFirmNamError(true);
    }
  };
  const handleChangeContactName = async (item) => {
    setContactName(item);
  };
  const handleChangePhone = async (item) => {
    setPhone(item);
    item?.length.toString() === "10"
      ? !validator.matches(item, "^[0-9]*$")
        ? await setPhoneError(true)
        : await setPhoneError(false)
      : await setPhoneError(true);
  };
  const handleChangeAdditionalPhone = async (item) => {
    setAdditionalPhone(item);
    item?.length.toString() === "10"
      ? !validator.matches(item, "^[0-9]*$")
        ? await setPhoneError(true)
        : await setPhoneError(false)
      : await setPhoneError(true);
  };
  const handleChangeContactPosition = async (item) => {
    setContactPosition(item);
  };
  const handleChangeEmail = async (item) => {
    setEmail(item);
    item !== ""
      ? validator.isEmail(item)
        ? await setEmailError(false)
        : await setEmailError(true)
      : await setEmailError(true);
  };
  const handleChangeAdditionalEmail = async (item) => {
    setAdditionalEmail(item);
    item !== ""
      ? validator.isEmail(item)
        ? await setAdditionalEmailError(false)
        : await setAdditionalEmailError(true)
      : await setAdditionalEmailError(true);
  };

  const handleWebSite = async (item) => {
    setWebSite(item);
  };
  const handleShippingTime = async (item) => {
    setShippingTime(item);
  };
  const handleNotes = async (item) => {
    setNotes(item);
  };
  const handleStorageCondition = async (item) => {
    setStorageCondition(item);
  };
  /**
   * This _onClick function manage Google map location
   */
  const _onClick = (value) => {
    setSupplierInventoryLatitude(value.lat);
    setSupplierInventoryLongitude(value.lng);
  };
  /**This fucntion is use for when user go view page to edit page this api set already existence records in edit page. */
  useEffect(() => {
    setLoader(true);
    if (props?.match?.params?.slug) {
      SuppliersApi.SuppliersView(
        localStorage.getItem("neswell_access_token"),
        props?.match?.params?.slug
      ).then((json) => {
        if (!json?.data?.error) {
          setFirmName(json?.data?.supplier_Firm_name);
          setContactName(json?.data?.supplier_Contact_name);
          setPhone(json?.data?.supplier_Contact_Phone);
          setAdditionalPhone(json?.data?.supplier_Contact_Phone2);
          setContactPosition(json?.data?.supplier_Contact_Position_Role);
          setEmail(json?.data?.supplier_Contact_email);
          setAdditionalEmail(json?.data?.supplier_Contact_email2);
          setWebSite(json?.data?.supplier_Website);
          setCoAsAvailabilityRadioBtn(
            json?.data?.supplier_CoAs_availability?.toString()
          );
          setTerpenesProfileRadioBtn(
            json?.data?.supplier_Terpenes_Profile?.toString()
          );
          setQADocumentRadioBtn(json?.data?.supplier_QA_Document?.toString());
          setShippingTime(json?.data?.supplier_Shipping_time);
          setNotes(json?.data?.supplier_Notes);
          setStorageCondition(json?.data?.supplier_storage_conditions);
          setPreferredSupplierRadioBtn(
            json?.data?.supplier_Is_Preferred_Supplier?.toString()
          );
          setSupplierInventory(json?.data?.supplier_Inventory_kg);
          setSupplierInventoryLatitude(json?.data?.supplier_Inventory_latitude);
          setSupplierInventoryLongitude(
            json?.data?.supplier_Inventory_longitude
          );
          setAddressName(json?.data?.supplier_Physical_Address);
          setLoader(false);
        } else {
          setLoader(false);
        }
      });
    } else {
      setLoader(false);
    }
  }, []);

  /**This function is use for fetch supplier's address from api */
  const fetchAddress = (lat, lng) => {
    return DashboardApi.fetchLocationAddress(
      lat,
      lng,
      localStorage.getItem("neswell_access_token")
    )
      .then((json) => {
        if (!json.data.error) {
          return json.data.message;
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  };
  /**This function is use for fatch address using latitude and longitude and set in map in edit page */
  useEffect(() => {
    setLoader(true);
    SuppliersApi.SuppliersView(localStorage.getItem("neswell_access_token"), id)
      .then(async (json) => {
        if (!json.data.error) {
          setExtractData(json?.data);

          let S = await fetchAddress(
            json?.data?.supplier_Inventory_latitude,
            json?.data?.supplier_Inventory_longitude
          );
          setSupplierAddress(S);
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  }, []);

  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={errorModelOpen}
        closeModal={() => setErrorModelOpen(false)}
        subTitle={apiError}
        fromError={true}
        error={true}
      />
      <div className="add-users-details-wrapper view-supplier-details">
        <div className="users-title">
          <h2>Create Supplier</h2>
        </div>
        <div className="add-user-form-wrapper ">
          <div className="row add-user-form">
            <div className="col-md-6">
              <div className="form-group">
                <span>Company Name</span>
                <input
                  value={firmName}
                  onChange={async (e) => {
                    handleChangeFirmName(e.target.value);
                  }}
                  className="add-user-input"
                  type="text"
                  placeholder="Enter Company Name"
                  required="required"
                />
                {error &&
                  firmNamError &&
                  (!firmName ? (
                    <p style={{ color: "red" }}>
                      Please enter a valid company name
                    </p>
                  ) : (
                    <p style={{ color: "red" }}>Company name is required. </p>
                  ))}
              </div>
              <div className="form-group">
                <span>Phone</span>
                <PhoneInput
                  className="react-phone"
                  country={"us"}
                  value={phone}
                  onChange={(e) => handleChangePhone(e)}
                />
              </div>
              <div className="form-group">
                <span>Additional Phone</span>
                <PhoneInput
                  className="react-phone"
                  country={"us"}
                  value={additionalPhone}
                  onChange={(e) => handleChangeAdditionalPhone(e)}
                />
              </div>
              <div className="form-group">
                <span>Contact Email</span>
                <input
                  value={email}
                  onChange={async (e) => {
                    handleChangeEmail(e.target.value);
                  }}
                  className="add-user-input"
                  type="email"
                  placeholder="Enter Email Address"
                  required="false"
                />
                {emailError && (
                  <p style={{ color: "red" }}>Please enter a valid email</p>
                )}
              </div>
              <div className="form-group">
                <span>Additional Email </span>
                <input
                  value={additionalEmail}
                  onChange={async (e) => {
                    handleChangeAdditionalEmail(e.target.value);
                  }}
                  className="add-user-input"
                  type="email"
                  placeholder="Enter Email Address"
                  required="false"
                />
              </div>
              {additionalEmailError && (
                <p style={{ color: "red" }}>Please enter a valid email</p>
              )}
              <div className="form-group">
                <span>Website</span>
                <input
                  value={website}
                  onChange={async (e) => {
                    handleWebSite(e.target.value);
                  }}
                  className="add-user-input"
                  Phone
                  type="url"
                  placeholder="Enter Website"
                  required="false"
                />
              </div>
              <div className="form-group">
                <div className="availability-radio-group">
                  <p>Terpenes Profile</p>
                  <div className="availability-radio">
                    <div className="radio">
                      <input
                        type="radio"
                        value={"true"}
                        checked={terpenesProfileRadioBtn === "true"}
                        onChange={(e) =>
                          setTerpenesProfileRadioBtn(e.target.value)
                        }
                      />
                      <label
                        htmlFor="terpenesProfile"
                        id="terpenesProfile"
                        className="radio-label"
                      >
                        Yes
                      </label>
                    </div>
                    <div className="radio">
                      <input
                        type="radio"
                        value={"false"}
                        checked={terpenesProfileRadioBtn === "false"}
                        onChange={(e) =>
                          setTerpenesProfileRadioBtn(e.target.value)
                        }
                      />
                      <label
                        htmlFor="terpenesProfile"
                        id="terpenesProfile"
                        className="radio-label"
                      >
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="availability-radio-group">
                  <p>Is Preferred Supplier</p>
                  <div className="availability-radio">
                    <div className="radio">
                      <input
                        type="radio"
                        value={"true"}
                        checked={preferredSupplierRadioBtn === "true"}
                        onChange={(e) =>
                          setPreferredSupplierRadioBtn(e.target.value)
                        }
                      />
                      <label
                        htmlFor="preferredSupplier"
                        className="radio-label"
                      >
                        Yes
                      </label>
                    </div>
                    <div className="radio">
                      <input
                        type="radio"
                        checked={preferredSupplierRadioBtn === "false"}
                        value={"false"}
                        onChange={(e) =>
                          setPreferredSupplierRadioBtn(e.target.value)
                        }
                      />
                      <label
                        htmlFor="preferredSupplier"
                        className="radio-label"
                      >
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="availability-radio-group">
                  <p>CoA's Availability</p>
                  <div className="availability-radio">
                    <div className="radio">
                      <input
                        type="radio"
                        value={"true"}
                        checked={CoAsAvailabilityRadioBtn === "true"}
                        onChange={(e) =>
                          setCoAsAvailabilityRadioBtn(e.target.value)
                        }
                      />
                      <label
                        htmlFor="CoAsAvailability"
                        id="CoAsAvailability"
                        className="radio-label"
                      >
                        Yes
                      </label>
                    </div>
                    <div className="radio">
                      <input
                        type="radio"
                        value={"false"}
                        checked={CoAsAvailabilityRadioBtn === "false"}
                        onChange={(e) =>
                          setCoAsAvailabilityRadioBtn(e.target.value)
                        }
                      />
                      <label
                        htmlFor="CoAsAvailability"
                        id="CoAsAvailability"
                        className="radio-label"
                      >
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="availability-radio-group">
                  <p>QA Document</p>
                  <div className="availability-radio">
                    <div className="radio">
                      <input
                        type="radio"
                        value={"true"}
                        checked={QADocumentRadioBtn === "true"}
                        onChange={(e) => setQADocumentRadioBtn(e.target.value)}
                      />
                      <label htmlFor="QADocument" className="radio-label">
                        Yes
                      </label>
                    </div>
                    <div className="radio">
                      <input
                        type="radio"
                        value={"false"}
                        checked={QADocumentRadioBtn === "false"}
                        onChange={(e) => setQADocumentRadioBtn(e.target.value)}
                      />
                      <label htmlFor="QADocument" className="radio-label">
                        No
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <span>Contact Name</span>
                <input
                  value={contactName}
                  onChange={async (e) => {
                    handleChangeContactName(e.target.value);
                  }}
                  className="add-user-input"
                  type="text"
                  placeholder="Enter Contact Name"
                  required="false"
                />
              </div>
              <div className="form-group">
                <span>Contact Position</span>
                <input
                  value={contactPosition}
                  onChange={async (e) => {
                    handleChangeContactPosition(e.target.value);
                  }}
                  className="add-user-input"
                  type="text"
                  placeholder="Enter Contact Position"
                  required="false"
                />
              </div>
              <div className="form-group">
                <span>Shipping Time</span>
                <input
                  value={shippingTime}
                  onChange={async (e) => {
                    handleShippingTime(e.target.value);
                  }}
                  className="add-user-input"
                  type="number"
                  placeholder="Enter Days"
                  required="false"
                />
              </div>
              <div className="form-group">
                <span>Remarks</span>
                <textarea
                  style={{ height: "166px", width: "100%" }}
                  value={notes}
                  onChange={async (e) => {
                    handleNotes(e.target.value);
                  }}
                  className="add-user-input"
                  type="textarea"
                  placeholder="Note about  Supplier"
                  required="false"
                />
              </div>
              <div className="form-group">
                <span>Storage Conditions</span>
                <textarea
                  style={{ height: "166px", width: "100%" }}
                  value={storageCondtion}
                  onChange={async (e) => {
                    handleStorageCondition(e.target.value);
                  }}
                  className="add-user-input"
                  type="textarea"
                  placeholder="Enter storage condition"
                  required="false"
                />
              </div>
              <div className="form-group">
                <span>Supplier Address</span>
                <div
                  style={{ height: "166px", width: "100%" }}
                  className="google-map"
                >
                  {supplierInventoryLatitude && supplierInventoryLongitude && (
                    <MyGoogleMap
                      viewProfile={false}
                      onClickMap={(value) => _onClick(value)}
                      lat={supplierInventoryLatitude}
                      lng={supplierInventoryLongitude}
                      setLat={(value) => setSupplierInventoryLatitude(value)}
                      setLng={(value) => setSupplierInventoryLongitude(value)}
                      value={(value) => setAddress(value)}
                      setAddresName={(value) => setAddressName(value)}
                      addressName={addressName}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="form-submit-btn">
            <button onClick={() => handleSubmit()} className="primary-btn">
              {props?.match?.params?.slug ? "Save" : "Save"}
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default CreateSupplier;
