import React, { useState, useEffect } from "react";
import "./supplier.style.scss";
import Layout from "../../Layout";
import SuppliersApi from "../../services/SuppliersApi";
import MyGoogleMap from "../../components/MyGoogleMap";
import DashboardApi from "../../services/DashboardApi";
import Loader from "../../components/loader";
import { Link } from "react-router-dom";
import { BiEdit } from "react-icons/bi";
import { RiDeleteBinLine } from "react-icons/ri";
import Modal from "../../components/ModalComponent";
import { useHistory } from "react-router-dom";
import { AiOutlineEye } from 'react-icons/ai';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import Trash from '../../assets/icons/trash.svg';

const ViewSuppliers = (props) => {
  const [SupplierData, setSupplierData] = useState(undefined);
  const [loader, setLoader] = useState(false);
  const [extract, setExtract] = useState(undefined);
  const [extractData, setExtractData] = useState(undefined);
  const [supplierAddress, setSupplierAddress] = useState(undefined);
  const [popUpModalOpen, setPopUpModalOpen] = useState(false);
  const id = props?.match?.params?.slug;
  let history = useHistory();

  /**
   * This function manage fetch supplier's address from API
  */
  const fetchAddress = (lat, lng) => {
    return DashboardApi.fetchLocationAddress(
      lat,
      lng,
      localStorage.getItem("neswell_access_token")
    )
      .then((json) => {
        if (!json?.data?.error) {
          return json?.data?.message;
        } else {
          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  };

  /**
   * This function manage Supplier's details form API
   */
  useEffect(() => {
    setLoader(true);
    SuppliersApi.SuppliersView(localStorage.getItem("neswell_access_token"), id)
      .then(async (json) => {
        if (!json.data.error) {
          setExtractData(json?.data);
          let Address = await fetchAddress(
            json?.data?.supplier_Inventory_latitude,
            json?.data?.supplier_Inventory_longitude
          );
          setExtract(json?.data?.extract_data)
          setSupplierAddress(Address);
        } else {

          setLoader(false);
        }
      })
      .catch((e) => {
        setLoader(false);
      });
  }, []);
  /**
     * When user clicked delete button ,this function manage user delete feature 
     * The function called delete Api with user's id 
  */
  const handleDeleteClicked = () => {
    setPopUpModalOpen(false)
    setLoader(true);
    SuppliersApi.SuppliersDelete(
      localStorage.getItem("neswell_access_token"),
      id
    ).then((json) => {
      if (!json.data.error) {
        setLoader(false);
        history.push("/suppliers-data");

      } else {
        setLoader(false);
      }
    });
  };

  /**
    * This function manage specific suppliers records from API and display in UI 
  */
  useEffect(() => {
    setLoader(true);
    SuppliersApi.SuppliersView(
      localStorage.getItem("neswell_access_token"),
      props?.match?.params?.slug
    ).then((json) => {
      if (!json?.data?.error) {
        setSupplierData(json?.data);
        setLoader(false);
      } else {
        setLoader(false);
      }
    });
  }, []);
  const handleExtraxtClicked = async (item) => {
    history.push(`/view-extract/${item?.id}`);
  };

  return (
    <Layout>
      {loader && <Loader />}
      <Modal
        open={popUpModalOpen}
        closeModal={() => setPopUpModalOpen(false)}
        DeleteUsersRecord={() => handleDeleteClicked()}
        title={"Are you sure?"}
        subTitle={"Do you want to delete this record?"}
        delete={true}
      />
      <div className="add-users-details-wrapper view-supplier-details">
        <div className="users-title create-new-extract-header">
          <h2>View Suppliers</h2>
          <div className="create-extract-header">
            <Link className="icon-gred-box" to={`/create-suppliers/${props?.match?.params?.slug}`}>
              {" "}
              <BiEdit />{" "}
            </Link>
            <Link className="icon-gred-box" onClick={() => setPopUpModalOpen(true)}>
              {" "}
              <img src={Trash} alt="trash" className="trash-icon" />
            </Link>
          </div>
        </div>
        <div className="create-supplier-bottom-tag">
          <h2>Information</h2>
        </div>
        <div className="extract-view-profile-wrapper border-botoom-custom">
          <div className="extract-view-profile-inner-wrapper">
            <div className="details-colum">
              <ul>
                <li>
                  <span>Company Name</span>
                  <p>{SupplierData?.supplier_Firm_name}</p>
                </li>
                <li className="phone-react-view">
                  <span>Phone</span>
                  {!SupplierData?.supplier_Contact_Phone ? (<p>--</p>) : <PhoneInput
                    className="react-phone"
                    value={SupplierData?.supplier_Contact_Phone}
                    disabled={true}
                  />}

                </li>
                {SupplierData?.supplier_Contact_Phone2 ?
                  <li className="phone-react-view">
                    <span>Additional Phone</span>
                    <PhoneInput
                      className="react-phone"
                      value={SupplierData?.supplier_Contact_Phone2}
                      disabled={true}
                    />
                  </li>
                  :
                  ''
                }

                <li>
                  <span>Contact Email</span>
                  {!SupplierData?.supplier_Contact_email ? <p>--</p> : <p>{SupplierData?.supplier_Contact_email}</p>}
                </li>
                {SupplierData?.supplier_Contact_email2 ?
                  <li>
                    <span>Additional Email</span>
                    <p>{SupplierData?.supplier_Contact_email2}</p>
                  </li>
                  :
                  ''
                }

                <li>
                  <span>Website</span>
                  {!SupplierData?.supplier_Website ? <p>--</p> :
                    <p>
                      <a target="_blank" href={SupplierData?.supplier_Website?.includes("https://") || SupplierData?.supplier_Website?.includes("http://") ? SupplierData?.supplier_Website : `https://${SupplierData?.supplier_Website}`} >{SupplierData?.supplier_Website}</a>
                    </p>}
                </li>
                <li>
                  <span>Terpenes Profile</span>
                  <p>
                    {SupplierData?.supplier_Terpenes_Profile ? "Yes" : "No"}
                  </p>
                </li>
                <li>
                  <span>Is Preferred Supplier</span>
                  <p>
                    {" "}
                    {SupplierData?.supplier_Is_Preferred_Supplier
                      ? "Yes"
                      : "No"}
                  </p>
                </li>
                <li>
                  <span>CoA's Availability</span>
                  <p>
                    {SupplierData?.supplier_CoAs_availability ? "Yes" : "No"}
                  </p>
                </li>
                <li>
                  <span>QA Document</span>
                  <p> {SupplierData?.supplier_QA_Document ? "Yes" : "No"} </p>
                </li>
              </ul>
            </div>
            <div className="details-colum">
              <ul>
                <li>
                  <span>Contact Name</span>
                  {!SupplierData?.supplier_Contact_name ? <p>--</p> : <p>{SupplierData?.supplier_Contact_name}</p>}
                </li>
                <li>
                  <span>Contact Position</span>
                  {!SupplierData?.supplier_Contact_Position_Role ? <p>--</p> : <p>{SupplierData?.supplier_Contact_Position_Role}</p>}
                </li>

                <li>
                  <span>Shipping Time</span>
                  {!SupplierData?.supplier_Shipping_time ? <p>--</p> : <p>  {SupplierData?.supplier_Shipping_time} {SupplierData?.supplier_Shipping_time ? SupplierData?.supplier_Shipping_time < 2 ? "Day" : 'Days' : ""} </p>}
                </li>

                <li>
                  <span>Remarks</span>
                  {!SupplierData?.supplier_Notes ? (<p>--</p>) : (
                    <p>{SupplierData?.supplier_Notes} </p>
                  )}
                </li>
                <li>
                  <span>Storage Conditions</span>
                  {!SupplierData?.supplier_storage_conditions ? (<p>--</p>) : (
                    <p>{SupplierData?.supplier_storage_conditions} </p>
                  )}
                </li>
                <li>
                  <span>Supplier Address</span>
                  <p>{supplierAddress}</p>
                  <div
                    style={{ height: "320px", width: "100%" }}
                    className="google-map"
                  >
                    {(SupplierData?.supplier_Inventory_latitude && SupplierData?.supplier_Inventory_longitude) && <MyGoogleMap
                      viewProfile={true}
                      lat={SupplierData?.supplier_Inventory_latitude}
                      lng={SupplierData?.supplier_Inventory_longitude}
                    />}
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="create-supplier-bottom-tag pdt">
          <h2>Extract</h2>
        </div>
        <div className="user-details-table details-table">
          <table >
            <thead>
              <tr>
                <th>Sr.</th>
                <th>Name</th>
                <th>Supplier Inventory</th>
                <th>Supplier Inventory Availablity</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody style={{ cursor: "pointer" }}>
              {extract?.map((item) => {
                return (
                  <tr>

                    <td><div onClick={() => handleExtraxtClicked(item)}>{item?.id}</div></td>
                    <td><div onClick={() => handleExtraxtClicked(item)}>{item?.name} </div></td>
                    <td><div onClick={() => handleExtraxtClicked(item)}>{item?.supplier_inventory} </div></td>
                    <td><div onClick={() => handleExtraxtClicked(item)}>{item?.supplier_inventory_availability}</div></td>
                    <td>
                      <span
                        onClick={() => handleExtraxtClicked(item)}
                        className="gray-color-eyes"
                      >
                        <AiOutlineEye />
                      </span>{" "}
                    </td>
                  </tr>

                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default ViewSuppliers;