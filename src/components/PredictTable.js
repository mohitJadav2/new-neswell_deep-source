import React, { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'
import { useSortBy, useTable } from 'react-table'
import Layout from '../Layout';
import Loader from "./loader";
import {  useColor } from 'react-color-palette'
import { useHistory } from 'react-router-dom'
import Modal from "./ModalComponent";
import { GoTriangleDown, GoTriangleUp } from 'react-icons/go';
import '../pages/SubBioTest/SubBioTest.scss'


const Styles = styled.div`

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

const Table = ({ columns, data, id ,mi_no, errorMsg, modelOpen }) => {

    const [loader, setLoader] = useState(false);
    const [records, setRecords] = React.useState(data)
    const [errorModelOpen, setErrorModelOpen] = useState(modelOpen)
    const [errorMessage,setErrorMessage] = useState(errorMsg)
    let history = useHistory();
    const [viewRecord, setViewRecord] = useState(undefined)
    const getRowId = React.useCallback(row => {
        return row.id
    }, [])

    useEffect(() =>{
        setRecords(data)
        setErrorMessage(errorMsg)
        setErrorModelOpen(modelOpen)
    },[data,errorMsg,modelOpen])
    
    const {
        getTableProps,
        getTableBodyProps,
        prepareRow,
        rows,
        headerGroups,
    } = useTable({
        getRowId,
        columns,
        data: viewRecord ? viewRecord :records, 
    },useSortBy
    )

    /**
     * This function manage table background row color according to score 
     */
    useEffect(() =>{
        let updatedRecords = records?.map((item) => {
          
            if(item?.score <= 39){
                return {...item, color:"#FF8880"}
            }
            else if(item?.score >=40 && item?.score <=59){
                return {...item, color:"#FFCA91"}
            }
            else if(item?.score >=60 && item?.score <=79){
                return {...item, color:"#FFE18C"}
            }
            else if(item?.score >=80 && item?.score <=100){
                return {...item, color:"#A4CD9D"}
            }
            else {
                return item
            }
        })
        setViewRecord(updatedRecords)
    },[records])

    return (
        <Layout>
            {loader && <Loader />}
            <Modal
                open={errorModelOpen}
                closeModal={() => {
                    setErrorModelOpen(false)
                    history.goBack()
                }}
                subTitle={errorMessage}
                error={true}
                fromError={true}
            />
            <div className='dragable-table-wrapper subbiotest-dragable-table-wrapper medical-indication-predict-page'>
                <div className='medical-indication-details-view-header'>
                    <div className='medical-indication-view-wrapper'>
                        <div className='name-left-column'>
                        <p>{localStorage.getItem("subBioTestes") ?"Sub Bio Test:" :"Medical Indication:"}<strong>{mi_no}</strong></p>
                        </div>
                        <div className='color-right-column'>
                            <p>Task Type: <strong>Predict</strong></p>
                            <ul>
                                <li> <span style={{ backgroundColor: '#FF8880', }} className='status-color'></span> 0-39 </li>
                                <li> <span style={{ backgroundColor: '#FFCA91', }} className='status-color'></span> 40-59 </li>
                                <li> <span style={{ backgroundColor: '#FFE18C', }} className='status-color'></span> 60-79 </li>
                                <li> <span style={{ backgroundColor: '#A4CD9D', }} className='status-color'></span> 80-100 </li>
                            </ul>
                        </div>
                    </div>
                </div>

                    <div className='table-responsive predict-fixed-row'>
                        <table className='dragable-table predict-test-table' {...getTableProps()} style={{ border: '1px solid #EBEBEB', width: "100%", }}>
                            <thead style={{ border: '1px solid #FFFFFF' }}>
                                {headerGroups.map(headerGroup => (
                                    <tr {...headerGroup.getHeaderGroupProps()} style={{ border: '1px solid #FFFFFF', boxShadow: ' 0px 1px 1px #1C6FE929' }}>
                                        {headerGroup.headers.map(column => (
                                            <th style={{ border: '1px solid #F8F8F8', color: '#757575' }} {...column.getSortByToggleProps()}>
                                                {column.render('Header')} 
                                                <span>{column.isSorted ? (column.isSortedDesc ? <GoTriangleDown style={{color: '#3498db' }}/> : <GoTriangleUp style={{color: '#3498db' }}/>) : ''}</span> 
                                            </th>
                                        ))}
                                    </tr>
                                ))}
                            </thead>
                            <tbody {...getTableBodyProps()} style={{ border: '1px solid #F8F8F8' }}>
                                {rows.map(
                                    (row, index) =>
                                        prepareRow(row) || (
                                            <Row
                                                index={index}
                                                row={row}
                                                {...row.getRowProps()}
                                            />
                                        )
                                )}
                            </tbody>
                        </table>
                    </div>
            </div>
        </Layout> 
       
    )
}

const Row = ({row}) => {
    
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);

    function useOutsideAlerter(ref) {
        useEffect(() => {
            /**
             * Alert if clicked on outside of element
             */
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    // setDisplayColorPicker(false);
                }
            }
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }
    return (
        <tr style={{backgroundColor: row?.original?.color, border: '1px solid #F8F8F8', textAlign: 'center' }} >
            {row.cells.map(cell => {
                return <td style={{ border: '1px solid #F8F8F8', color: "#404040" }} {...cell.getCellProps()}>{cell.render('Cell')}</td>
            })}
        </tr>
    )
}

const App = (props) => {
    const [loader, setLoader] = useState(props?.loader);
    useEffect(() => {
        setLoader(props?.loader)
    }, [props?.loader]);
    
    

    return (
        <Styles>
            {loader && <Loader />}
            <Table columns={props?.columns} data={props?.row} id={props?.id} mi_no={props?.mi_no} errorMsg={props?.errorMrssage} modelOpen={props?.model} showLayout={props?.id ? false : true}/>
        </Styles>
    )
}


export default App

