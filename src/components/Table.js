import React, { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'
import { useTable } from 'react-table'
import { DndProvider, useDrag, useDrop } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import update from 'immutability-helper'
import Layout from '../Layout';
import Loader from "./loader";
import { AiOutlineBgColors } from 'react-icons/ai';
import { useColor } from 'react-color-palette'
import { useHistory } from 'react-router-dom'
import Modal from "../components/ModalComponent";
import '../pages/SubBioTest/SubBioTest.scss';
import DragIcon from '../assets/drag-drop-icon.png'
import MedicalIndicationApi from '../services/MedicalIndicationApi'
import BioTestApi from '../services/BioTestApi'


const layerStyles = {
    position: "fixed",
    pointerEvents: "none",
    zIndex: 100,
    left: 0,
    top: 0,
    width: "100%",
    height: "100%"
};
const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

const Table = ({ columns, data, id ,mi_no,errorMsg, modelOpen }) => {
    const [loader, setLoader] = useState(false);
    const [records, setRecords] = React.useState()
    const [recordData, setRecordData] = React.useState(true)
    const [errorModelOpen, setErrorModelOpen] = useState(false)
    const [successModelOpen, setSuccessModelOpen] = useState(false)
    const [errorMessage, setErrorMessage] = useState(undefined)
    const [apiError, setApiError] = useState(undefined);
    const [inputError,setInputError] = useState(false)
    const [stringError,setStringError] = useState(false)
    let history = useHistory();
    const [color, setColor] = useColor("");
    const getRowId = React.useCallback(row => {
        return row.id
    }, [])

    useEffect(() =>{
        setRecords(data)
        setErrorMessage(errorMsg)
        setErrorModelOpen(modelOpen)
    },[data,errorMsg,modelOpen])
    
    const {
        getTableProps,
        getTableBodyProps,
        prepareRow,
        rows,
        headerGroups,
    } = useTable({
        getRowId,
        columns,
        data: records,
    })
    /**
     * This function manage when user drag and drop row in table
     */
    const moveRow = (dragIndex, hoverIndex) => {
        const dragRecord = records[dragIndex]
        setRecords(
            update(records, {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragRecord],
                ],
            })
        )
        setRecordData(false)
    }
    /**
     * This function manage table score data when user submit the score data 
     */
    const handleSubmit = async() =>{
        if(localStorage.getItem("subBioTestes")){
            setLoader(true)
        let updatedRecord11 = await records?.map((item, idx) => ({ ...item, index: idx + 1 }))
        let updatedRecord12 = updatedRecord11?.map((item) => item?.score)
        let filterScore = updatedRecord12?.filter((item) => typeof item == 'number' && item != '')
        let filterString = filterScore?.filter(item => typeof item == 'string')
        if(updatedRecord12?.length == filterScore?.length && !filterString.length > 0){        
            await BioTestApi.AddSubBioTestSocre(localStorage.getItem("neswell_access_token"), id, updatedRecord11).then(
                (json) => {
                    if (json?.header?.status === 200 || json?.header?.status === 201) {
                        setSuccessModelOpen(true)
                        setErrorMessage(json?.data?.status)
                        setLoader(false)
                    } else {
                        setErrorModelOpen(true)
                        setApiError(true)
                        setLoader(false)
                    }
                }
            )
        }else{
            if(filterString.length > 0){
                setStringError(true)
                setErrorModelOpen(true)
                setLoader(false)
            }else{
                setInputError(true)
                setErrorModelOpen(true)
                setLoader(false)
            }
        }
        }else{
            setLoader(true)
            let updatedRecord11 = await records?.map((item, idx) => ({ ...item, index: idx + 1 }))
            let updatedRecord12 = updatedRecord11?.map((item) => item?.score)
            let filterScore = updatedRecord12?.filter((item) => typeof item == 'number')
            let filterString = filterScore?.filter(item => typeof item == 'string')
            
            if(updatedRecord12?.length == filterScore?.length && !filterString.length > 0){
            await MedicalIndicationApi.AddMedicalIndicationScore(localStorage.getItem("neswell_access_token"), id, updatedRecord11).then(
                (json) => {
                    if (json?.header?.status === 200 || json?.header?.status === 201) {
                        setSuccessModelOpen(true)
                        setErrorMessage(json?.data?.status)
                        setLoader(false)
                    } else {
                        setErrorModelOpen(true)
                        setApiError(true)
                        setLoader(false)
                    }
                }
            )
            }else{
                if(filterString.length > 0){
                    setStringError(true)
                    setErrorModelOpen(true)
                    setLoader(false)
                }else{
                    setInputError(true)
                    setErrorModelOpen(true)
                    setLoader(false)
                }
            }
        }
    }
  
    /**
     * This function manage table row according score color in table row 
     */
    const handleOrganize = async (item) => {
        let sortArray = []
        let updatedRecordsRed = records?.filter((item => item?.color == '#FF8880'))
        let updatedRecordsYellow = records?.filter((item => item?.color == '#FFCA91'))
        let updatedRecordsOrange = records?.filter((item => item?.color == '#FFE18C'))
        let updatedRecordsGreen = records?.filter((item => item?.color == '#A4CD9D'))
        let updatedRecordsBlank = records?.filter((item => item?.color == ''))

        updatedRecordsGreen.map((item) => (sortArray.push(item)))
        updatedRecordsOrange.map((item) => (sortArray.push(item)))
        updatedRecordsYellow.map((item) => (sortArray.push(item)))
        updatedRecordsRed.map((item) => (sortArray.push(item)))
        updatedRecordsBlank.map((item) => (sortArray.push(item)))
        setRecords(sortArray)
    }
    /**
     * This function manage table row color in score table
     * When user selet color from color Picker, it's set color in row
     */
    const handleSetColorInData = (item, id) => {
        let updatedRecords = records?.map((item1) => (item1?.id == id ? { ...item1, color: item } : item1))
        setRecords(updatedRecords)
    }
    /**
     * This function manage score input box, set score filed in array and update main array
     */
    const handleSetInputData = (item, id) => {
        let updatedInputData = records?.map((item2) => (item2?.id == id ? { ...item2, score: item } : item2))
        setRecords(updatedInputData)
    }
    return (
        <Layout>
            {loader && <Loader />}
            <Modal
                open={errorModelOpen}
                closeModal={() => {
                    stringError || inputError || apiError ? setErrorModelOpen(false) :history.goBack()
                }}
                subTitle={apiError ? "Enter Proper Data" :  stringError ? "Please enter numeric value only." : inputError? "All scores must be fill up." : errorMessage}
                fromError={true}
                error={true}
            />
            <Modal
                open={successModelOpen}
                DeleteUsersRecord={() => {
                    setErrorModelOpen(false)
                    history.goBack()
                }}
                title={"Record add successfully."}
                subTitle={"Record added successfully."}

            />
            <div className='dragable-table-wrapper subbiotest-dragable-table-wrapper'>
                <div className='medical-indication-details-view-header'>
                    <div className='medical-indication-view-wrapper'>
                          <div className='name-left-column'>
                            
                            <p>{localStorage.getItem("subBioTestes") ?"Sub Bio Test:" :"Medical Indication:"}<strong>{mi_no}</strong></p>
                        </div>
                        <div className='color-right-column'>
                            <p>Task Type: <strong>Score</strong></p>
                            <ul>
                                <li> <span style={{ backgroundColor: '#FF8880', }} className='status-color'></span> 0-39 </li>
                                <li> <span style={{ backgroundColor: '#FFCA91', }} className='status-color'></span> 40-59 </li>
                                <li> <span style={{ backgroundColor: '#FFE18C', }} className='status-color'></span> 60-79 </li>
                                <li> <span style={{ backgroundColor: '#A4CD9D', }} className='status-color'></span> 80-100 </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <DndProvider backend={HTML5Backend}>
                    <div className='table-responsive '>
                        <table className='dragable-table ' {...getTableProps()} style={{ border: '1px solid #EBEBEB' }}>
                            <thead style={{ border: '1px solid #FFFFFF' }}>
                                {headerGroups.map(headerGroup => (
                                    <tr {...headerGroup.getHeaderGroupProps()} style={{ border: '1px solid #FFFFFF', boxShadow: ' 0px 1px 1px #1C6FE929' }}>
                                        {headerGroup.headers.map(column => (
                                            <th style={{ border: '1px solid #F8F8F8', color: '#757575' }} {...column.getHeaderProps()}>{column.render('Header')}</th>
                                        ))}
                                        <th style={{ border: '1px solid #FFFFFF', boxShadow: ' 0px 1px 1px #1C6FE929', color: '#757575', width: 130, }}>Score</th>
                                        <th className='pin' style={{ border: '1px solid #FFFFFF', boxShadow: ' 0px 1px 1px #1C6FE929', color: '#757575', minWidth: 70, width: 70, }}></th>
                                    </tr>
                                ))}
                            </thead>
                            <tbody {...getTableBodyProps()} style={{ border: '1px solid #F8F8F8' }}>
                                {rows.map(
                                    (row, index) =>
                                        prepareRow(row) || (
                                            <Row
                                                index={index}
                                                row={row}
                                                moveRow={moveRow}
                                                records={(item, id) => handleSetColorInData(item, id)}
                                                textInput={(item, id) => handleSetInputData(item, id)}
                                                {...row.getRowProps()}
                                            />
                                        )
                                )}
                            </tbody>
                        </table>
                    </div>
                </DndProvider>
            </div>
            <div className='sub-bio-test-btn' >
                <div style={{ marginRight: 15, }} className="form-submit-btn">
                    <button onClick={() => handleOrganize()} className="primary-btn sbt-btn">Organize</button>
                </div>
                <div className="form-submit-btn">
                    <button onClick={() => handleSubmit()} className="primary-btn sbt-btn">Submit</button>
                </div>
            </div>
        </Layout>
    )
}

const DND_ITEM_TYPE = 'row'

/**
 * This function manage table rows
 */
const Row = ({ row, index, moveRow, records, textInput }) => {
    const dropRef = React.useRef(null)
    const dragRef = React.useRef(null)

    const [, drop] = useDrop({
        accept: DND_ITEM_TYPE,
        hover(item, monitor) {
            if (!dropRef.current) {
                return
            }
            const dragIndex = item.index
            const hoverIndex = index
            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return
            }
            // Determine rectangle on screen
            const hoverBoundingRect = dropRef.current.getBoundingClientRect()
            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
            // Determine mouse position
            const clientOffset = monitor.getClientOffset()
            // Get pixels to the top
            const hoverClientY = clientOffset.y - hoverBoundingRect.top
            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%
            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }
            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }
            // Time to actually perform the action
            moveRow(dragIndex, hoverIndex)
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        },
    })

    const [{ isDragging }, drag, preview] = useDrag({
        item: { type: DND_ITEM_TYPE, index },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    })

    const opacity = isDragging ? 0 : 1

    preview(drop(dropRef))
    drag(dragRef)

    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const [pricePerkg, setpricePerkg] = useState();
    const [color, setColor] = useColor("");
    const [score, setScore] = useState("");
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);
    const handleClick = (e) => {
        e.preventDefault();
        setDisplayColorPicker(!displayColorPicker);
    };
    function useOutsideAlerter(ref) {
        useEffect(() => {
            /**
             * Alert if clicked on outside of element
             */
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    setDisplayColorPicker(false);
                }
            }
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }
    /**
     * This function manage row background color 
     */
    const handleColorSet = (color, cell) => {
        records(color, cell.id);
        setColor(color)
    }
    /**
     * This function manage score data in table
     */
    const handleInput = (text, cell) => {
        let { value, min, max } = text.target;

        let pattern = /^\d{1,}(\.\d{0,4})?$/;
        let isValid = pattern.test(text);
        if(!Number(value)){
            return false
        }

        let ScoreValue = Math.max(Number(min), Math.min(Number(max), Number(value)));
        textInput(ScoreValue, cell.id)
        setScore(ScoreValue)
    }
    return (
        <>
            <tr ref={dropRef} style={{ opacity, backgroundColor: color, border: '1px solid #F8F8F8', textAlign: 'center' }} >
                {row.cells.map(cell => {
                    return <td style={{ border: '1px solid #F8F8F8', color: "#404040" }} {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}{
                    < >
                        <td className="form-group score-column" style={{ backgroundColor: color, border: '1px solid #F8F8F8', color: "#404040" }}>
                            <input
                                className="add-user-input"
                                placeholder="Enter the Score"
                                type="text"
                                pattern='/^\d+\.?\d*$/'
                                min="0"
                                max="100"
                                onChange={(e) => handleInput(e, row)}
                                value={score}
                                required={true}
                                style={{ border: "none", }}
                            />
                        </td>
                        <td className='drag-column form-group' style={{ backgroundColor: color,border: '1px solid #F8F8F8', color: "#404040" }}>
                            <div className='table-color-dragable-wrapper' >
                                <div className='table-color-popup' >
                                    <div className='table-color-picker' >
                                        <AiOutlineBgColors ref={wrapperRef} onClick={handleClick} />
                                    </div>

                                </div>
                                <div ref={dragRef} style={{ marginLeft: '20px' }}>
                                    <img src={DragIcon} style={{ width: '12px', }} alt="" />
                                </div>
                            </div>
                        </td>
                    </>
                }
                {displayColorPicker && (<div className='table-drag-color'> <div className='table-color-dropdown'>

                    <ul >
                        <li color={color} onClick={(e) => {
                            handleColorSet('#FF8880', row)
                            setDisplayColorPicker(false);
                        }} style={{
                            backgroundColor: '#FF8880',
                            top: 488,
                            marginRight: 20,
                            left: 1225,
                            width: 32,
                            height: 32,
                            borderRadius: 5,
                            opacity: 1
                        }}></li>
                        <li color={color} onClick={(e) => {
                            handleColorSet('#FFCA91', row)
                            setDisplayColorPicker(false);
                        }} style={{
                            backgroundColor: '#FFCA91',
                            top: 488,
                            marginRight: 20,
                            left: 1225,
                            width: 32,
                            height: 32,
                            borderRadius: 5,
                            opacity: 1
                        }}> </li>
                        <li color={color} onClick={(e) => {
                            handleColorSet('#FFE18C', row)
                            setDisplayColorPicker(false);
                        }} style={{
                            backgroundColor: '#FFE18C',
                            top: 488,
                            marginRight: 20,
                            left: 1225,
                            width: 32,
                            height: 32,
                            borderRadius: 5,
                            opacity: 1
                        }}></li>
                        <li color={color} onClick={(e) => {
                            handleColorSet('#A4CD9D', row)
                            setDisplayColorPicker(false);
                        }} style={{
                            backgroundColor: '#A4CD9D',
                            top: 488,
                            marginRight: 20,
                            left: 1225,
                            width: 32,
                            height: 32,
                            borderRadius: 5,
                            opacity: 1
                        }}></li>
                        <li color={color} onClick={(e) => {
                            handleColorSet('', row)
                            setDisplayColorPicker(false);
                        }}
                            style={{
                                backgroundColor: '#F1F1F1',
                                top: 488,
                                marginRight: 20,
                                left: 1225,
                                width: 32,
                                height: 32,
                                borderRadius: 5,
                                opacity: 1
                            }}></li>
                    </ul>
                </div> </div>)
                }
            </tr>
        </>
    )
}

const App = (props) => {
    const [loader, setLoader] = useState(props?.loader);
    useEffect(() => {
        setLoader(props?.loader)
    }, [props?.loader]);
    
    return (
        <Styles>
            {loader && <Loader />}
            <Table columns={props?.columns} data={props?.row} id={props?.id} errorMsg={props?.errorMrssage} modelOpen={props?.model} mi_no={props?.mi_no}/>
        </Styles>
    )
}


export default App