import React, { Component } from "react";

import GoogleMapReact from "google-map-react";

import styled from "styled-components";
import AutoComplete from "./Autocomplete";
import Marker from "./Marker";

const Wrapper = styled.main`
  width: 100%;
  height: 100%;
`;

class MyGoogleMap extends Component {
  state = {
    mapApiLoaded: false,
    mapInstance: null,
    mapApi: null,
    geoCoder: null,
    places: [],
    center: [],
    zoom: 12,
    address: "",
    draggable: true,
    lat: this.props.lat,
    lng: this.props.lng,
  };
  componentWillMount() {
    this.setCurrentLocation();
    this.setState({
      lat: this.props.lat,
      lng: this.props.lng,
    });
  }

  onMarkerInteraction = (childKey, childProps, mouse) => {
    this.setState({
      draggable: false,
      lat: mouse.lat,
      lng: mouse.lng,
    });
  };
  onMarkerInteractionMouseUp = (childKey, childProps, mouse) => {
    this.setState({ draggable: true });
    this._generateAddress();
  };

  _onChange = ({ center, zoom }) => {
    this.setState({
      center: center,
      zoom: zoom,
    });
  };

  _onClick = (value) => {
    this.setState({
      lat: value.lat,
      lng: value.lng,
    });
  };

  apiHasLoaded = (map, maps) => {
    this.setState({
      mapApiLoaded: true,
      mapInstance: map,
      mapApi: maps,
    });

    this._generateAddress();
  };

  addPlace = (place) => {
    this.setState({
      places: [place],
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
    });
    this.props.setLat(place.geometry.location.lat())
    this.props.setLng(place.geometry.location.lng())
    this._generateAddress();
  };

  _generateAddress() {
    const { mapApi } = this.state;

    const geocoder = new mapApi.Geocoder();

    geocoder.geocode(
      { location: { lat: this.props.lat, lng: this.props.lng } },
      (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            this.zoom = 12;
            this.setState({ address: results[0].formatted_address });
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Geocoder failed due to: " + status);
        }
      }
    );
  }


  setCurrentLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.setState({
          center: [position.coords.latitude, position.coords.longitude],
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      });
    }
  }
  render() {
    const { places, mapApiLoaded, mapInstance, mapApi } = this.state;
    return (
      <Wrapper>
        {!window.location.href.includes("view-extract") &&
          mapApiLoaded &&
          !this.props.viewProfile && (
            <div>
              <AutoComplete
                map={mapInstance}
                mapApi={mapApi}
                addplace={this.addPlace}
                setAddressName={(value) => this.props.setAddresName(value)}
                addressName={this.props.addressName}
              />
            </div>
          )}
        {this.props.edit ? (
          <GoogleMapReact
            defaultCenter={{
              lat: parseFloat(this.props?.lat?.toString()?.slice(0, 5)),
              lng: parseFloat(this.props?.lng?.toString()?.slice(0, 5)),
            }}
            zoom={this.state.zoom}
            draggable={this.state.draggable}
            onChange={this._onChange}
            onChildMouseDown={this.onMarkerInteraction}
            onChildMouseUp={this.onMarkerInteractionMouseUp}
            onChildMouseMove={this.onMarkerInteraction}
            onClick={this.props.onClickMap}
            bootstrapURLKeys={{
              key: "AIzaSyAEXqkxDIgJ9DX-GZ_9FIK0coRTp8Ro0qg",
              libraries: ["places", "geometry"],
            }}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => this.apiHasLoaded(map, maps)}
          >
            <Marker
              text={this.state.address}
              lat={this.props.lat}
              lng={this.props.lng}
            />
          </GoogleMapReact>
        ) : (
          <GoogleMapReact
            defaultCenter={{
              lat: parseFloat(this.props?.lat?.toString()?.slice(0, 5)),
              lng: parseFloat(this.props?.lng?.toString()?.slice(0, 5)),
            }}
            zoom={this.state.zoom}
            draggable={this.state.draggable}
            onChange={this._onChange}
            onChildMouseDown={this.onMarkerInteraction}
            onChildMouseUp={this.onMarkerInteractionMouseUp}
            onChildMouseMove={this.onMarkerInteraction}
            onClick={this.props.onClickMap}
            bootstrapURLKeys={{
              key: "AIzaSyAEXqkxDIgJ9DX-GZ_9FIK0coRTp8Ro0qg",
              libraries: ["places", "geometry"],
            }}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => this.apiHasLoaded(map, maps)}
          >
            <Marker
              text={this.state.address}
              lat={this.props.lat}
              lng={this.props.lng}
            />
          </GoogleMapReact>
        )}
      </Wrapper>
    );
  }
}

export default MyGoogleMap;
