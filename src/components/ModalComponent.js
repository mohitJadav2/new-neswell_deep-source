import React, { useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";
import { AiFillCheckCircle, AiFillCloseCircle } from "react-icons/ai";
import {IoIosAlert} from "react-icons/io"
import "./modal.style.scss";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 30,
    borderRadius: 40,
    border: 'none',
    width: 275,
    boxShadow: '0 6px 16px rgba(0,0,0, 0.16)'
  },
};
/**
 * This function manage pop of application like delete, error
 */
const ModalComponent = (props) => {
  let subtitle;

  return (
    <div>
      <Modal
        isOpen={props.open}
        style={customStyles}
        contentLabel="Example Modal"
        overlayClassName={"react-modal-body"}
      >
       
        <div className="user-profile-popup">
          {props?.error && (
            <span className="close-icon">
              <AiFillCloseCircle />
              <h2 className="alt-title clr-red">Error</h2>
            </span>
          )}
          {!props?.error && props?.delete ? (
            <span className="warning-icon">
              <IoIosAlert />
              <h2 className="alt-title clr-warning">Warning</h2>
            </span>
          ) : (
            <>
            {!props?.error && <span className="checked-icon">
              <AiFillCheckCircle />
              <h2 className="alt-title clr-blue">Success</h2>
            </span>}
            </>
          )}
          {/* {props?.delete && (
            <h2 ref={(_subtitle) => (subtitle = _subtitle)}>{props?.title}</h2>
          )} */}
           <p>{props?.subTitle}</p>
          <div className={props?.delete ? "popup-footer delete-popup" : "popup-footer create-popup"}>
            {!props?.fromError && props?.delete && (
              <button className="popup-btn p-btn-delete" onClick={() => props?.DeleteUsersRecord()}>Delete</button>
            )}
            {props?.error || props?.delete ? (
              <button className="popup-btn p-btn-close" onClick={() => props?.closeModal()}>{!props?.delete ?"Close" : "Close"}</button>
            ) : (
              <button className="popup-btn p-btn-success" onClick={() => props?.DeleteUsersRecord()}>Continue</button>
            )}
          </div>
          
        </div>
      </Modal>
    </div>
  );
};

export default ModalComponent;
