import React from 'react'
// import loaderGif from "../assets/loader.svg";

const loader = () => {
    return (
        <div className='loader-outer' style={{position: "absolute", top: 0, left: 0, transform: "translate -50% -50%", zIndex: 9, width: window.location.pathname.length <= 1 ? "100%" : "calc(100% - 18vw)",marginLeft: window.location.pathname.length <= 1  ? 0 : "18vw", height: "100%", display: 'flex', alignItems: 'center', justifyContent: 'center', backgroundColor: '#00000066'}}>
            {/* <img style={{borderRadius: "50%", maxWidth: '250px'}} src={loaderGif} alt="" /> */}
            <div class="loader-dash uno"></div>
            <div class="loader-dash dos"></div>
            <div class="loader-dash tres"></div>
            <div class="loader-dash cuatro"></div>
        </div>
    )
}

export default loader;