import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter, Router } from 'react-router-dom';

/**
 * This section manages whole web application
 * The <App /> component stands for App.js
 * The App.js manage all application components
 * The <BrowserRouter /> is navigation component for application
 */
ReactDOM.render(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
  document.getElementById('root'),
);
