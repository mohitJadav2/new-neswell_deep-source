import React, { useState } from 'react';
import { Provider } from "react-redux";
import Login from "./pages/Login"
import './styles/App.scss';
import './styles/responsive.scss';
import { Route, Switch } from 'react-router-dom';
import User from "./pages/Users"
import AddUser from './pages/Users/AddUsers';
import UserProfile from './pages/Users/UserProfile';
import {store, persistor} from "./store";
import ForgotPassword from "./pages/Login/ForgotPassword";
import ResetPassword from './pages/Login/ResetPassword';
import OtpPage from './pages/Login/OtpCode';
import UserExtract from './pages/ExtractProfile/extractProfileDetails';
import CreateNewExtract from './pages/ExtractProfile/createNewExtract';
import ExtractViewProfile from './pages/ExtractProfile/viewExtractProfile';
import BioTestList from './pages/ManageBioTest/bioTestList';
import BioTestView from './pages/ManageBioTest/bioTestView';
import CreateBio from './pages/ManageBioTest/createBio';
import ViewBio from './pages/ManageBioTest/viewBio';
import PageNotFound from './pages/PageNotFound/pageNotFound';
import UploadExperiment from "./pages/UploadExperiment";
import ExperimentData from "./pages/UploadExperiment/experimentData.js";
import ExperimentList from "./pages/UploadExperiment/experimentList.js";
import MedicalIndicationDetals from './pages/MedicalIndication/medicalIndicationDetails';
import MedicalIndicationView from './pages/MedicalIndication/medicalIndicationDetailsView';
import CreateMedicalIndication from './pages/MedicalIndication/createMedicalIndication';
// import SubBioTestScorePage from './pages/SubBioTest/SubBioTestScorePage';
import SubBioScore from './pages/SubBioTest/Score';
import SupplierData from './pages/Suppliers/supplierData'
import CreateSupplier from './pages/Suppliers/CreateSupplier'
import ViewSupplier from './pages/Suppliers/ViewSuppliers'
import SubBioTestList from './pages/SubBioTest/SubBioTestList';
// import SubBioTestViewPage from './pages/SubBioTest/SubBioTestViewPage';
// import SubBioTestPredictPage from './pages/SubBioTest/SubBioTestPredictPage';
import SubBioTestPredictPage from './pages/SubBioTest/Predict';
// import MeidcalIndicationScore from './pages/MedicalIndication/MedicalIndicationScore'
import Score from './pages/MedicalIndication/Score'
// import MeidcalIndicationPredict from './pages/MedicalIndication/MedicalIndicationPredict'
import MeidcalIndicationPredict from './pages/MedicalIndication/Predict'
// import Table from './pages/MedicalIndication/Score'
/**
 * The App() manages routes and components of web application
 * Here <Provider /> & <presistGate /> manage redux store 
 * The <Switch /> & <Route /> imported from "react-router-dom" library to manage navigation of application
 */

function App() {
  return (
    <Provider store={store}>
      <persistGate persistor={persistor}>
      <Switch>  
        <Route exact path="/" component={Login} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/otp" component={OtpPage} />
        <Route exact path="/reset-password" component={ResetPassword} />
        <Route exact path="/reset-password/:uuid/:token" component={ResetPassword} />
        <Route exact path="/users" component={User} />
        <Route path="/add-users/" exact component={AddUser} />
        <Route path="/add-users/:slug" exact component={AddUser} />
        <Route exact path="/users-profile/:slug" component={UserProfile} />
        <Route path="/users-extract" component={UserExtract} />
        <Route path="/create-extract/:slug" component={CreateNewExtract} />
        <Route path="/create-extract" component={CreateNewExtract} />
        <Route path="/view-extract/:slug" component={ExtractViewProfile} />
        <Route path="/bio-test-list" component={BioTestList} />
        <Route path="/bio-test-view" exact component={BioTestView} />
        <Route path="/bio-test-view/:slug" component={BioTestView} />
        <Route path="/create-bio" exact component={CreateBio} />
        <Route path="/create-bio/:slug" exact component={CreateBio} />
        <Route path="/view-bio-details/:slug" component={ViewBio} />
        <Route path="/sub-bio-test-list" component={SubBioTestList} />
        <Route path="/upload-experiment" exact component={UploadExperiment} />
        <Route path="/upload-experiment/:slug" exact component={UploadExperiment} />
        <Route path="/view-experiment/:slug" exact component={UploadExperiment} />
        <Route path="/experiment-data" component={ExperimentData} />
        <Route path="/experiment-list" component={ExperimentList} />
        <Route path="/create-medical-indication" exact component={CreateMedicalIndication} />
        <Route path="/create-medical-indication/:slug" exact component={CreateMedicalIndication} />
        <Route path="/medical-indication-view" exact component={MedicalIndicationView} />
        <Route path="/medical-indication-view/:slug" exact component={MedicalIndicationView} />
        {/* <Route path="/medical-indication-score" exact component={MeidcalIndicationScore} />
        <Route path="/medical-indication-score/:slug" exact component={MeidcalIndicationScore} /> */}
        <Route path="/medical-indication-score" exact component={Score} />
        <Route path="/medical-indication-score/:slug" exact component={Score} />
        <Route path="/medical-indication-predict" exact component={MeidcalIndicationPredict} />
        <Route path="/medical-indication-predict/:slug" exact component={MeidcalIndicationPredict} />
        <Route path="/medical-indication-details" component={MedicalIndicationDetals} />
        {/* <Route path="/sub-bio-test-score" exact component={SubBioTestScorePage} />
        <Route path="/sub-bio-test-score/:slug" exact component={SubBioTestScorePage} /> */}
         <Route path="/sub-bio-test-score" exact component={SubBioScore} />
        <Route path="/sub-bio-test-score/:slug" exact component={SubBioScore} />
        {/* <Route path="/sub-bio-test-view" exact component={SubBioTestViewPage} /> */}
        {/* <Route path="/sub-bio-test-view/:slug" exact component={SubBioTestViewPage} /> */}
        <Route path="/sub-bio-test-predict" exact component={SubBioTestPredictPage} />
        <Route path="/sub-bio-test-predict/:slug" exact component={SubBioTestPredictPage} />
        <Route path="/suppliers-data" component={SupplierData} />
        <Route path="/create-suppliers" exact component={CreateSupplier} />
        <Route path="/create-suppliers/:slug" exact component={CreateSupplier} />
        <Route path="/view-suppliers/:slug" component={ViewSupplier} />
        {/* <Route path="/table" component={Table} /> */}
        <Route path="" component={PageNotFound} />
      </Switch>
      </persistGate>
    </Provider>
  );
}

/**
 * App.js export to index.js
 */
export default App;
