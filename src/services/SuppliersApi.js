import { fetchDataFromServer } from "./url";

class SuppliersApi{
    /**
     *This API function manage supplier deatils  
    */
    static SuppliersData(orderType, orderField,token, page, perPage){
        return fetchDataFromServer(`/suppliers/?pera=${orderType}&field=${orderField}&page=${page}&page_size=${perPage}`, "GET", null, token, true);
    }

    /**
     *This API manage search records in supplier list table 
    */
    static SuppliersSearch(orderType, orderField, token, string, page, perPage){
        return fetchDataFromServer(`/suppliers/search/?pera=${orderType}&field=${orderField}&search=${string}&page=${page}&page_size=${perPage} `, "GET", null, token, true);
    }

    /**
     * This API manage supplier's delete records
     */
    static SuppliersDelete(token, id) {
        return fetchDataFromServer(`/supplier/delete/${id}/`, "DELETE", null, token, true);
    }

    /**
     *This API manage supplier's edit pagen data 
    */
    static SuppliersEdit(token, id, formData) {
        return fetchDataFromServer(`/supplier/update/${id}/`, "PUT", formData, token, true);
    }

    /**
     *This API manage when user create new supplier record 
    */
    static AddSuppliers(formData, token) {
        return fetchDataFromServer(`/suppliers/`, "POST", formData, token, true);
    }

    /**
     *This API manage supplier view page data 
    */
    static SuppliersView(token,id) {
        return fetchDataFromServer(`/supplier/${id}`, "GET",  null, token, true);
    }

    /**
     * This API manage map location in create supplier and view supplier page 
     */
    static fetchLocationAddress(lat, lng, token){
        return fetchDataFromServer(`/get_location/${lat}/${lng}/`, "GET", null, token, true);
    }
    
    /**
     *This API manage extract records in supplier view page 
     */
    static extractDetail(id, token){
        return fetchDataFromServer(`/supplier/${id}/`, "GET", null, token, true);
    }
}
export default SuppliersApi