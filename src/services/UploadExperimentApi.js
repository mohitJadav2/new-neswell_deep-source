import { fetchDataFromServer, multipartApi } from "./url";

class UploadExperimentApi {
    static fetchExtractsData(token) {
        return fetchDataFromServer(`/get_extract_list/`, "GET", null, token, true);
    }
    static fetchExperimentData(token, orderType, orderField, currentPage,perPage) {
        return fetchDataFromServer(`/experiments/?pera=${orderType}&field=${orderField}&page=${currentPage}&page_size=${perPage}`, "GET", null, token, true);
    }
    static AssendingfetchExperimentData(orderType, orderField,token, page, perPage){
        return fetchDataFromServer(`/experiments/?pera=${orderType}&field=${orderField}&page=${page}&page_size=${perPage}`, "GET", null, token, true);
    }
    static submitExperiment(token, formData) {
        return fetchDataFromServer(`/experiments/`, "POST", formData, token, true);
    }
    static getChildBioTestes(token) {
        return fetchDataFromServer(`/Get_child_bio_test_List/`, "GET", null, token, true);
    }
    static getFatherBioTestDocuments(id, token) {
        return fetchDataFromServer(`/Get_child_bio_test_List/${id}/`, "GET", null, token, true);
    }
    static generateMatrixData(row, col, gid, token) {
        return fetchDataFromServer(`/Get_table_form_data/${row}/${col}/${gid}/`, "GET", null, token, true);
    }
    static getGroupOptions(token) {
        return fetchDataFromServer(`/get_group_list/`, "GET", null, token, true);
    }
    static getClusterOption(token) {
        return fetchDataFromServer(`/get_cluster_list/`, "GET", null, token, true);
    }
    static getExtractOption(token) {
        return fetchDataFromServer(`/get_extract_list/`, "GET", null, token, true);
    }
    static submitUploadExperiment(token, formData) {
        return fetchDataFromServer(`/upload_experiments/`, "POST", formData, token, true);
    }
    static editExperiment(token,id, formData) {
        return fetchDataFromServer(`/upload_experiments/record/${id}/`, "PUT", formData, token, true);
    }
    static getUploadExperimentById(token, id) {
        return fetchDataFromServer(`/upload_experiments/record/${id}`, "GET", null, token, true);
    }
    static getUploadExperiment(token, page, perPage) {
        return fetchDataFromServer(`/upload_experiments/record/?page=${page}&page_size=${perPage}`, "GET", null, token, true);
    }
    static searchUploadExperiment(orderType,orderField,token, pagNumber, totaldataToShow, searchText) {
        return fetchDataFromServer(`/upload_experiment/record/search/?pera=${orderType}&field=${orderField}&search=${searchText}&page=${pagNumber}&page_size=${totaldataToShow}`, "GET", null, token, true);
    }
    static deleteExperiment(token, id) {
        return fetchDataFromServer(`/upload_experiments/record/${id}/`, "DELETE", null, token, true);
    }
    static submitFiles(formData, token){
        return multipartApi(`/upload_experiments/`, "POST", formData, token);
    }
    static getFaterBioTest(token){
        return fetchDataFromServer(`/Get_father_bio_test_List/`, "GET", null, token, true);
    }
    static getFaterBioTestList(token,id){
        return fetchDataFromServer(`/Get_father_bio_test_List/${id}/`, "GET", null, token, true);
    }
    static getFatherFilterCulster(token,id){
        return fetchDataFromServer(`/get_father_cluster_list/${id}`, "GET", null, token, true);
    }
    static getExperimentOptionData(token,id){
        return fetchDataFromServer(`/upload_experiments/record/${id}/`, "GET", null, token, true);
    }
    static getExperimentAllData(token){
        return fetchDataFromServer(`/experimentAllData/`, "GET", null, token, true);
    }
}

export default UploadExperimentApi;
