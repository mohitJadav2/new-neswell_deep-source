import { fetchDataFromServer, postImage } from "./url";

class BioTestApi {
    static fatherParentBioTest(token, pageNumber,totaldataToShow) {
        return fetchDataFromServer(`/biotest/father/?page=${pageNumber}&page_size=${totaldataToShow}`, "GET", null, token, true);
    }
    static searchFatherBioTest(token, searchTextes,totaldataToShow) {
        return fetchDataFromServer(`/biotest/search/?search=${searchTextes}&page_size=${totaldataToShow}`, "GET", null, token, true);
    }
    static fetchFatherDetailData(token, id) {
        return fetchDataFromServer(`/biotest/father/${id}`, "GET", null, token, true);
    }
    static fetchChildBasedOnParent(token, id) {
        return fetchDataFromServer(`/biotest/father/child/${id}/`, "GET", null, token, true);
    }
    static searchchildBioTestes(token, id, searchText) {
        return fetchDataFromServer(`/biotest/father/child/${id}/?search=${searchText}`, "GET", null, token, true);
    }
    static childSearch(fieldType, fieldName, formData, token, totaldataToShow, pageNumber){
        return fetchDataFromServer(`/biotest/child/?pera=${fieldType}&field=${fieldName}&search=${formData}&page_size=${totaldataToShow}&page=${pageNumber}`, "GET", null, token, true);
    }
    static childAscendingOrder(fieldType, fieldName,totaldataToShow, token){
        return fetchDataFromServer(`/biotest/child/?pera=${fieldType}&field=${fieldName}&page_size=${totaldataToShow}`, "GET", null, token, true);
    }
    static DeleteAndDetailChildBioTest(token, id, type) {
        return fetchDataFromServer(`/biotest/child/${id}/`, type, null, token, true);
    }
    static DuplicateBioTestGet(token, id) {
        return fetchDataFromServer(`/biotest/child/${id}/duplicate/`, "GET", null, token, true);
    }
    static DuplicateBioTestPost(token, id, formData) {
        return fetchDataFromServer(`/biotest/child/${id}/duplicate/`, "POST", formData, token, true);
    }
    static AllFatherList(token) {
        return fetchDataFromServer(`/biotest/father/`, "GET", null, token, true);
    }
    static AllClusterList(token) {
        return fetchDataFromServer(`/clusters/`, "GET", null, token, true);
    }
    static creatChildBioTest(token, formData) {
        return fetchDataFromServer(`/biotest/child/`, "POST", formData, token, true);
    } 
    static getClusterOption(token) {
        return fetchDataFromServer(`/get_cluster_list/`, "GET", null, token, true);
    }
    static getFatherFilterCulster(token,id){
        return fetchDataFromServer(`/get_father_cluster_list/${id}`, "GET", null, token, true);
    }
    static getFilterExtract(token,id,clusterList){
        return fetchDataFromServer(`/supported/extracts/${id}/${clusterList}`, "GET", null, token, true);
    }
    static getAllSubBioTestList(token,orderType,orderField,page, perPage){
        return fetchDataFromServer(`/biotest/childs/records/?page=${page}&page_size=${perPage}&pera=${orderType}&field=${orderField}`, "GET", null, token, true);
    }
    static getAllSubBioTestListPagination(token, page, perPage,orderType,orderField){
        return fetchDataFromServer(`/biotest/childs/records/?page=${page}&page_size=${perPage}&pera=${orderType}&field=${orderField}`, "GET", null, token, true);
    }
    static searchSubBioTestes(token, searchText, orderType,orderField,perPage,page) {
        return fetchDataFromServer(`/biotest/childs/records/?search=${searchText}&pera=${orderType}&field=${orderField}&page=${page}&page_size=${perPage}`, "GET", null, token, true);
    }
    static getAllSubBioTestListPerPage(token,orderType,orderField,page, perPage) {
        return fetchDataFromServer(`/biotest/childs/records/?page=${page}&page_size=${perPage}&pera=${orderType}&field=${orderField}`, "GET", null, token, true);
    }
    static getAllSubBioTestDelete(token, id, type) {
        return fetchDataFromServer(`/biotest/childs/records/${id}/`, type, null, token, true);
    }
    static subBioAscendingOrder(fieldType, fieldName,currentPage,totaldataToShow, token){
        return fetchDataFromServer(`/biotest/childs/records/?pera=${fieldType}&field=${fieldName}&page=${currentPage}&page_size=${totaldataToShow}`, "GET", null, token, true);
    }
    static SubBioTestSocre(token, id){
        return fetchDataFromServer(`/biotest/score/${id}/`, "GET", null, token, true);
    }
    static AddSubBioTestSocre(token,id, formData){
        return fetchDataFromServer(`/biotest/score/${id}/`, "POST",formData, token, true);
    }
    static SubBioTestView(token, id){
        return fetchDataFromServer(`/biotest/view/results/${id}/`, "GET", null, token, true);
    }
    static SubBioTestPredict(token, id){
        return fetchDataFromServer(`/biotest/predict/${id}/`, "GET", null, token, true);
    }
}

export default BioTestApi;
